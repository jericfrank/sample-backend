'use strict';

const path = require( 'path' );
const pack = require( '../package' );

module.exports = {
	'app' : {
		'host'         : '0.0.0.0',
		'port'         : process.env.APP_PORT,
		'jwtKey'       : process.env.JWT_KEY, // Generate jwt key just `run openssl rand -base64 32`,
		'sparkPostKey' : process.env.SPARKPOST_KEY,
		'email'        : 'a.reyes@greasy.tritontek.com.ph',
		'url'          : 'greasy.tritontek.com.ph',
		'imageProxy'   : process.env.IMAGE_PROXY
	},

	'kue' : {
		'port' : 9007
	},

	'database' : {
		'username' : process.env.DB_USERNAME,
		'password' : process.env.DB_PASSWORD,
		'name'     : 'MWFB',
		'options'  : {
			'host'    : process.env.DB_HOST,
			'dialect' : 'mysql',
			'logging' : false
		}
	},

	'logs' : {
		'folder'  : path.join(__dirname, '../logs/'),
		'streams' : [ {
			'level'  : 'debug',
			'stream' : process.stdout // log INFO and above to stdout
		} ]
	},

	's3' : {
		'region'     : 'ap-southeast-1',
		'bucketName' : process.env.AWS_BUCKET_NAME,
		'url'        : process.env.S3_URL
	},

	'swagger' : {
		'info' : {
			'title'       : 'Project greasy API',
			'version'     : pack.version,
			'description' : 'Backend service for project greasy.'
		},
		'swaggerUIPath'     : '/api/',
		'jsonPath'          : '/api/swagger.json',
		'documentationPath' : '/api/docs',
		'pathReplacements'  : [ {
			'replaceIn'   : 'groups',
			'pattern'     : /api/,
			'replacement' : ''
		} ],
		'tags' : [ {
			'name'        : 'users',
			'description' : 'User resource'
		}, {
			'name'        : 'privileges',
			'description' : 'Privilege resource'
		}, {
			'name'        : 'profiles',
			'description' : 'Profile resource'
		}, {
			'name'        : 'stations',
			'description' : 'Station resource'
		}, {
			'name'        : 'events',
			'description' : 'Events resource'
		}, {
			'name'        : 'event-types',
			'description' : 'Event types resource'
		}, {
			'name'        : 'event-frequencies',
			'description' : 'Event frequency types'
		}, {
			'name'        : 'event-recurrences',
			'description' : 'Event recurrences'
		}, {
			'name'        : 'event-templates',
			'description' : 'Event Templates'
		}, {
			'name'        : 'companies',
			'description' : 'Companies'
		} ]
	}
};
