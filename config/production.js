'use strict';

const config = require( './development' );

config.app.routePrefix = '/api';
config.logs.folder     = '/var/log';
config.logs.streams    = [ {
	'level' : 'info',
	'path'  : `${config.logs.folder}/project-greasy.log`
} ];

module.exports = config;
