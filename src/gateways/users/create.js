'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );
const joi  = require( 'joi' );

const userService = use( 'services/users/' );
const log         = use( 'logging' );
const generoute   = use( 'utils/generoute' );
const send        = use( 'utils/send' );


module.exports = {
	'method' : 'POST',
	'path'   : '/api/users',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Create a user',
		'notes'       : 'Returns a newly created user object',
		'tags'        : [ 'api' ],
		'response'    : {
			'schema' : joi.object().keys( {
				'statusCode' : joi.number().description( 'response status' ).example( 200 ),
				'data'       : joi.object().keys( {
					'CreatedBy'       : joi.number().description( 'created by whom' ),
					'UpdatedBy'       : joi.number().description( 'updated by whom' ),
					'Id'              : joi.number().description( 'user id' ),
					'UserName'        : joi.string().description( 'user distinct name' ),
					'FirstName'       : joi.string().description( 'name use to identify the person' ),
					'MiddleName'      : joi.string().allow( '' ).description( 'middle name of the person' ),
					'LastName'        : joi.string().description( 'last name of the person' ),
					'EmailAddress'    : joi.string().description( 'email address' ),
					'Password'        : joi.string().description( 'password' ),
					'ProfileId'       : joi.number().description( 'profile Id' ),
					'StationId'       : joi.number().description( 'station Id' ),
					'Stations'        : joi.array().items( joi.number() ).description( 'array of station id\'s' ),
					'AccountDisabled' : joi.boolean().description( 'account active or not' ),
					'updated_at'      : joi.any().description( 'updated date' ),
					'created_at'      : joi.any().description( 'created date' )
				} )
			} )
		},

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' }
				}
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'payload' : {
				'UserName'     : joi.string().required().description( 'username to identify the person' ),
				'FirstName'    : joi.string().required().description( 'name use to identify the person' ),
				'MiddleName'   : joi.string().allow( '' ).description( 'middle name of the person' ),
				'LastName'     : joi.string().required().description( 'last name of the person' ),
				'EmailAddress' : joi.string().email().required().description( 'email address' ),
				'Password'     : joi.string().required().description( 'password' ),
				'ProfileId'    : joi.number().optional().description( 'profile Id' ),
				'Stations'     : joi.array().optional().items( joi.number().min( 1 ).required() ).description( 'array of station id\'s' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const userId   = request.auth.credentials.sub;
				const data     = request.payload;
				const response = yield userService.create( userId, data );

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					const error = _.first( response.errors );
					return reply( boom.badRequest( error.message ) );
				}

				return reply( send.created( response ) ).code( 201 );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
