'use strict';

const boom  = require( 'boom' );
const joi   = require( 'joi' );

const userService = use( 'services/users' );
const log         = use( 'logging' );
const generoute   = use( 'utils/generoute' );
const send        = use( 'utils/send' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/users/current-user',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Get user based on token',
		'notes'       : 'Returns a user object',
		'tags'        : [ 'api' ],

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'404' : { 'description' : 'The server has not found anything matching the Request' },
					'401' : { 'description' : 'The request requires user authentication' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' }
				}
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required().description( 'some weird set of characters' )
			} ).unknown()
		},

		'handler' : generoute( function* ( request, reply ) {
			const headers  = request.headers;

			try {
				const response = yield userService.getCurrentUser( headers );

				if ( !response ) {
					return reply( boom.notFound() );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
