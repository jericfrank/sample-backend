'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );
const joi  = require( 'joi' );

const userService = use( 'services/users' );
const log         = use( 'logging' );
const generoute   = use( 'utils/generoute' );
const send        = use( 'utils/send' );

module.exports = {
	'method' : 'POST',
	'path'   : '/api/users/forgot-password',
	'config' : {
		'description' : 'Send email link for resetting password',
		'tags'        : [ 'api' ],
		'response'    : {
			'schema' : joi.object().keys( {
				'statusCode' : joi.number().description( 'response status' ).example( 200 ),
				'data'       : joi.object().keys( {
					'Operation' : joi.boolean().description( 'true if email is queue | false if sparkpost is broke' )
				} )
			} )
		},

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'404' : { 'description' : 'The server has not found anything matching the Request' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' }
				}
			}
		},

		'validate' : {
			'payload' : {
				'EmailAddress' : joi.string().required().description( 'email address' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const payload  = request.payload;
				const response = yield userService.forgotPassword( payload );

				if ( !response ) {
					return reply( boom.notFound( 'Resource not found' ) );
				}

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					return reply( boom.badRequest( response.message ) );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );

				return reply( boom.badImplementation() );
			}
		} )
	}
};
