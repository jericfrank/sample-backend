'use strict';

const boom  = require( 'boom' );
const joi   = require( 'joi' );

const userService = use( 'services/users' );
const log         = use( 'logging' );
const generoute   = use( 'utils/generoute' );
const send        = use( 'utils/send' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/users/{Id}',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Get a user specified by id',
		'notes'       : 'Returns a user object',
		'tags'        : [ 'api' ],

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'params' : {
				'Id' : joi.number().required().description( 'user id' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			const id = request.params.Id;

			try {
				const response = yield userService.getById( id );

				if ( !response ) {
					return reply( boom.notFound() );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
