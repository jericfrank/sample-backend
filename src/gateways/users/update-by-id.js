'use strict';

const boom  = require( 'boom' );
const joi   = require( 'joi' );

const userService = use( 'services/users' );
const log         = use( 'logging' );
const generoute   = use( 'utils/generoute' );
const send        = use( 'utils/send' );

module.exports = {
	'method' : 'PUT',
	'path'   : '/api/users/{Id}',
	'config' : {
		'description' : 'Update a specific user',
		'notes'       : 'Returns a newly updated user object',
		'tags'        : [ 'api' ],
		'validate'    : {
			'params' : {
				'Id' : joi.number().required().description( 'user id' )
			},

			'payload' : {
				'UserName'        : joi.string().optional().description( 'username to identify the person' ),
				'Id'              : joi.number().optional().description( 'user id' ),
				'FirstName'       : joi.string().optional().description( 'name use to identify the person' ),
				'MiddleName'      : joi.string().optional().allow( '' ).description( 'middle name of the person' ),
				'LastName'        : joi.string().optional().description( 'last name of the person' ),
				'EmailAddress'    : joi.string().optional().email().description( 'email address' ),
				'Password'        : joi.string().optional().description( 'password' ),
				'ProfileId'       : joi.number().optional().description( 'profile id' ),
				'StationId'       : joi.number().optional().description( 'station id' ),
				'AccountDisabled' : joi.boolean().optional().description( 'is user active?' ),
				'Stations'        : joi.array().optional().items( joi.number().min( 1 ) ).description( 'array of station id\'s' ),
				'CreatedBy'       : joi.number().optional().description( 'created by' ),
				'UpdatedBy'       : joi.number().optional().description( 'updated by' ),
				'created_at'      : joi.string().optional().description( 'created at' ),
				'updated_at'      : joi.string().optional().description( 'updated at' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const id       = request.params.Id;
				const payload  = request.payload;
				const response = yield userService.updateById( id, payload );

				if ( !response ) {
					return reply( boom.notFound() );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
