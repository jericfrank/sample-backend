'use strict';

const boom  = require( 'boom' );

const userService = require( '../../services/users/' );
const log         = require( '../../logging' );
const generoute   = require( '../../utils/generoute' );
const send        = require( '../../utils/send' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/users',
	'config' : {
		'description' : 'Get all users',
		'notes'       : 'Returns a list of users',
		'tags'        : [ 'api' ],

		'handler' : generoute( function* ( request, reply ) {
			try {
				const response = yield userService.getAll();
				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
