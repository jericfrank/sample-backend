'use strict';

exports.register = ( server, options, next ) => {
	server.route( [
		require( './create' ),
		require( './get-all' ),
		require( './update-by-id' ),
		require( './get-by-id' ),
		require( './delete-by-id' ),
		require( './forgot-password' ),
		require( './reset-password' ),
		require( './get-current-user' )
	] );

	next();
};

exports.register.attributes = {
	'name'    : 'users gateway',
	'version' : '1.0.0'
};
