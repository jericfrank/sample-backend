'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );
const joi  = require( 'joi' );

const authService = require( '../../services/auth/' );
const log         = require( '../../logging' );
const generoute   = require( '../../utils/generoute' );
const send        = require( '../../utils/send' );

module.exports = {
	'method' : 'POST',
	'path'   : '/api/refresh-token',
	'config' : {
		'description' : 'Generate new token',
		'notes'       : 'Returns a an object with token',
		'tags'        : [ 'api' ],
		'validate'    : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown()
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const headers  = request.headers;
				const response = yield authService.refreshToken( headers );

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					return reply( boom.badRequest( response.message ) );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );

				return reply( boom.badImplementation() );
			}
		} )
	}
};
