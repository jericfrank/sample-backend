'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );
const joi  = require( 'joi' );

const authService = use( 'services/auth/' );
const log         = use( 'logging' );
const generoute   = use( 'utils/generoute' );
const send        = use( 'utils/send' );

module.exports = {
	'method' : 'POST',
	'path'   : '/api/authenticate',
	'config' : {
		'description' : 'Authenticate a user',
		'notes'       : 'Returns a an object with token and a userId',
		'tags'        : [ 'api' ],
		'validate'    : {
			'payload' : {
				'UserName' : joi.string().required().description( 'username to identify the person' ),
				'Password' : joi.string().required().description( 'password' )
			}
		},

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'401' : { 'description' : 'The server has not found anything matching the Request' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' }
				}
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			const payload  = request.payload;

			try {
				const response = yield authService.authenticate( payload );
				if ( !response ) {
					return reply( boom.unauthorized( 'Invalid username or password' ) );
				}

				if ( _.has( response, 'accountDisabled' ) ) {
					log.info( response );
					return reply( boom.unauthorized( response.accountDisabled ) );
				}

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					return reply( boom.badRequest( response.message ) );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
