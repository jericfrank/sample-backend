'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );
const joi  = require( 'joi' );

const authService = use( 'services/auth/' );
const log         = use( 'logging' );
const generoute   = use( 'utils/generoute' );
const send        = use( 'utils/send' );

module.exports = {
	'method' : 'POST',
	'path'   : '/api/logout',
	'config' : {
		'description' : 'Invalidate token',
		'tags'        : [ 'api' ],
		'response'    : {
			'schema' : joi.object().keys( {
				'statusCode' : joi.number().description( 'response status' ).example( 200 ),
				'data'       : joi.object().keys( {
					'Operation' : joi.boolean().description( 'true if password is updated' )
				} )
			} )
		},
		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown()
		},

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'401' : { 'description' : 'Unauhtorized' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' }
				}
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const headers  = request.headers;
				const response = yield authService.logout( headers );

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					const error = _.first( response.errors );
					return reply( boom.badRequest( error.message ) );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
