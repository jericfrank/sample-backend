'use strict';

exports.register = ( server, options, next ) => {
	server.route( [
		require( './authenticate' ),
		require( './refresh-token' ),
		require( './logout' )
	] );

	next();
};

exports.register.attributes = {
	'name'    : 'auth gateway',
	'version' : '1.0.0'
};
