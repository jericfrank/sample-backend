'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );

const eventTemplateService = use( 'services/event-templates' );
const log                  = use( 'logging' );
const generoute            = use( 'utils/generoute' );
const send                 = use( 'utils/send' );
const validate             = use( 'gateways/event-templates/utils/validation/create' );
const statusDefinition     = use( 'utils/status-code-definition/get-all' );
const response             = use( 'gateways/event-templates/utils/response-schema/create' );

module.exports = {
	'method' : 'POST',
	'path'   : '/api/event-templates',
	'config' : {
		validate,
		response,
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Create an event template',
		'notes'       : 'HEADS UP: Property `TeamOne`, `TeamOneScore`, `TeamTwo`, `TeamTwoScore` and `EventDetails` can be omitted',
		'tags'        : [ 'api' ],
		'plugins'     : {
			'hapi-swagger' : {
				'responses' : statusDefinition
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const payload = request.payload;
				let response  = {};

				response = yield eventTemplateService.create( payload );

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					const error = _.first( response.errors );
					return reply( boom.badRequest( error.message ) );
				}

				return reply( send.created( response ) ).code( 201 );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
