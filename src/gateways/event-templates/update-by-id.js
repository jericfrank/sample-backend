'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );

const eventTemplateService = use( 'services/event-templates' );
const log                  = use( 'logging' );
const generoute            = use( 'utils/generoute' );
const send                 = use( 'utils/send' );
const validate             = use( 'gateways/event-templates/utils/validation/update-by-id' );


module.exports = {
	'method' : 'PUT',
	'path'   : '/api/event-templates/{Id}',
	'config' : {
		validate,
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Update an event template',
		'notes'       : `All <strong>fields</strong> in the payload are optional. <br />
			Basically you can omit the \`field(s)\` that don't needs to be update. <br />
			Returns a newly updated resource object. <br /><br />`,

		'tags'    : [ 'api' ],
		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'404' : { 'description' : 'The server has not found anything matching the Request' },
					'401' : { 'description' : 'The request requires user authentication' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' }
				}
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const id     = request.params.Id;
				const data   = request.payload;

				const response = yield eventTemplateService.updateById( id, data );

				if ( !response ) {
					return reply( boom.notFound( 'Resource not found' ) );
				}

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					const error = _.first( response.errors );
					return reply( boom.badRequest( error.message ) );
				}

				return reply( send.success( response ) ).code( 200 );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
