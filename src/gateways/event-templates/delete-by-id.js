'use strict';

const boom = require( 'boom' );
const joi  = require( 'joi' );

const eventTemplateService = use( 'services/event-templates' );
const log                  = use( 'logging' );
const generoute            = use( 'utils/generoute' );
const send                 = use( 'utils/send' );

module.exports = {
	'method' : 'DELETE',
	'path'   : '/api/event-templates/{Id}',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Delete an event template',
		'tags'        : [ 'api' ],
		'response'    : {
			'schema' : joi.object().keys( {
				'statusCode' : joi.number().description( 'response status' ).example( 200 ),
				'data'       : joi.object().keys( {
					'Operation' : joi.boolean().description( 'true if station is deleted | false if not' )
				} )
			} )
		},

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'422' : { 'description' : 'The request was well-formed but was unable to be followed due to semantic errors' },
					'404' : { 'description' : 'The server has not found anything matching the Request' },
					'401' : { 'description' : 'The request requires user authentication' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' }
				}
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'params' : {
				'Id' : joi.number().required().description( 'event id' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const id       = request.params.Id;
				const response = yield eventTemplateService.deleteById( id );

				if ( !response ) {
					return reply( boom.notFound( 'Resource not found' ) );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
