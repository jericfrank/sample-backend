'use strict';

exports.register = ( server, options, next ) => {
	server.route( [
		require( './create' ),
		require( './get-all' ),
		require( './update-by-id' ),
		require( './get-by-id' ),
		require( './delete-by-id' )
	] );

	next();
};

exports.register.attributes = {
	'name'    : 'event template gateway',
	'version' : '1.0.0'
};
