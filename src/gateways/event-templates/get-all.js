'use strict';

const boom = require( 'boom' );
const joi  = require( 'joi' );

const eventTemplateService = use( 'services/event-templates' );
const log                  = use( 'logging' );
const generoute            = use( 'utils/generoute' );
const send                 = use( 'utils/send' );
const response             = use( 'gateways/event-templates/utils/response-schema/get-all' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/event-templates',
	'config' : {
		response,
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Get all event templates',
		'tags'        : [ 'api' ],
		'plugins'     : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'401' : { 'description' : 'The request requires user authentication' }
				}
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),
			'query' : {
				'StationId' : joi.any().description( 'Station id' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const stationId  = request.query.StationId;
				const response = yield eventTemplateService.getAll( stationId );

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation( error ) );
			}
		} )
	}
};
