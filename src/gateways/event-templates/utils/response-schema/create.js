'use strict';

const Joi = require( 'joi' );

module.exports =  {
	'schema' : Joi.object().keys( {
		'statusCode' : Joi.number().description( 'response status' ).example( 200 ),
		'data'       : Joi.object().keys( {
			'StationId'       : Joi.number().example( 1 ),
			'CreatedBy'       : Joi.number().example( 1 ),
			'UpdatedBy'       : Joi.number().example( 1 ),
			'Id'              : Joi.number().example( 1 ),
			'EventTypeId'     : Joi.number().example( 1 ),
			'Title'           : Joi.string().example( 'NBA' ),
			'Start'           : Joi.any().example( '2017-02-06T06:04:00.000Z' ),
			'End'             : Joi.any().example( '2017-02-06T06:04:00.000Z' ),
			'TeamOne'         : Joi.string().allow( '' ).example( 'bulls' ),
			'TeamOneAbbrv'    : Joi.any().allow( '' ).example( 'bll' ),
			'TeamOneScore'    : Joi.number().allow( '' ).example( 1 ),
			'TeamTwo'         : Joi.string().allow( '' ).example( 'heat' ),
			'TeamTwoAbbrv'    : Joi.any().allow( '' ).example( 'he' ),
			'TeamTwoScore'    : Joi.number().allow( '' ).example( 1 ),
			'PartNo'          : Joi.any().example( 1 ),
			'GamePartId'      : Joi.any().example( 1 ),
			'GamePart'        : Joi.any(),
			'created_at'      : Joi.any().example( '2017-02-06T06:04:00.000Z' ),
			'updated_at'      : Joi.any().example( '0000-00-00 00:00:00' ),
			'EventSetting'    : Joi.any().optional().example( 'An object | null' ),
			'EventDetails'    : Joi.any().optional().example( 'An object | empty array' ),
			'ParentId'        : Joi.any().example( 1 ),
			'EventRecurrence' : Joi.any()
		} )
	} )
};
