'use strict';

const Joi = require( 'joi' );

const authorization = use( 'utils/validation/authorization' );

module.exports = {
	'headers' : authorization,
	'payload' : {
		'StationId'    : Joi.number().required().description( 'station id' ),
		'EventTypeId'  : Joi.number().required().description( 'type of event' ),
		'Title'        : Joi.string().required().description( 'title of event' ),
		'TeamOne'      : Joi.string().optional().description( 'team one title ( property can be omitted )' ),
		'TeamOneAbbrv' : Joi.any().optional().example( 'bll' ),
		'TeamOneScore' : Joi.number().optional().description( 'team one score ( property can be omitted )' ),
		'TeamTwo'      : Joi.string().optional().description( 'team two title ( property can be omitted )' ),
		'TeamTwoAbbrv' : Joi.any().optional().example( 'he' ),
		'TeamTwoScore' : Joi.number().optional().description( 'team two score ( property can be omitted )' ),
		'PartNo'       : Joi.number().optional().example( 1 ),
		'GamePartId'   : Joi.number().optional().example( 1 ),
		'EventSetting' : Joi.object().keys( {
			'FrequencyTypeId'    : Joi.number().min( 1 ).required().description( 'frequency type id' ),
			'FrequencyTypeValue' : Joi.number().min( 1 ).required().description( 'frequency type value' ),
			'Color'              : Joi.string().required().description( 'color' )
		} ),

		'EventDetails' : Joi.array().optional().items(  Joi.object().keys( {
			'Description' : Joi.string().example( 'some random string' )
		} ) )
	}
};
