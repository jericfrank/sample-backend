'use strict';

const Joi = require( 'joi' );

const authorization = use( 'utils/validation/authorization' );

module.exports = {
	'headers' : authorization,

	'params' : {
		'Id' : Joi.number().required().description( 'event id' )
	},

	'payload' : {
		'Id'           : Joi.number().optional().description( 'event id' ),
		'StationId'    : Joi.number().optional().description( 'station id' ),
		'EventTypeId'  : Joi.number().optional().description( 'type of event' ),
		'Title'        : Joi.string().optional().description( 'title of event' ),
		'TeamOne'      : Joi.string().optional().description( 'team one title' ),
		'TeamOneAbbrv' : Joi.string().optional().description( 'team\'s abbriviation' ),
		'TeamOneScore' : Joi.number().optional().description( 'team one score' ),
		'TeamTwo'      : Joi.string().optional().description( 'team two title' ),
		'TeamTwoAbbrv' : Joi.string().optional().description( 'team\'s abbriviation' ),
		'TeamTwoScore' : Joi.number().optional().description( 'team two score' ),
		'PartNo'       : Joi.number().optional().description( 'number part of game' ),
		'GamePartId'   : Joi.number().optional().description( 'game part identification' ),
		'EventSetting' : Joi.object().optional().keys( {
			'Id'                 : Joi.number().required().description( 'event settomg id' ),
			'EventId'            : Joi.number().required().description( 'type of event' ),
			'FrequencyTypeId'    : Joi.number().required().description( 'frequency type id' ),
			'FrequencyTypeValue' : Joi.number().required().description( 'frequency type value' ),
			'Color'              : Joi.string().required().description( 'color' ),
			'NoLineTextToExport' : Joi.number().optional().description( 'line' ),
			'FrequencyType'      : Joi.object().optional().description( 'frequency' )
		} ),
		'EventDetails' : Joi.array().optional().items(  Joi.object().keys( {
			'Id'          : Joi.number().optional().description( 'event details id' ),
			'EventId'     : Joi.number().optional().description( 'event id' ),
			'Description' : Joi.string().optional().description( 'content of event details' )
		} ) )
	}
};
