'use strict';

const boom = require( 'boom' );
const joi  = require( 'joi' );

const eventTypeService = use( 'services/event-types' );
const log              = use( 'logging' );
const generoute        = use( 'utils/generoute' );
const send             = use( 'utils/send' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/event-types',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Get all event types',
		'tags'        : [ 'api' ],
		'response'    : {
			'schema' : joi.object().keys( {
				'statusCode' : joi.number().description( 'response status' ).example( 200 ),
				'data'       : joi.array().items(
					joi.object().keys( {
						'CreatedBy'  : joi.number().example( 1 ),
						'UpdatedBy'  : joi.number().example( 1 ),
						'Id'         : joi.number().example( 1 ),
						'Name'       : joi.string().example( 'Static/Fixed' ),
						'created_at' : joi.any().example( '2017-02-06T06:04:00.000Z' ),
						'updated_at' : joi.any().example( '0000-00-00 00:00:00' )
					} )
				)
			} )
		},

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'401' : { 'description' : 'The request requires user authentication' }
				}
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown()
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const response = yield eventTypeService.getAll();
				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation( error ) );
			}
		} )
	}
};
