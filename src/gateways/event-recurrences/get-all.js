'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );
const joi  = require( 'joi' );

const eventRecurrenceService = use( 'services/event-recurrences' );
const log                = use( 'logging' );
const generoute          = use( 'utils/generoute' );
const send               = use( 'utils/send' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/event-recurrences',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Get all event recurrences',
		'tags'        : [ 'api' ],
		'plugins'     : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'401' : { 'description' : 'The request requires user authentication' }
				}
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'query' : {
				'EventId' : joi.number().description( 'event id' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const getEventId = _.partialRight( _.result, 'query.EventId' );
				const response   = yield eventRecurrenceService.getAll( getEventId( request ) );
				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation( error ) );
			}
		} )
	}
};
