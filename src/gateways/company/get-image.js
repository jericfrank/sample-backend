'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );

const companyService = use( 'services/company' );
const log            = use( 'logging' );
const generoute      = use( 'utils/generoute' );
const send           = use( 'utils/send' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/companies/logo',
	'config' : {
		'description' : 'Get company logo url',
		'tags'        : [ 'api' ],
		'plugins'     : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' }
				}
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const response = yield companyService.getAll();
				return reply( send.success( _.pick( response, 'CompanyLogo' ) ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation( error ) );
			}
		} )
	}
};
