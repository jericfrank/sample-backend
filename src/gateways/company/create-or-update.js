'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );
const joi  = require( 'joi' );

const companyService = use( 'services/company' );
const log            = use( 'logging' );
const generoute      = use( 'utils/generoute' );
const send           = use( 'utils/send' );

module.exports = {
	'method' : 'PUT',
	'path'   : '/api/companies',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Create or Update company info',
		'notes'       : `All <strong>fields</strong> in the payload are optional. <br />
			Basically you can omit the \`field(s)\` that don't needs to be created or updated. <br />
			Returns a company resource object. <br /><br />
			If you want to upload a photo ( base64 ), just use the \`ImageString\` property. <br />
			<strong>Example</strong> :

			{ "ImageString" : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4" }`,

		'tags'     : [ 'api' ],
		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'payload' : {
				'Name'          : joi.string().optional().description( 'name of company' ),
				'Address'       : joi.string().optional().description( 'address of company' ),
				'ContactPerson' : joi.string().optional().description( 'person of interest' ),
				'ContactNumber' : joi.string().optional().description( 'company contact number' ),
				'PricingType'   : joi.number().max( 2 ).optional().description( 'pricing type' ),
				'MaxLimit'      : joi.number().optional().description( 'limit' ),
				'CompanyLogo'   : joi.string().optional().description( 'profile id' ),
				'ImageString'   : joi.string().optional().description( 'image string' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			const payload = request.payload;

			try {
				const response = yield companyService.createOrUpdate( payload );

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					const error = _.first( response.errors );
					return reply( boom.badRequest( error.message ) );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
