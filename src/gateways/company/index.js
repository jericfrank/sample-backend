'use strict';

exports.register = ( server, options, next ) => {
	server.route( [
		require( './get-all' ),
		require( './create-or-update' ),
		require( './get-image' )
	] );

	next();
};

exports.register.attributes = {
	'name'    : 'company gateway',
	'version' : '1.0.0'
};
