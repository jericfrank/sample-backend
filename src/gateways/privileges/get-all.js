'use strict';

const boom = require( 'boom' );
const joi  = require( 'joi' );

const log               = require( '../../logging' );
const generoute         = require( '../../utils/generoute' );
const privilegesService = require( '../../services/privileges' );
const send              = require( '../../utils/send' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/privileges',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Get all privileges',
		'notes'       : 'This is an alias of ProfileModules',
		'tags'        : [ 'api' ],
		'validate'    : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'query' : {
				'profile' : joi.number().description( 'profile id' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			let profile = request.query.profile;

			if ( !profile ) {
				profile = null;
			}

			try {
				const response = yield privilegesService.getAll( profile );
				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
