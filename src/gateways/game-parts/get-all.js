'use strict';

const boom = require( 'boom' );
const joi  = require( 'joi' );

const gamePartsService = use( 'services/game-parts' );
const log              = use( 'logging' );
const generoute        = use( 'utils/generoute' );
const send             = use( 'utils/send' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/game-parts',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Get all event game parts',
		'tags'        : [ 'api' ],

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'401' : { 'description' : 'The request requires user authentication' }
				}
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown()
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const response = yield gamePartsService.getAll();
				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation( error ) );
			}
		} )
	}
};
