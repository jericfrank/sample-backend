'use strict';

const Joi = require( 'joi' );

const authorization = use( 'utils/validation/authorization' );

module.exports = {
	'headers' : authorization,

	'params' : {
		'Id' : Joi.number().required().description( 'event id' )
	},

	'payload' : {
		'Id'           : Joi.number().required().description( 'event id' ),
		'StationId'    : Joi.number().required().description( 'name use to identify station' ),
		'EventTypeId'  : Joi.number().required().description( 'type of event' ),
		'Title'        : Joi.string().required().description( 'title of event' ),
		'Start'        : Joi.date().raw().required().description( 'start date' ),
		'End'          : Joi.date().min( Joi.ref( 'Start' ) ).raw().required().description( 'end date' ),
		'TeamOne'      : Joi.string().optional().description( 'team one title' ),
		'TeamOneAbbrv' : Joi.string().optional().example( 'bll' ),
		'TeamOneScore' : Joi.number().optional().description( 'team one score' ),
		'TeamTwo'      : Joi.string().optional().description( 'team two title' ),
		'TeamTwoAbbrv' : Joi.string().optional().example( 'he' ),
		'TeamTwoScore' : Joi.number().optional().description( 'team two score' ),
		'PartNo'       : Joi.any().optional().example( 1 ),
		'GamePartId'   : Joi.any().optional().example( 1 ),
		'created_at'   : Joi.any().optional().example( '2017-02-06T06:04:00.000Z' ),
		'updated_at'   : Joi.any().optional().example( '0000-00-00 00:00:00' ),
		'CreatedBy'    : Joi.number().optional().example( 1 ),
		'UpdatedBy'    : Joi.number().optional().example( 1 ),
		'EventSetting' : Joi.object().keys( {
			'FrequencyTypeId'    : Joi.number().required().description( 'frequency type id' ),
			'FrequencyTypeValue' : Joi.number().required().description( 'frequency type value' ),
			'Color'              : Joi.string().required().description( 'color' ),
			'NoLineTextToExport' : Joi.number().required().description( 'line' ),
			'CreatedBy'          : Joi.number().optional().example( 1 ),
			'UpdatedBy'          : Joi.number().optional().example( 1 ),
			'FrequencyType'      : Joi.object().optional(),
			'created_at'         : Joi.any().optional().example( '2017-02-06T06:04:00.000Z' ),
			'updated_at'         : Joi.any().optional().example( '0000-00-00 00:00:00' ),
			'Id'                 : Joi.number().required().description( 'event id' ),
			'EventId'            : Joi.number().required().description( 'type of event' )
		} ),
		'EventDetails' : Joi.array().optional().items(  Joi.object().keys( {
			'Id'          : Joi.any().example( 'some random string' ),
			'EventId'     : Joi.number().example( 1 ),
			'Description' : Joi.string().example( 'some random string' ),
			'CreatedBy'   : Joi.number().optional().example( 0 ),
			'UpdatedBy'   : Joi.number().optional().example( 0 ),
			'created_at'  : Joi.any().optional().example( 'some random string' ),
			'updated_at'  : Joi.any().optional().example( 'some random string' )
		} ) ),
		'GamePart'        : Joi.any(),
		'ParentId'        : Joi.number().optional( 'Event Id if recurring' ),
		'EventRecurrence' : Joi.object().optional().keys( {
			'Id'                  : Joi.number().optional().description( 'Event Recurrence id' ),
			'EventId'             : Joi.number().optional().description( 'Event id' ),
			'RepeatOption'        : Joi.number().required().description( 'Type of reccurence' ),
			'DayOption'           : Joi.number().optional().description( 'Is it every? or every weekday?' ),
			'RepeatEvery'         : Joi.number().optional().description( 'how days it should repeat' ),
			'Start'               : Joi.date().raw().required().description( 'start date' ),
			'End'                 : Joi.date().optional().allow( null ).min( Joi.ref( 'Start' ) ).raw().optional().description( 'end date' ),
			'EndAfter'            : Joi.number().optional().allow( null ).description( 'number of ocurrences' ),
			'Weekdays'            : Joi.array().optional().items( Joi.number().required() ),
			'MonthOption'         : Joi.number().optional().description( 'Is it every day? or every n day' ),
			'Day'                 : Joi.number().optional().description( 'Day from 01-31' ),
			'MonthWeekOption'     : Joi.number().optional().description( 'First, Second, Third, Fourth or Last' ),
			'MonthWeekdaysOption' : Joi.number().optional().description( 'From sunday to saturday' )
		} )
	}
};
