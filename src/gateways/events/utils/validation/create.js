'use strict';

const Joi = require( 'joi' );

const authorization = use( 'utils/validation/authorization' );

module.exports = {
	'headers' : authorization,
	'payload' : {
		'StationId'    : Joi.number().required().description( 'name use to identify station' ),
		'EventTypeId'  : Joi.number().required().description( 'type of event' ),
		'Title'        : Joi.string().required().description( 'title of event' ),
		'Start'        : Joi.date().raw().required().description( 'start date' ),
		'End'          : Joi.date().min( Joi.ref( 'Start' ) ).raw().required().description( 'end date' ),
		'TeamOne'      : Joi.string().optional().description( 'team one title ( property can be omitted )' ),
		'TeamOneAbbrv' : Joi.any().optional().example( 'bll' ),
		'TeamOneScore' : Joi.number().optional().description( 'team one score ( property can be omitted )' ),
		'TeamTwo'      : Joi.string().optional().description( 'team two title ( property can be omitted )' ),
		'TeamTwoAbbrv' : Joi.any().optional().example( 'he' ),
		'TeamTwoScore' : Joi.number().optional().description( 'team two score ( property can be omitted )' ),
		'PartNo'       : Joi.number().optional().example( 1 ),
		'GamePartId'   : Joi.number().optional().example( 1 ),
		'EventSetting' : Joi.object().keys( {
			'FrequencyTypeId'    : Joi.number().min( 1 ).required().description( 'frequency type id' ),
			'FrequencyTypeValue' : Joi.number().min( 1 ).required().description( 'frequency type value' ),
			'Color'              : Joi.string().required().description( 'color' )
		} ),

		'EventDetails' : Joi.array().optional().items(  Joi.object().keys( {
			'Description' : Joi.string().example( 'some random string' )
		} ) ),

		'EventRecurrence' : Joi.object().optional().keys( {
			'RepeatOption'        : Joi.number().required().description( 'Type of reccurence' ),
			'DayOption'           : Joi.number().optional().description( 'Is it every? or every weekday?' ),
			'RepeatEvery'         : Joi.number().optional().description( 'how days it should repeat' ),
			'Start'               : Joi.date().raw().required().description( 'start date' ),
			'End'                 : Joi.date().optional().allow( null ).min( Joi.ref( 'Start' ) ).raw().optional().description( 'end date' ),
			'EndAfter'            : Joi.number().optional().allow( null ).description( 'number of ocurrences' ),
			'Weekdays'            : Joi.array().optional().items( Joi.number().required() ),
			'MonthOption'         : Joi.number().optional().description( 'Is it every day? or every n day' ),
			'Day'                 : Joi.number().optional().description( 'Day from 01-31' ),
			'MonthWeekOption'     : Joi.number().optional().description( 'First, Second, Third, Fourth or Last' ),
			'MonthWeekdaysOption' : Joi.number().optional().description( 'From sunday to saturday' )
		} )
	}
};
