'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );

const eventService     = use( 'services/events' );
const log              = use( 'logging' );
const generoute        = use( 'utils/generoute' );
const send             = use( 'utils/send' );
const validate         = use( 'gateways/events/utils/validation/create' );
const statusDefinition = use( 'utils/status-code-definition/get-all' );
const response         = use( 'gateways/events/utils/response-schema/create' );

module.exports = {
	'method' : 'POST',
	'path'   : '/api/events',
	'config' : {
		validate,
		response,
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Create an event',
		'notes'       : 'HEADS UP: Property `TeamOne`, `TeamOneScore`, `TeamTwo`, `TeamTwoScore` and `EventDetails` can be omitted',
		'tags'        : [ 'api' ],
		'plugins'     : {
			'hapi-swagger' : {
				'responses' : statusDefinition
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const payload = request.payload;
				let response  = {};

				if ( _.isObject( payload.EventRecurrence ) ) {
					response = yield eventService.createWithRecurrence( payload );
				} else {
					response = yield eventService.create( payload );
				}

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					const error = _.first( response.errors );
					return reply( boom.badRequest( error.message ) );
				}

				return reply( send.created( response ) ).code( 201 );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
