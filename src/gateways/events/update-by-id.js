'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );

const eventService = use( 'services/events' );
const log          = use( 'logging' );
const generoute    = use( 'utils/generoute' );
const send         = use( 'utils/send' );
const validate     = use( 'gateways/events/utils/validation/update-by-id' );
const response     = use( 'gateways/events/utils/response-schema/update-by-id' );


module.exports = {
	'method' : 'PUT',
	'path'   : '/api/events/{Id}',
	'config' : {
		validate,
		response,
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Update an event',
		'notes'       : 'HEADS UP: Property `TeamOne`, `TeamOneScore`, `TeamTwo`, `TeamTwoScore` and `EventDetails` can be omitted',
		'tags'        : [ 'api' ],
		'plugins'     : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'404' : { 'description' : 'The server has not found anything matching the Request' },
					'401' : { 'description' : 'The request requires user authentication' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' }
				}
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const id     = request.params.Id;
				const data   = request.payload;
				let response = {};

				if ( _.isObject( data.EventRecurrence ) ) {
					response = yield eventService.updateWithRecurrence( id, data );
				} else {
					response = yield eventService.updateById( id, data );
				}

				if ( !response ) {
					return reply( boom.notFound( 'Resource not found' ) );
				}

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					const error = _.first( response.errors );
					return reply( boom.badRequest( error.message ) );
				}

				const io = request.server.plugins[ 'hapi-io' ].io;
				io.emit( 'event', response );

				return reply( send.success( response ) ).code( 200 );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
