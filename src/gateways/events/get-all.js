'use strict';

const boom = require( 'boom' );
const joi  = require( 'joi' );

const eventService = use( 'services/events' );
const log          = use( 'logging' );
const generoute    = use( 'utils/generoute' );
const send         = use( 'utils/send' );
const response     = use( 'gateways/events/utils/response-schema/get-all' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/events',
	'config' : {
		response,
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Get all events',
		'tags'        : [ 'api' ],
		'plugins'     : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'401' : { 'description' : 'The request requires user authentication' }
				}
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'query' : {
				'Start'   : joi.any().description( 'Start date' ),
				'End'     : joi.any().description( 'End date' ),
				'Station' : joi.number().description( 'Station id' )
			}

		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const start         = request.query.Start;
				const end           = request.query.End;
				const station       = request.query.Station;
				const response      = yield eventService.getAll( start, end, station );

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation( error ) );
			}
		} )
	}
};
