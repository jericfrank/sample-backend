'use strict';

const boom = require( 'boom' );
const joi  = require( 'joi' );

const eventService = use( 'services/events' );
const log          = use( 'logging' );
const generoute    = use( 'utils/generoute' );
const send         = use( 'utils/send' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/events/{EventId}/settings',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Get an event settings specified by event id',
		'tags'        : [ 'api' ],
		'response'    : {
			'schema' : joi.object().keys( {
				'statusCode' : joi.number().description( 'response status' ).example( 200 ),
				'data'       : joi.object().keys( {
					'CreatedBy'          : joi.number().description( 'created by whom' ),
					'UpdatedBy'          : joi.number().description( 'updated by whom' ),
					'Id'                 : joi.number().description( 'user id' ),
					'EventId'            : joi.number().description( 'user distinct name' ),
					'FrequencyTypeId'    : joi.number().description( 'name use to identify the person' ),
					'FrequencyTypeValue' : joi.number().description( 'middle name of the person' ),
					'Color'              : joi.string().description( 'last name of the person' ),
					'NoLineTextToExport' : joi.number().description( 'email address' ),
					'updated_at'         : joi.any().description( 'updated date' ),
					'created_at'         : joi.any().description( 'created date' )
				} )
			} )
		},

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' },
					'401' : { 'description' : 'The request requires user authentication' },
					'404' : { 'description' : 'The server has not found anything matching the Request' }
				},
				'deprecated' : true
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'params' : {
				'EventId' : joi.number().required().description( 'event id' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			const id = request.params.EventId;
			try {
				const response = yield eventService.getSettingByEventId( id );

				if ( !response ) {
					return reply( boom.notFound( 'Resource not found' ) );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
