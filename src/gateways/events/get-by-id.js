'use strict';

const boom = require( 'boom' );
const joi  = require( 'joi' );

const eventService = use( 'services/events' );
const log          = use( 'logging' );
const generoute    = use( 'utils/generoute' );
const send         = use( 'utils/send' );
const response     = use( 'gateways/events/utils/response-schema/get-by-id' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/events/{Id}',
	'config' : {
		response,
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Get an event specified by id',
		'tags'        : [ 'api' ],
		'plugins'     : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' },
					'401' : { 'description' : 'The request requires user authentication' },
					'404' : { 'description' : 'The server has not found anything matching the Request' }
				}
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'params' : {
				'Id' : joi.number().required().description( 'event id' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			const id = request.params.Id;
			try {
				const response = yield eventService.getById( id );

				if ( !response ) {
					return reply( boom.notFound( 'Resource not found' ) );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
