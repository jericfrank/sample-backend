'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );
const joi  = require( 'joi' );

const eventService = use( 'services/events' );
const log          = use( 'logging' );
const generoute    = use( 'utils/generoute' );
const send         = use( 'utils/send' );


module.exports = {
	'method' : 'PUT',
	'path'   : '/api/events/{EventId}/settings/{Id}',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Update an event setting',
		'tags'        : [ 'api' ],
		'response'    : {
			'schema' : joi.object().keys( {
				'statusCode' : joi.number().description( 'response status' ).example( 200 ),
				'data'       : joi.object().keys( {
					'CreatedBy'          : joi.number().description( 'created by whom' ),
					'UpdatedBy'          : joi.number().description( 'updated by whom' ),
					'Id'                 : joi.number().description( 'setting id' ),
					'EventId'            : joi.number().description( 'user distinct name' ),
					'FrequencyTypeId'    : joi.number().description( 'name use to identify the person' ),
					'FrequencyTypeValue' : joi.number().description( 'middle name of the person' ),
					'Color'              : joi.string().description( 'last name of the person' ),
					'NoLineTextToExport' : joi.number().description( 'email address' ),
					'updated_at'         : joi.any().description( 'updated date' ),
					'created_at'         : joi.any().description( 'created date' )
				} )
			} )
		},

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'404' : { 'description' : 'The server has not found anything matching the Request' },
					'401' : { 'description' : 'The request requires user authentication' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' }
				},
				'deprecated' : true
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'payload' : {
				'Id'                 : joi.number().required().description( 'event setting id' ),
				'EventId'            : joi.number().required().description( 'on what event' ),
				'FrequencyTypeId'    : joi.number().required().description( 'frequency type id' ),
				'FrequencyTypeValue' : joi.number().required().description( 'frequency type value' ),
				'Color'              : joi.string().required().description( 'color' ),
				'NoLineTextToExport' : joi.number().required().description( 'no of line should text be exported' )
			},

			'params' : {
				'EventId' : joi.number().required().description( 'on what event' ),
				'Id'      : joi.number().required().description( 'event setting id' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const id       = request.params.Id;
				const data     = request.payload;
				const response = yield eventService.updateSetting( id, data );

				if ( !response ) {
					return reply( boom.notFound( 'Resource not found' ) );
				}

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					const error = _.first( response.errors );
					return reply( boom.badRequest( error.message ) );
				}

				return reply( send.success( response ) ).code( 200 );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
