'use strict';

exports.register = ( server, options, next ) => {
	server.route( [
		require( './create-setting' ),
		require( './update-setting' ),
		require( './get-setting-by-event-id' ),
		require( './create' ),
		require( './get-all' ),
		require( './get-by-id' ),
		require( './update-by-id' ),
		require( './delete-by-id' ),
		require( './delete-by-series' )
	] );

	next();
};

exports.register.attributes = {
	'name'    : 'events gateway',
	'version' : '1.0.0'
};
