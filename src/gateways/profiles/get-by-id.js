'use strict';

const boom  = require( 'boom' );
const joi   = require( 'joi' );

const log            = require( '../../logging' );
const generoute      = require( '../../utils/generoute' );
const send           = require( '../../utils/send' );
const profileService = require( '../../services/profiles' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/profiles/{Id}',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Get a profile specified by id',
		'notes'       : 'Returns a profile object',
		'tags'        : [ 'api' ],
		'validate'    : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'params' : {
				'Id' : joi.number().required().description( 'profile id' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			const id = request.params.Id;

			try {
				const response = yield profileService.getById( id );

				if ( !response ) {
					return reply( boom.notFound( 'Resource not found' ) );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
