'use strict';

exports.register = ( server, options, next ) => {
	server.route( [
		require( './get-all' ),
		require( './get-by-id' )
	] );

	next();
};

exports.register.attributes = {
	'name'    : 'profile gateway',
	'version' : '1.0.0'
};
