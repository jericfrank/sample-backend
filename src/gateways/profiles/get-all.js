'use strict';

const boom = require( 'boom' );
const joi  = require( 'joi' );

const log            = require( '../../logging' );
const generoute      = require( '../../utils/generoute' );
const profileService = require( '../../services/profiles' );
const send           = require( '../../utils/send' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/profiles',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Get all profiles',
		'notes'       : 'Returns a list of profiles',
		'tags'        : [ 'api' ],
		'validate'    : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown()

		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const response = yield profileService.getAll();
				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
