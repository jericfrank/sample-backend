'use strict';

exports.register = ( server, options, next ) => {
	server.route( [
		require( './get-all' )
	] );

	next();
};

exports.register.attributes = {
	'name'    : 'event details gateway',
	'version' : '1.0.0'
};
