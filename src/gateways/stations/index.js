'use strict';

exports.register = ( server, options, next ) => {
	server.route( [
		require( './get-all' ),
		require( './get-by-id' ),
		require( './delete-by-id' ),
		require( './update-by-id' ),
		require( './create' ),
		require( './update-calendar-setting' ),
		require( './create-config' ),
		require( './get-config' ),
		require( './delete-config' )
	] );

	next();
};

exports.register.attributes = {
	'name'    : 'stations gateway',
	'version' : '1.0.0'
};
