'use strict';

const boom = require( 'boom' );
const joi  = require( 'joi' );

const stationService = use( 'services/stations' );
const log            = use( 'logging' );
const generoute      = use( 'utils/generoute' );
const send           = use( 'utils/send' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/stations',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Get all stations',
		'tags'        : [ 'api' ],
		'response'    : {
			'schema' : joi.object().keys( {
				'statusCode' : joi.number().description( 'response status' ).example( 200 ),
				'data'       : joi.array().items(
					joi.object().keys( {
						'Id'              : joi.number().example( 1 ),
						'Name'            : joi.string().example( 'MWFB' ),
						'Location'        : joi.string().example( 'Wisconsin' ),
						'Logo'            : joi.any().example( 'path-to-image.jpg' ),
						'IsActive'        : joi.boolean(),
						'CreatedBy'       : joi.number().example( 1 ),
						'UpdatedBy'       : joi.number().example( 1 ),
						'created_at'      : joi.any().example( '2017-02-06T06:04:00.000Z' ),
						'updated_at'      : joi.any().example( '0000-00-00 00:00:00' ),
						'CalendarSetting' : joi.any().optional().example( 'An object | null' )
					} )
				)
			} )
		},

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' },
					'401' : { 'description' : 'The request requires user authentication' }
				}
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown()
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const response = yield stationService.getAll();
				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation( error ) );
			}
		} )
	}
};
