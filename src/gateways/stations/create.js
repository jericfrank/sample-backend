'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );
const joi  = require( 'joi' );

const stationService = use( 'services/stations' );
const log            = use( 'logging' );
const generoute      = use( 'utils/generoute' );
const send           = use( 'utils/send' );

module.exports = {
	'method' : 'POST',
	'path'   : '/api/stations',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Create station',
		'notes'       : 'HEADS UP: If "Logo" is set to empty string this will be converted to null',
		'tags'        : [ 'api' ],
		'response'    : {
			'schema' : joi.object().keys( {
				'statusCode' : joi.number().description( 'response status' ).example( 200 ),
				'data'       : joi.object().keys( {
					'Id'              : joi.number().example( 1 ),
					'Name'            : joi.string().example( 'MWFB' ),
					'Location'        : joi.string().example( 'Wisconsin' ),
					'Logo'            : joi.any().example( 'path-to-image.jpg' ),
					'CreatedBy'       : joi.number().example( 1 ),
					'UpdatedBy'       : joi.number().example( 1 ),
					'created_at'      : joi.any().example( '2017-02-06T06:04:00.000Z' ),
					'updated_at'      : joi.any().example( '0000-00-00 00:00:00' ),
					'CalendarSetting' : joi.object().optional().keys( {
						'Id'              : joi.number().example( 1 ),
						'StationId'       : joi.number().example( 1 ),
						'StorageLocation' : joi.string().example( 'gs-crossover-1489038367' ),
						'StartOfWeek'     : joi.number().example( 1 ),
						'CreatedBy'       : joi.number().example( 0 ),
						'UpdatedBy'       : joi.number().example( 0 ),
						'created_at'      : joi.any().example( '2017-02-06T06:04:00.000Z' ),
						'updated_at'      : joi.any().example( '0000-00-00 00:00:00' )
					} )
				} )
			} )
		},

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' },
					'401' : { 'description' : 'The request requires user authentication' }
				}
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'payload' : {
				'Name'            : joi.string().required().description( 'name use to identify station' ),
				'Location'        : joi.string().required().description( 'location' ),
				'Logo'            : joi.string().allow( null ).allow( '' ).description( 'image logo' ),
				'CalendarSetting' : joi.object().required().keys( {
					'StartOfWeek' : joi.number().required().description( '1 for monday' ).example( 1 )
				} )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			const payload = request.payload;

			if ( !payload.Logo ) {
				payload.Logo = null;
			}

			try {
				const userId   = request.auth.credentials.sub;
				const response = yield stationService.create( userId, payload );

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					const error = _.first( response.errors );
					return reply( boom.badRequest( error.message ) );
				}

				return reply( send.created( response ) ).code( 201 );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
