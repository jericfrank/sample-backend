'use strict';

const boom = require( 'boom' );
const joi  = require( 'joi' );

const stationService = use( 'services/stations' );
const log            = use( 'logging' );
const generoute      = use( 'utils/generoute' );
const send           = use( 'utils/send' );

module.exports = {
	'method' : 'GET',
	'path'   : '/api/stations/{Id}',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Get a station specified by id',
		'tags'        : [ 'api' ],
		'response'    : {
			'schema' : joi.object().keys( {
				'statusCode' : joi.number().description( 'response status' ).example( 200 ),
				'data'       : joi.object().keys( {
					'Id'              : joi.number().example( 1 ),
					'Name'            : joi.string().example( 'MWFB' ),
					'Location'        : joi.string().example( 'Wisconsin' ),
					'Logo'            : joi.any().example( 'path-to-image.jpg' ),
					'IsActive'        : joi.boolean().example( true ),
					'CreatedBy'       : joi.number().example( 1 ),
					'UpdatedBy'       : joi.number().example( 1 ),
					'created_at'      : joi.any().example( '2017-02-06T06:04:00.000Z' ),
					'updated_at'      : joi.any().example( '0000-00-00 00:00:00' ),
					'CalendarSetting' : joi.any().optional().example( 'An object | null' )
				} )
			} )
		},

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' },
					'401' : { 'description' : 'The request requires user authentication' },
					'404' : { 'description' : 'The server has not found anything matching the Request' }
				}
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'params' : {
				'Id' : joi.number().required().description( 'station id' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			const id = request.params.Id;
			try {
				const response = yield stationService.getById( id );

				if ( !response ) {
					return reply( boom.notFound( 'Resource not found' ) );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
