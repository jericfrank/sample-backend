'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );
const joi  = require( 'joi' );

const stationService = use( 'services/stations' );
const log            = use( 'logging' );
const generoute      = use( 'utils/generoute' );
const send           = use( 'utils/send' );

module.exports = {
	'method' : 'POST',
	'path'   : '/api/stations/{Id}/config',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Create station config',
		'tags'        : [ 'api' ],
		'response'    : {
			'schema' : joi.object().keys( {
				'statusCode' : joi.number().description( 'response status' ).example( 200 ),
				'data'       : joi.object().keys( {
					'Name' : joi.string().example( 'ADUMMYSTATION4' ),
					'Text' : joi.string().example( 'key=value' )
				} )
			} )
		},

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' },
					'401' : { 'description' : 'The request requires user authentication' }
				}
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'params' : {
				'Id' : joi.number().required().description( 'station id' )
			},

			'payload' : {
				'Text' : joi.string().required().description( 'some key value ' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			try {
				const stationId = request.params.Id;
				const payload   = request.payload;

				const data = _.assign( {}, payload, {
					'StationId' : stationId
				} );

				const response = yield stationService.createConfig( data );

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					const error = _.first( response.errors );
					return reply( boom.badRequest( error.message ) );
				}

				return reply( send.created( response ) ).code( 201 );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
