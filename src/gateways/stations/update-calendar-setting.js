'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );
const joi  = require( 'joi' );

const stationService = use( 'services/stations' );
const log            = use( 'logging' );
const generoute      = use( 'utils/generoute' );
const send           = use( 'utils/send' );

module.exports = {
	'method' : 'PUT',
	'path'   : '/api/stations/{Id}/calendar-setting',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Update a stations\'s calendar setting',
		'tags'        : [ 'api' ],
		'response'    : {
			'schema' : joi.object().keys( {
				'statusCode' : joi.number().description( 'response status' ).example( 200 ),
				'data'       : joi.object().keys( {
					'Id'              : joi.number().example( 1 ),
					'StationId'       : joi.number().example( 1 ),
					'StorageLocation' : joi.string().example( 'gs-abafm-1489118462' ),
					'StartOfWeek'     : joi.number().example( 1 ),
					'CreatedBy'       : joi.number().example( 1 ),
					'UpdatedBy'       : joi.number().example( 1 ),
					'created_at'      : joi.any().example( '2017-02-06T06:04:00.000Z' ),
					'updated_at'      : joi.any().example( '0000-00-00 00:00:00' )
				} )
			} )
		},

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' },
					'401' : { 'description' : 'The request requires user authentication' },
					'404' : { 'description' : 'The server has not found anything matching the Request' }
				},
				'deprecated' : true
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'params' : {
				'Id' : joi.number().required().description( 'calendar setting\'s id' )
			},

			'payload' : {
				'StartOfWeek' : joi.number().required().description( '1 for monday' ).example( 1 )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			const id      = request.params.Id;
			const payload = request.payload;

			try {
				const response = yield stationService.updateCalendarSetting( id, payload );

				if ( !response ) {
					return reply( boom.notFound( 'Resource not found' ) );
				}

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					const error = _.first( response.errors );
					return reply( boom.badRequest( error.message ) );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
