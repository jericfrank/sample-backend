'use strict';

const _    = require( 'lodash' );
const boom = require( 'boom' );
const joi  = require( 'joi' );

const stationService = use( 'services/stations' );
const log            = use( 'logging' );
const generoute      = use( 'utils/generoute' );
const send           = use( 'utils/send' );

module.exports = {
	'method' : 'DELETE',
	'path'   : '/api/stations/{Id}',
	'config' : {
		'auth'        : { 'strategy' : 'jwt' },
		'description' : 'Delete a station',
		'notes'       : 'Returns success object',
		'tags'        : [ 'api' ],
		'response'    : {
			'schema' : joi.object().keys( {
				'statusCode' : joi.number().description( 'response status' ).example( 200 ),
				'data'       : joi.object().keys( {
					'Operation' : joi.boolean().description( 'true if station is deleted | false if not' )
				} )
			} )
		},

		'plugins' : {
			'hapi-swagger' : {
				'responses' : {
					'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
					'422' : { 'description' : 'The request was well-formed but was unable to be followed due to semantic errors' },
					'404' : { 'description' : 'The server has not found anything matching the Request' },
					'401' : { 'description' : 'The request requires user authentication' },
					'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' }
				}
			}
		},

		'validate' : {
			'headers' : joi.object( {
				'authorization' : joi.string().required()
			} ).unknown(),

			'params' : {
				'Id' : joi.number().required().description( 'station id' )
			}
		},

		'handler' : generoute( function* ( request, reply ) {
			const id = request.params.Id;

			try {
				const response = yield stationService.deleteById( id );

				if ( !response ) {
					return reply( boom.notFound( 'Resource not found' ) );
				}

				if ( _.has( response, 'message' ) ) {
					log.info( response );
					return reply( boom[ response.errorType ]( response.message ) );
				}

				return reply( send.success( response ) );
			} catch ( error ) {
				log.fatal( error );
				return reply( boom.badImplementation() );
			}
		} )
	}
};
