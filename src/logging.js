'use strict';

const Bunyan = require( 'bunyan' );

module.exports = Bunyan.createLogger( {
	'name'    : 'Project Greasy API',
	'streams' : [ {
		'level'  : 'debug',
		'stream' : process.stdout // log INFO and above to stdout
	} ]
} );
