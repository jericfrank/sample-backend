'use strict';

const Hapi = require( 'hapi' );

const config    = require( './config' );
const log       = require( './logging' );
const models    = require( './models' );
const generoute = require( './utils/generoute' );

const initServer = () => {
	const server = new Hapi.Server( {
		'connections' : {
			'router' : {
				'stripTrailingSlash' : true
			}
		}
	} );

	server.connection( {
		'host'   : config.app.host,
		'port'   : config.app.port,
		'routes' : {
			'cors' : {
				'additionalHeaders' : [
					'Origin'
				]
			}
		}
	} );

	return Promise.resolve( server );
};

const initDevelopmentSetup = ( server ) => {
	return new Promise( ( resolve, reject ) => {
		server.register( [
			require( 'inert' ),
			require( 'vision' ), {
			'register' : require( 'hapi-swagger' ),
			'options'  : config.swagger
		} ], err => {
			if ( err ) {
				log.warn( err, 'Unable to setup Swagger API documentation' );
				return reject( err );
			}

			server.route( {
				'method' : 'GET',
				'path'   : '/api',

				handler ( request, reply ) {
					reply( {
						'statusCode' : 404,
						'error'      : 'Not Found'
					} ).code( 404 );
				}
			} );

			server.views( {
				'engines' : { 'html' : require( 'handlebars' ) },
				'path'    : __dirname
			} );

			server.route( {
				'method' : 'GET',
				'path'   : '/api/test-socket',

				handler ( request, reply ) {
					const host = request.headers.host || 'localhost:9001';
					reply.view( 'test-socket', { host } );
				}
			} );

			return resolve( true );
		} );
	} );
};

const initApplicationMiddleware = ( server ) => {
	return new Promise( ( resolve, reject ) => {
		server.register( require( 'hapi-auth-jwt2' ), err => {
			if ( err ) {
				log.fatal( err, 'Unable to register JWT plugin' );
				return reject( err );
			}

			server.auth.strategy( 'jwt', 'jwt', {
				'key'           : config.app.jwtKey,
				'verifyFunc'    : require( './utils/jwt-validate' ),
				'errorFunc'     : require( './utils/jwt-tokenhandler' ),
				'verifyOptions' : { 'algorithms' : [ 'HS256' ] }
			} );

			server.register(
				{
					'register' : use( 'utils/route-acl' ),
					'options'  : {
						'permissionFunc' : use( 'utils/permission' )
					}
				}
			);

			return server.register(
				{
					'register' : require( 'hapi-io' ),
					'options'  : {
						'socketio' : {
							'path' : '/api/socket.io'
						}
					}
				}, err => {
					if ( err ) {
						log.fatal( err, 'Unable to register plugins' );
						return reject( err );
					}

					return resolve( true );
			} );
		} );
	} );
};

const initGateways = ( server ) => {
	return new Promise( ( resolve, reject ) => {
		server.register( [
			{ 'register' : require( './gateways/users' ) },
			{ 'register' : require( './gateways/auth' ) },
			{ 'register' : require( './gateways/profiles' ) },
			{ 'register' : require( './gateways/privileges' ) },
			{ 'register' : require( './gateways/stations' ) },
			{ 'register' : require( './gateways/events' ) },
			{ 'register' : require( './gateways/event-types' ) },
			{ 'register' : require( './gateways/event-frequencies' ) },
			{ 'register' : require( './gateways/event-details' ) },
			{ 'register' : require( './gateways/game-parts' ) },
			{ 'register' : require( './gateways/event-recurrences' ) },
			{ 'register' : require( './gateways/event-templates' ) },
			{ 'register' : require( './gateways/company' ) }
			], err => {
				if ( err ) {
					log.fatal( err, 'Unable to register plugins' );
					return reject( err );
				}

				return resolve( true );
		} );
	} );
};

const startServer = ( server ) => {
	return new Promise( ( resolve, reject ) => {
		server.start( err => {
			if ( err ) {
				log.fatal( err, 'Unable to start server' );
				return reject( err );
			}

			return resolve( true );
		} );
	} );
};

const init = generoute( function* () {
	const server = yield initServer();

	// Swagger API Documentation
	if ( process.env.NODE_ENV !== 'production' ) {
		yield initDevelopmentSetup( server );
	}

	yield initApplicationMiddleware( server );
	yield models.sequelize.sync();
	yield initGateways( server );
	yield startServer( server );

	return server;
} );


module.exports = init()
	.then( ( server ) => {
		log.info( `Project Greasy API is running at: ${server.info.uri}` );
		return server;
	} )
	.catch( error => {
		throw new Error( error );
	} );
