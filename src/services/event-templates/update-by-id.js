'use strict';

const _ = require( 'lodash' );

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const EventTemplate        = models.EventTemplate;
const EventSettingTemplate = models.EventSettingTemplate;
const EventDetailTemplate  = models.EventDetailTemplate;
const ValidationError      = models.Sequelize.ValidationError;

const createDetailsGenerator = function* ( rawDetails, addEventIdTo, createDetails ) {
	if ( !rawDetails.length ) {
		return rawDetails;
	}

	const mapAndAddId = _.partialRight( _.map, detail => addEventIdTo( detail ) );
	return yield createDetails( mapAndAddId( rawDetails ) );
};

const deleteDetails = generoute( function* ( id ) {
	return yield EventDetailTemplate.destroy( {
		'where' : {
			'EventId' : id
		}
	} );
} );

const createDetails = generoute( function* ( data ) {
	yield EventDetailTemplate.bulkCreate( data );

	const id      = _.first( _.map( data, 'EventId' ) );
	const details = yield EventDetailTemplate.findAll( {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		},
		'where' : {
			'EventId' : id
		}
	} );

	return _.map( details, detail => detail.toJSON() );
} );

const updateSettingGenerator = function* ( setting, updateSetting ) {
	const response = yield updateSetting( setting.Id, setting );

	if ( !response ) {
		throw Error( 'Event Settings does not exists' );
	}

	return response;
};

const update = generoute( function* ( id, payload ) {
	const setting = yield EventSettingTemplate.findById( id );

	if ( !setting ) {
		return setting;
	}

	const settingAttrs = _.keys( payload );

	const updatedSetting = yield setting.update( payload, { 'field' : settingAttrs } );

	return updatedSetting.toJSON();
} );

module.exports = generoute( function* ( id, payload ) {
	try {
		const addEventIdTo  = _.partialRight( _.assign, { 'EventId' : id } );
		const updateSetting = _.partialRight( updateSettingGenerator, update );

		const maybeCreateDetail = generoute( _.partialRight( createDetailsGenerator, addEventIdTo, createDetails ) );

		const event = yield EventTemplate.findByIdWithDetails( id, models );

		if ( !event ) {
			return event;
		}

		const ammendableEvent = _.pick( payload, [
			'StationId',
			'EventTypeId',
			'Title',
			'Start',
			'End',
			'TeamOne',
			'TeamTwo',
			'TeamOneScore',
			'TeamTwoScore',
			'TeamOneAbbrv',
			'TeamTwoAbbrv',
			'PartNo',
			'GamePartId'
		] );

		const eventAttrs = _.keys( ammendableEvent );

		yield event.update( ammendableEvent, { 'fields' : eventAttrs } );

		const ammendableSetting = _.pick( payload.EventSetting, [
			'Id',
			'FrequencyTypeId',
			'FrequencyTypeValue',
			'Color'
		] );

		yield updateSetting( ammendableSetting );

		const details           = _.result( payload, 'EventDetails', [] );
		const ammendableDetails = _.map( details, detail => _.omit( detail, 'Id' ) );


		yield deleteDetails( id );
		yield maybeCreateDetail( ammendableDetails );

		const updatedEvent = yield EventTemplate.findByIdWithDetails( id, models );

		return updatedEvent;
	} catch ( err ) {
		if ( err instanceof ValidationError ) {
			return err;
		}

		throw err;
	}
} );
