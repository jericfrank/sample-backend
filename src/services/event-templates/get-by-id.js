'use strict';

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const EventTemplate = models.EventTemplate;

module.exports = generoute( function* ( id ) {
	const event = yield EventTemplate.findById( id, {
		'attributes' : {
			'exclude' : [ 'deleted_at', 'created_at', 'updated_at', 'CreatedBy', 'UpdatedBy' ]
		},
		'include' : [
			models.EventTemplate.joinSetting( models ),
			models.EventTemplate.joinDetails( models ),
			models.EventTemplate.joinGamePart( models )
		],
		'where' : {
			'deleted_at' : null
		}
	} );

	if ( !event ) {
		return event;
	}

	return event.toJSON();
} );
