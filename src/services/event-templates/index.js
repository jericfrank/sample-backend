'use strict';

const create     = require( './create' );
const getAll     = require( './get-all' );
const updateById = require( './update-by-id' );
const getById    = require( './get-by-id' );
const deleteById = require( './delete-by-id' );

module.exports = {
	create,
	getAll,
	updateById,
	getById,
	deleteById
};
