'use strict';

const _ = require( 'lodash' );

const models           = use( 'models' );
const generoute        = use( 'utils/generoute' );

const EventTemplate             = models.EventTemplate;
const EventSettingTemplate      = models.EventSettingTemplate;
const EventDetailTemplate       = models.EventDetailTemplate;
const ValidationError           = models.Sequelize.ValidationError;
const ForeignKeyConstraintError = models.Sequelize.ForeignKeyConstraintError;

const assignSettingToEvent = setting => {
	const assignSetting   = _.partialRight( _.assign, { 'EventSetting' : setting } );
	return event => assignSetting( event );
};

const assignDetailsToEvent = details => {
	const assignDetails   = _.partialRight( _.assign, { 'EventDetails' : details } );
	return event => assignDetails( event );
};

const removeMetaInfo  = event => {
	return _.omit( event, [ 'deleted_at', 'created_at', 'updated_at', 'CreatedBy', 'UpdatedBy' ] );
};

const assignedDefaults = data => {
	const TeamOne      = _.result( data, 'TeamOne', '' );
	const TeamTwo      = _.result( data, 'TeamTwo', '' );
	const TeamOneScore = _.result( data, 'TeamOneScore', 0 );
	const TeamTwoScore = _.result( data, 'TeamTwoScore', 0 );
	const TeamOneAbbrv = _.result( data, 'TeamOneAbbrv', '' );
	const TeamTwoAbbrv = _.result( data, 'TeamTwoAbbrv', '' );
	const PartNo       = _.result( data, 'PartNo', null );

	let GamePartId   = _.result( data, 'GamePartId', null );

	if ( data.EventTypeId === 2 ) {
		GamePartId = 1;
	}

	return {
		TeamOne, TeamOneAbbrv, TeamOneScore,
		TeamTwo, TeamTwoAbbrv, TeamTwoScore,
		PartNo, GamePartId
	};
};

const createDetails = generoute( function* ( data ) {
	yield EventDetailTemplate.bulkCreate( data );

	const id      = _.first( _.map( data, 'EventId' ) );
	const details = yield EventDetailTemplate.findAll( {
		'attributes' : {
			'exclude' : [ 'deleted_at', 'created_at', 'updated_at', 'CreatedBy', 'UpdatedBy' ]
		},
		'where' : {
			'EventId' : id
		}
	} );

	return _.map( details, detail => detail.toJSON() );
} );

const createDetailsGenerator = function* ( rawDetails, addEventIdTo, createDetails ) {
	if ( !rawDetails.length ) {
		return event => event;
	}

	const mapAndAddId = _.partialRight( _.map, detail => addEventIdTo( detail ) );
	const details     = yield createDetails( mapAndAddId( rawDetails ) );
	return assignDetailsToEvent( details );
};

const createSetting = generoute( function* ( data ) {
	const setting = yield EventSettingTemplate.create( data );
	return setting.toJSON();
} );

module.exports = generoute( function* ( data ) {
	try {
		const edata        = _.assign( data, assignedDefaults( data ) );
		const event        = yield EventTemplate.create( edata );
		const addEventIdTo = _.partialRight( _.assign, { 'EventId' : event.Id } );

		const maybeCreateDetail = generoute( _.partialRight( createDetailsGenerator, addEventIdTo, createDetails ) );
		const eventDetails      = _.result( data, 'EventDetails', [] );
		const assignDetail      = yield maybeCreateDetail( eventDetails );

		yield createSetting( addEventIdTo( data.EventSetting ) );
		const setting       = yield EventSettingTemplate.findByEventId( event.Id );
		const assignSetting = assignSettingToEvent( setting );

		const getEventInfo = _.flowRight( removeMetaInfo, assignDetail, assignSetting );
		return getEventInfo( event.toJSON() );
	} catch ( err ) {
		if ( err instanceof ValidationError ) {
			return err;
		}

		if ( err instanceof ForeignKeyConstraintError ) {
			return {
				'message' : 'ForeignKeyConstraintError',
				'errors'  : [ { 'message' : 'Station does not exists.' } ]
			};
		}

		throw err;
	}
} );
