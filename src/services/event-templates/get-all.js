'use strict';

const _ = require( 'lodash' );

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const { EventTemplate } = models;

module.exports = generoute( function* ( stationId ) {
	const options = {
		'attributes' : {
			'exclude' : [ 'deleted_at', 'created_at', 'updated_at', 'CreatedBy', 'UpdatedBy' ]
		},
		'include' : [
			models.EventTemplate.joinSetting( models ),
			models.EventTemplate.joinGamePart( models ),
			models.EventTemplate.joinDetails( models )
		],
		'where' : {
			'deleted_at' : null
		}
	};

	if ( stationId ) {
		options.where.StationId = stationId;
	}

	const events = yield EventTemplate.findAll( options );

	return _.map( events, event => event.toJSON() );
} );
