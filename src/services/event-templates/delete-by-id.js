'use strict';

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const EventTemplate = models.EventTemplate;

module.exports = generoute( function* ( Id ) {
	const event = yield EventTemplate.findById( Id, {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		},
		'where' : {
			'deleted_at' : null
		}
	} );

	if ( !event ) {
		return event;
	}

	const deletedEvent = yield EventTemplate.destroy( {
		'where' : {
			Id
		}
	} );

	return {
		'Operation' : Boolean( deletedEvent )
	};
} );

