'use strict';

const log = use( 'logging' );

( () => {
	log.info( 'Async file service is up' );
	require( './create-file' )();
	require( './copy-file' )();
	require( './create-image' )();
	require( './create-config' )();
	require( './get-config' )();
	require( './delete-config' )();
	require( './delete-file' )();
} )();
