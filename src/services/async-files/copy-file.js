'use strict';

const _   = require( 'lodash' );
const AWS = require( 'aws-sdk' );
const kue = require( 'kue');

const config = use( 'config' );

const Bucket = config.s3.bucketName;

AWS.config.update( {
	'region' : config.s3.region
} );

module.exports = () => {
	const queue = kue.createQueue();
	const s3    = new AWS.S3();

	queue.process( 'copy-file', ( job, done ) => {
		// you need to set the s3 bucket in the CopySource key
		const oldKey = _.kebabCase( job.data.OldKey.toLowerCase() );
		const newKey = _.kebabCase( job.data.NewKey.toLowerCase() );

		const copyParams = {
			Bucket,
			'CopySource' : `${Bucket}/${oldKey}.txt`,
			'Key'        : `${newKey}.txt`
		};

		const deleteParams = {
			Bucket,
			'Key' : `${oldKey}.txt`
		};

		const copy = s3.copyObject( copyParams ).promise();
		const del  = s3.deleteObject( deleteParams ).promise();

		Promise.all( [ copy, del ] )
			.then( res => done( null, res ) )
			.catch( err => done( err ) );
	} );
};
