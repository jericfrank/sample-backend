'use strict';

const AWS    = require( 'aws-sdk' );
const kue    = require( 'kue' );
const log    = use( 'logging' );
const config = use( 'config' );
const Bucket = config.s3.bucketName;

AWS.config.update( {
	'region' : config.s3.region
} );

const handleS3Response = () => true;

module.exports = () => {
	return new Promise( ( resolve, reject ) => {
		const queue = kue.createQueue();
		const s3    = new AWS.S3();

		queue.process( 'delete-config', ( job, done ) => {
			const { Name } = job.data;
			const name     = ( Name || '' ).split( ' ' ).join( '-' ).toLowerCase();
			const Key      = `config/${name}.config`;

			const jobRef = s3.deleteObject( { Bucket, Key } )
				.promise()
				.then( handleS3Response )
				.then( res => done( null, res ) )
				.catch( err => done( err ) );

			resolve( jobRef );
		} );

		queue.on( 'error', err => {
			log.fatal( err );
			reject( err );
		} );
	} );
};
