'use strict';

const _      = require( 'lodash' );
const AWS    = require( 'aws-sdk' );
const kue    = require( 'kue' );
const moment = require( 'moment' );
const fs     = require( 'fs' );

const log    = use( 'logging' );
const config = use( 'config' );

const Bucket = config.s3.bucketName;

AWS.config.update( {
	'region' : config.s3.region
} );

const copyImage = ( s3, { Name } ) => {
	return ( res ) => {
		const copyParams = {
			Bucket,
			'CopySource' : `${Bucket}/${res.Key}`,
			'Key'        : `${Name}/favicon.ico`,
			'ACL'        : 'public-read'
		};

		s3.copyObject( copyParams ).promise();
		return res;
	};
};

const handleS3Response = ( job ) => {
	const { Filepath } = job.data;

	return ( res ) => {
		return new Promise( ( resolve, reject ) => {
			log.info( res );
			fs.unlink( Filepath, err => {
				if ( err ) {
					return reject( err );
				}

				return resolve( res.Key );
			} );
		} );
	};
};

module.exports = () => {
	return new Promise( ( resolve, reject ) => {
		const queue = kue.createQueue();
		const s3    = new AWS.S3();

		queue.process( 'create-image', ( job, done ) => {
			const { Name, Filepath, ContentType } = job.data;

			const fileFormat = _.last( ContentType.split( '/' ) );
			const Key        = `${Name}/logo-${moment.utc().valueOf()}.${fileFormat}`;
			const Body       = fs.createReadStream( Filepath );
			const ACL        = 'public-read';

			const jobRef = s3.upload( { Bucket, Key, Body, ContentType, ACL } )
				.promise()
				.then( copyImage( s3, { Name, fileFormat } ) )
				.then( handleS3Response( job ) )
				.then( key => done( null, key ) )
				.catch( err => done( err ) );

			resolve( jobRef );
		} );

		queue.on( 'error', err => {
			log.fatal( err );
			reject( err );
		} );
	} );
};
