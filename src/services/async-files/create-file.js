'use strict';

const _      = require( 'lodash' );
const AWS    = require( 'aws-sdk' );
const kue    = require( 'kue' );
const moment = require( 'moment' );

const log    = use( 'logging' );
const config = use( 'config' );
const models = use( 'models' );

const jobService       = use( 'services/jobs' );
const buildFileContent = use( 'utils/build-file-content' );
const generoute        = use( 'utils/generoute' );

const { Event, EventQueue } = models;

const Bucket = config.s3.bucketName;

AWS.config.update( {
	'region' : config.s3.region
} );

const isConflictToNextJob  = ( job, content ) => {
	return new Promise( ( resolve ) => {
		const { FrequencyInterval } = job.data;

		if ( content !== '' ) {
			return resolve( false );
		}

		return kue.Job.rangeByType( 'create-file', 'delayed', 0, -1, 'asc', ( err, jobs ) => {
			const job = _.first( _.filter( jobs, job => {
				const type = _.get( job.data, 'Event.EventTypeId', null );

				return type === 2 || type === 3;
			} ) );


			if ( !job ) {
				return resolve( false );
			}

			const nextEvent = job.data.Event;

			const apprxTime   = moment.utc().add( FrequencyInterval, 'ms' );
			const nextJobTime = moment.utc( nextEvent.Start );

			if ( nextJobTime.isBefore( apprxTime ) ) {
				return resolve( true );
			}

			return resolve( false );
		} );
	} );
};

const generateNextIndex = ( index, body ) => {
	let i = index;
	if ( index < body.length - 1 ) {
		i = index + 1;
	} else {
		i = 0;
	}

	return i;
};

const updateJobId = generoute( function* (job ) {
	const event = yield Event.findById( job.data.Event.Id );

	if ( event ) {
		yield event.update( { 'JobId' : job.id } );
	}

	return true;
} );

const handleS3Response = ( { job, Body } ) => {
	const { Event, Index, EventDetails, FrequencyInterval, Delay } = job.data;

	return generoute( function* ( res ) {
		log.info( `${Bucket} : ${Index}=${Body}` );
		const index = generateNextIndex( Index, EventDetails );

		yield EventQueue.update( { 'Index' : index }, {
			'where' : {
				'EventId' : Event.Id
			},
			'fields' : [ 'Index' ]
		} );

		if ( Event.EventTypeId !== 1 ) {
			return res;
		}

		if ( yield isConflictToNextJob( job, Body ) ) {
			return res;
		}

		const ed = moment.utc( Event.End, 'YYYY-MM-DDTHH:mm:ssZ' );
		if ( !moment.utc().add( Delay, 'ms' ).isBefore( ed ) ) {
			return res;
		}

		const newJob = yield jobService.create( _.assign( {}, job.data, {
			'Index' : index,
			'Delay' : FrequencyInterval
		} ) );

		yield updateJobId( newJob );

		return res;
	} );
};

module.exports = () => {
	return new Promise( ( resolve, reject ) => {
		const queue = kue.createQueue();
		const s3    = new AWS.S3();

		queue.process( 'create-file', ( job, done ) => {
			const { Name, Delay, Event, Index, EventDetails } = job.data;
			const name = ( Name || '' ).split( ' ' ).join( '-' ).toLowerCase();
			const Key  = `${name}.txt`;

			const Body = buildFileContent( Event, EventDetails, Delay, Index );
			const jobRef = s3.putObject( { Bucket, Key, Body } )
				.promise()
				.then( handleS3Response( { job, Body } ) )
				.then( res => done( null, res ) )
				.catch( err => done( err ) );

			resolve( jobRef );
		} );

		queue.on( 'error', err => {
			log.fatal( err );
			reject( err );
		} );
	} );
};
