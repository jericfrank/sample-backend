'use strict';

const _ = require( 'lodash' );

const generoute = use( 'utils/generoute' );
const models    = use( 'models' );

const ProfileModule = models.ProfileModule;

module.exports = generoute( function* ( profile ) {
	try {
		const options = {
			'attributes' : {
				'exclude' : [ 'deleted_at' ]
			},
			'include' : [ {
				'model'      : models.Module,
				'as'         : 'Module',
				'attributes' : {
					'exclude' : [ 'deleted_at' ]
				}
			} ]
		};

		if ( profile ) {
			options.where = {
				'ProfileId' : profile
			};
		}

		const privileges = yield ProfileModule.findAll( options );

		return _.map( privileges, privilege => privilege.toJSON() );
	} catch ( error ) {
		throw error;
	}
} );
