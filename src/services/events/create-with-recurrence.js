'use strict';

const _      = require( 'lodash' );
const moment = require( 'moment' );

const models = use( 'models' );

const dateRange          = use( 'utils/date-range' );
const generoute          = use( 'utils/generoute' );
const generecurrenceType = use( 'utils/generecurrence-type' );
const splitWeekdays      = use( 'utils/split-weekdays' );
const joinWeekdays       = use( 'utils/join-weekdays' );

const ValidationError = models.Sequelize.ValidationError;

const Event           = models.Event;
const EventRecurrence = models.EventRecurrence;

module.exports = generoute( function* ( data ) {
	try {
		const eventService = use( 'services/events' );
		const recurrence   = data.EventRecurrence;

		const {
			step,
			action,
			end,
			weekdays,
			day,
			monthWeekOption,
			monthWeekdayOption
		} = generecurrenceType( recurrence );

		const opt = {
			step,
			weekdays,
			day,
			monthWeekOption,
			monthWeekdayOption,
			'arrDate' : [ recurrence.Start, end ]
		};

		const range = dateRange( opt, action, date => date.format( 'YYYY-MM-DD' ) );

		const startTime = _.last( data.Start.split( 'T' ) );
		const utcRange  = _.chain( range )
			.map( date => moment.utc( `${date}T${startTime}` ) )
			.filter( date => !date.isBefore( moment.utc() ) )
			.map( date => date.format() )
			.value();

		const event  = _.omit( data, [ 'EventRecurrence' ] );
		const events = yield _.map( utcRange, generoute( function* ( date ) {
			const tokenParser = 'YYYY-MM-DDTHH:mm:ssZ';
			const endDate     = _.first( date.split( 'T' ) );
			const endTime     = _.last( event.End.split( 'T' ) );

			const newStart = moment.utc( date );
			const newEnd   = moment.utc( `${endDate}T${endTime}`, tokenParser );

			const underlap = yield Event.findUnderlap( {
				'Start'     : newStart.format(),
				'End'       : newEnd.format(),
				'StationId' : event.StationId
			} );

			if ( underlap.length ) {
				const event = _.first( underlap );
				throw new ValidationError( 'ValidationError', [ {
					'message' : `Submitted date range overlaps Event: ${event.Title}.`
				} ] );
			}

			const inBetween = yield Event.findInBetween( {
				'Start'     : newStart.format(),
				'End'       : newEnd.format(),
				'StationId' : event.StationId
			} );

			if ( inBetween.length ) {
				const event = _.first( inBetween );
				throw new ValidationError( 'ValidationError', [ {
					'message' : `Submitted date range inbetween Event: ${event.Title}.`
				} ] );
			}

			return _.assign( {}, event, {
				'Start' : newStart.format(),
				'End'   : newEnd.format()
			} );
		} ) );

		const newEvent = yield eventService.create( _.first( events ) );
		const tail     = _.tail( events );

		const promisedTail = _.map( tail, event => {
			const e = _.assign( {}, event, { 'ParentId' : newEvent.Id } );
			return eventService.create( e );
		} );

		const promisedUpdateParentId = Event.upsert( {
			'Id'       : newEvent.Id,
			'ParentId' : newEvent.Id
		} );

		yield Promise.all( promisedTail.concat( promisedUpdateParentId ) );

		const newRecurrence    = yield EventRecurrence.create( _.assign( {}, recurrence, {
			'EventId'  : newEvent.Id,
			'Weekdays' : joinWeekdays( recurrence )
		} ) );

		newRecurrence.Weekdays = splitWeekdays( newRecurrence );
		newEvent.ParentId      = newEvent.Id;

		return _.assign( {}, newEvent, { 'EventRecurrence' : newRecurrence } );
	} catch ( err ) {
		if ( err instanceof ValidationError ) {
			return err;
		}
		throw err;
	}
} );
