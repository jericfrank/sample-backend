'use strict';

const _ = require( 'lodash' );

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const {
	Event,
	EventSetting,
	EventDetail,
	EventQueue,
	EventRecurrence
} = models;

module.exports = generoute( function* ( Id ) {
	const jobService = use( 'services/jobs' );

	const event = yield Event.findById( Id, {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		},
		'where' : {
			'deleted_at' : null
		}
	} );

	if ( !event ) {
		return event;
	}

	const eventChildren = yield Event.findAll( {
		'where' : {
			'ParentId' : Id
		}
	} );

	const ids = _.map( eventChildren, 'Id' );

	const settings = yield _.map( ids, id => {
		return EventSetting.findOne( {
			'where' : {
				'EventId'    : id,
				'deleted_at' : null
			}
		} );
	} );

	if ( settings.length ) {
		yield _.map( settings, setting => setting.destroy() );
	}

	const details = yield _.map( ids, id => {
		return EventDetail.findAll( {
			'where' : {
				'EventId'    : id,
				'deleted_at' : null
			}
		} );
	} );

	if ( details.length ) {
		yield _.map( ids, id => {
			return EventDetail.destroy( {
				'where' : {
					'EventId'    : id,
					'deleted_at' : null
				}
			} );
		} );
	}

	const queues = yield _.map( ids, id => {
		return EventQueue.destroy( {
			'where' : {
				'EventId' : id
			}
		} );
	} );

	if ( queues.length ) {
		yield _.map( ids, id => {
			return EventQueue.destroy( {
				'where' : {
					'EventId' : id
				}
			} );
		} );
	}

	const recurrence = yield EventRecurrence.findOne( {
		'where' : {
			'EventId'    : Id,
			'deleted_at' : null
		}
	} );

	if ( recurrence ) {
		yield recurrence.destroy();
	}

	yield _.map( eventChildren, event => jobService.deleteById( event.JobId ) );

	yield Event.destroy( {
		'where' : {
			'ParentId' : Id
		}
	} );

	return {
		'Operation' : true
	};
} );

