'use strict';

const createSetting             = require( './create-setting' );
const updateSetting             = require( './update-setting' );
const getSettingByEventId       = require( './get-setting-by-event-id' );
const create                    = require( './create' );
const getAll                    = require( './get-all' );
const getById                   = require( './get-by-id' );
const updateById                = require( './update-by-id' );
const deleteById                = require( './delete-by-id' );
const createDetails             = require( './create-details' );
const upsertDetails             = require( './upsert-details' );
const getByIdWithDetails        = require( './get-by-id-with-details' );
const deleteDetails             = require( './delete-details' );
const createWithRecurrence      = require( './create-with-recurrence' );
const deleteBySeries            = require( './delete-by-series' );
const updateWithRecurrence      = require( './update-with-recurrence' );
const createJobInDelayedEvent   = require( './create-job-in-delayed-event' );
const createJobInCompletedEvent = require( './create-job-in-completed-event' );

module.exports = {
	createSetting,
	updateSetting,
	getSettingByEventId,
	create,
	getAll,
	getById,
	updateById,
	deleteById,
	createDetails,
	upsertDetails,
	getByIdWithDetails,
	deleteDetails,
	createWithRecurrence,
	deleteBySeries,
	updateWithRecurrence,
	createJobInDelayedEvent,
	createJobInCompletedEvent
};
