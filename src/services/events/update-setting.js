'use strict';

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const EventSetting = models.EventSetting;

module.exports = generoute( function* ( id, payload ) {
	const setting = yield EventSetting.findById( id );

	if ( !setting ) {
		return setting;
	}

	const updatedSetting = yield setting.update( payload );

	return updatedSetting.toJSON();
} );
