'use strict';

const _      = require( 'lodash' );
const moment = require( 'moment' );

const models       = use( 'models' );
const generoute    = use( 'utils/generoute' );
const computeDelay = use( 'utils/compute-delay' );

const Event                     = models.Event;
const EventQueue                = models.EventQueue;
const ValidationError           = models.Sequelize.ValidationError;
const ForeignKeyConstraintError = models.Sequelize.ForeignKeyConstraintError;

const assignSettingToEvent = setting => {
	const removeDeletedAt = _.partialRight( _.omit, 'deleted_at' );
	const assignSetting   = _.partialRight( _.assign, { 'EventSetting' : setting } );
	const removeAndAssign = _.flowRight( assignSetting, removeDeletedAt );
	return event => removeAndAssign( event );
};

const assignDetailsToEvent = details => {
	const removeDeletedAt = _.partialRight( _.omit, 'deleted_at' );
	const assignDetails   = _.partialRight( _.assign, { 'EventDetails' : details } );
	const removeAndAssign = _.flowRight( assignDetails, removeDeletedAt );
	return event => removeAndAssign( event );
};

const assignedDefaults = data => {
	const TeamOne      = _.result( data, 'TeamOne', '' );
	const TeamTwo      = _.result( data, 'TeamTwo', '' );
	const TeamOneScore = _.result( data, 'TeamOneScore', 0 );
	const TeamTwoScore = _.result( data, 'TeamTwoScore', 0 );
	const TeamOneAbbrv = _.result( data, 'TeamOneAbbrv', '' );
	const TeamTwoAbbrv = _.result( data, 'TeamTwoAbbrv', '' );
	const PartNo       = _.result( data, 'PartNo', 0 );

	let GamePartId   = _.result( data, 'GamePartId', null );

	if ( data.EventTypeId === 2 ) {
		GamePartId = 1;
	}

	return {
		TeamOne, TeamOneAbbrv, TeamOneScore,
		TeamTwo, TeamTwoAbbrv, TeamTwoScore,
		PartNo, GamePartId
	};
};

const createDetailsGenerator = function* ( rawDetails, addEventIdTo, createDetails ) {
	if ( !rawDetails.length ) {
		return event => event;
	}

	const mapAndAddId = _.partialRight( _.map, detail => addEventIdTo( detail ) );
	const details     = yield createDetails( mapAndAddId( rawDetails ) );
	return assignDetailsToEvent( details );
};

const getIntervalAsMilliseconds = ( setting ) => {
	const measurement = _.result( setting.FrequencyType, 'UnitOfMeasurement.Name', null );
	const value       = _.result( setting, 'FrequencyTypeValue', 1 );

	if ( !measurement ) {
		return 0;
	}

	return moment.duration( value, measurement ).asMilliseconds();
};

module.exports = generoute( function* ( data ) {
	try {
		const eventService   = use( 'services/events' );
		const stationService = use( 'services/stations' );
		const jobService     = use( 'services/jobs' );

		const underlap = yield Event.findUnderlap( {
			'Start'     : data.Start,
			'End'       : data.End,
			'StationId' : data.StationId
		} );

		if ( underlap.length ) {
			const event = _.first( underlap );
			throw new ValidationError( 'ValidationError', [ {
				'message' : `Submitted date range overlaps Event: ${event.Title}.`
			} ] );
		}

		const inBetween = yield Event.findInBetween( {
			'Start'     : data.Start,
			'End'       : data.End,
			'StationId' : data.StationId
		} );

		if ( inBetween.length ) {
			const event = _.first( inBetween );
			throw new ValidationError( 'ValidationError', [ {
				'message' : `Submitted date range inbetween Event: ${event.Title}.`
			} ] );
		}

		const edata        = _.assign( data, assignedDefaults( data ) );
		const event        = yield Event.create( edata );
		const addEventIdTo = _.partialRight( _.assign, { 'EventId' : event.Id } );

		yield eventService.createSetting( addEventIdTo( data.EventSetting ) );
		const setting       = yield eventService.getSettingByEventId( event.Id );
		const assignSetting = assignSettingToEvent( setting );

		const eventDetails      = _.result( data, 'EventDetails', [] );
		const maybeCreateDetail = generoute( _.partialRight( createDetailsGenerator, addEventIdTo, eventService.createDetails ) );
		const assignDetail      = yield maybeCreateDetail( eventDetails );

		const date = {
			'Start' : event.Start,
			'End'   : event.End
		};
		const frequencyInterval = getIntervalAsMilliseconds( setting );
		const delay             = computeDelay( date, frequencyInterval );

		const station = yield stationService.getById( event.StationId );

		const startJob = yield jobService.create( {
			'Name'              : station.Name,
			'Index'             : 0,
			'Delay'             : delay,
			'FrequencyInterval' : frequencyInterval,
			'Event'             : event,
			'EventDetails'      : eventDetails
		} );

		yield EventQueue.create( {
			'JobId'    : startJob.id,
			'EventId'  : event.Id,
			'Duration' : delay,
			'Index'    : 0
		} );

		// for now, omit the live event type
		const endDelay = computeDelay( { 'Start' : event.End, 'End' : event.End }, 0 );
		const endJob   = yield jobService.create( {
			'Name'              : station.Name,
			'Index'             : 0,
			'Delay'             : endDelay,
			'FrequencyInterval' : frequencyInterval,
			'Event'             : event,
			'EventDetails'      : eventDetails
		} );

		yield EventQueue.create( {
			'JobId'    : endJob.id,
			'EventId'  : event.Id,
			'Duration' : delay,
			'Index'    : 0
		} );

		yield event.update( { 'JobId' : startJob.id } );

		const getEventInfo = _.flowRight( assignDetail, assignSetting );
		return getEventInfo( _.omit( event.toJSON(), 'JobId' ) );
	} catch ( err ) {
		if ( err instanceof ValidationError ) {
			return err;
		}

		if ( err instanceof ForeignKeyConstraintError ) {
			return {
				'message' : 'ForeignKeyConstraintError',
				'errors'  : [ { 'message' : 'Station does not exists.' } ]
			};
		}

		throw err;
	}
} );
