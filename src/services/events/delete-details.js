'use strict';

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const EventDetail = models.EventDetail;

module.exports = generoute( function* ( id ) {
	return yield EventDetail.destroy( {
		'where' : {
			'EventId' : id
		}
	} );
} );
