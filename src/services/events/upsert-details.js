'use strict';

const _ = require( 'lodash' );

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const EventDetail = models.EventDetail;

module.exports = generoute( function* ( data ) {
	// sorry no ids in bulk create
	// https://github.com/sequelize/sequelize/issues/2908
	const details = yield EventDetail.bulkCreate( data, {
		'updateOnDuplicate' : [ 'Description' ]
	} );
	return _.map( details, detail => detail.toJSON() );
} );
