'use strict';

const _      = require( 'lodash' );
const moment = require( 'moment' );

const generoute    = use( 'utils/generoute' );
const log          = use( 'logging' );
const computeDelay = use( 'utils/compute-delay' );

const { Event, EventQueue } = use( 'models' );

const updateJobId = job => {
	return Event.findById( job.data.Event.Id )
		.then( event => {
			event.update( { 'JobId' : job.id } );
			return job;
		} );
};

const delayedStaticLiveMusic = generoute( function* ( { id, frequencyInterval, event, station, updatedEvent, ammendableDetails, delay } ) {
	const jobService = use( 'services/jobs' );

	let endJobId = 0;
	let index    = 0;

	const queues = yield EventQueue.findAll( {
		'where' : {
			'EventId' : id
		}
	} );

	index    = _.last( _.map( queues, 'Index' ) );
	endJobId = _.last( _.map( queues, 'JobId' ) );

	const createJob = generoute( ( { Name, Index, Delay, FrequencyInterval, Event, AmmendableDetails } ) => {
		return jobService.create( {
			Name,
			Index,
			Delay, // endDelay
			FrequencyInterval,
			Event, // updatedEvent
			'EventDetails' : _.map( AmmendableDetails, details => _.pick( details, [ 'Description' ] ) )
		} );
	} );

	const now = moment.utc().add( frequencyInterval, 'ms' );
	const ed  = moment.utc( updatedEvent.End );

	if ( now.isBefore( ed ) ) {
			const newDelay = delay || frequencyInterval;
			log.info( 'ADJUST EVENT DURATION', delay, frequencyInterval, event.JobId, endJobId );

			yield jobService.deleteById( event.JobId )
				.catch( err => log.fatal( err ) );
			yield jobService.deleteById( endJobId )
				.catch( err => log.fatal( err ) );

			const startJob = yield createJob( {
				'Name'              : station.Name,
				'Index'             : index,
				'Delay'             : newDelay,
				'FrequencyInterval' : frequencyInterval,
				'Event'             : updatedEvent,
				'AmmendableDetails' : ammendableDetails

			} )
			.then( updateJobId )
			.catch( err => log.fatal( err ) );

			const endDelay = computeDelay( { 'Start' : updatedEvent.End, 'End' : updatedEvent.End }, 0 );
			const endJob   = yield createJob( {
				'Name'              : station.Name,
				'Index'             : 0,
				'Delay'             : endDelay,
				'FrequencyInterval' : frequencyInterval,
				'Event'             : updatedEvent,
				'AmmendableDetails' : ammendableDetails
			} ).catch( err => log.fatal( err ) );

			yield EventQueue.destroy( {
				'where' : {
					'EventId' : id
				}
			} );

			yield EventQueue.create( {
				'JobId'    : _.get( startJob, 'id', 0 ),
				'EventId'  : event.Id,
				'Duration' : delay,
				'Index'    : index
			} );

			yield EventQueue.create( {
				'JobId'    : endJob.id,
				'EventId'  : id,
				'Duration' : endDelay,
				'Index'    : 0
			} );
	} else {
		log.info( 'ELSE PATH OF ADJUST EVENT DURATION' );
		yield jobService.deleteById( event.JobId )
			.catch( err => log.fatal( err ) );
		yield jobService.deleteById( endJobId )
			.catch( err => log.fatal( err ) );

		const endDelay = computeDelay( { 'Start' : updatedEvent.End, 'End' : updatedEvent.End }, 0 );
		const endJob = yield createJob( {
			'Name'              : station.Name,
			'Index'             : 0,
			'Delay'             : endDelay,
			'FrequencyInterval' : frequencyInterval,
			'Event'             : updatedEvent,
			'AmmendableDetails' : ammendableDetails
		} ).catch( err => log.fatal( err ) );

		yield EventQueue.create( {
			'JobId'    : endJob.id,
			'EventId'  : id,
			'Duration' : endDelay,
			'Index'    : 0
		} );
	}
} );

module.exports = generoute( function* ( data ) {
	return delayedStaticLiveMusic( data );
} );
