'use strict';

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const EventSetting = models.EventSetting;

module.exports = generoute( function* ( data ) {
	const setting = yield EventSetting.create( data );

	return setting.toJSON();
} );
