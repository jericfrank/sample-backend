'use strict';

const _ = require( 'lodash' );

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const {
	Event,
	EventSetting,
	EventDetail,
	EventQueue
} = models;

module.exports = generoute( function* ( Id ) {
	const jobService = use( 'services/jobs' );

	const event = yield Event.findById( Id, {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		},
		'where' : {
			'deleted_at' : null
		}
	} );

	if ( !event ) {
		return event;
	}

	const setting = yield EventSetting.findOne( {
		'where' : {
			'EventId'    : Id,
			'deleted_at' : null
		}
	} );

	if ( setting ) {
		yield setting.destroy();
	}

	const details = yield EventDetail.findAll( {
		'where' : {
			'EventId'    : Id,
			'deleted_at' : null
		}
	} );

	if ( details.length ) {
		yield EventDetail.destroy( {
			'where' : {
				'EventId'    : Id,
				'deleted_at' : null
			}
		} );
	}

	const queues = yield EventQueue.findAll( {
		'where' : {
			'EventId' : Id
		}
	} );

	if ( queues.length ) {
		yield EventQueue.destroy( {
			'where' : {
				'EventId' : Id
			}
		} );

		_.map( queues, queue => {
			return jobService.deleteById( queue.JobId );
		} );
	}

	yield jobService.deleteById( event.JobId );

	const deletedEvent = yield Event.destroy( {
		'where' : {
			Id
		}
	} );

	return {
		'Operation' : Boolean(deletedEvent )
	};
} );

