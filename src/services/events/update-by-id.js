'use strict';

const _      = require( 'lodash' );
const moment = require( 'moment' );
const kue    = require( 'kue' );

const models       = use( 'models' );
const generoute    = use( 'utils/generoute' );
const computeDelay = use( 'utils/compute-delay' );
const log          = use( 'logging' );

const Event           = models.Event;
const ValidationError = models.Sequelize.ValidationError;

const updateSettingGenerator = function* ( setting, updateSetting ) {
	const response = yield updateSetting( setting.Id, setting );

	if ( !response ) {
		throw Error( 'Event Settings does not exists' );
	}

	return response;
};

const createDetailsGenerator = function* ( rawDetails, addEventIdTo, createDetails ) {
	if ( !rawDetails.length ) {
		return rawDetails;
	}

	const mapAndAddId = _.partialRight( _.map, detail => addEventIdTo( detail ) );
	return yield createDetails( mapAndAddId( rawDetails ) );
};

const getIntervalAsMilliseconds = ( setting ) => {
	const measurement = _.result( setting.FrequencyType, 'UnitOfMeasurement.Name', null );
	const value       = _.result( setting, 'FrequencyTypeValue', 1 );

	if ( !measurement ) {
		return 0;
	}

	return moment.duration( value, measurement ).asMilliseconds();
};

const getJobStatus = ( jobId ) => {
	return new Promise( ( resolve ) => {
		const queue = kue.createQueue();
		kue.Job.get( jobId, ( err, job ) => {
			if ( err ) {
				return resolve( '' );
			}

			queue.shutdown();
			return resolve( job.state() );
		} );
	} );
};

module.exports = generoute( function* ( id, payload ) {
	try {
		const eventService = use( 'services/events' );

		if ( moment.utc( payload.End ).isBefore( moment.utc() ) ) {
			throw new ValidationError( 'ValidationError', [ {
				'message' : 'Unable to update past event.'
			} ] );
		}

		const underlap = yield Event.findUnderlap( {
			'Id'        : id,
			'Start'     : payload.Start,
			'End'       : payload.End,
			'StationId' : payload.StationId
		} );

		if ( underlap.length ) {
			const event = _.first( underlap );
			throw new ValidationError( 'ValidationError', [ {
				'message' : `Submitted date range overlaps Event: ${event.Title}.`
			} ] );
		}

		const inBetween = yield Event.findInBetween( {
			'Id'        : id,
			'Start'     : payload.Start,
			'End'       : payload.End,
			'StationId' : payload.StationId
		} );

		if ( inBetween.length ) {
			const event = _.first( inBetween );
			throw new ValidationError( 'ValidationError', [ {
				'message' : `Submitted date range inbetween Event: ${event.Title}.`
			} ] );
		}

		const addEventIdTo  = _.partialRight( _.assign, { 'EventId' : id } );
		const updateSetting = _.partialRight( updateSettingGenerator, eventService.updateSetting );

		const maybeCreateDetail = generoute( _.partialRight( createDetailsGenerator, addEventIdTo, eventService.createDetails ) );

		const ammendableEvent = _.pick( payload, [
			'StationId',
			'EventTypeId',
			'Title',
			'Start',
			'End',
			'TeamOne',
			'TeamTwo',
			'TeamOneScore',
			'TeamTwoScore',
			'TeamOneAbbrv',
			'TeamTwoAbbrv',
			'PartNo',
			'GamePartId'
		] );

		const ammendableSetting = _.pick( payload.EventSetting, [
			'Id',
			'FrequencyTypeId',
			'FrequencyTypeValue',
			'Color'
		] );

		const details           = _.result( payload, 'EventDetails', [] );
		const ammendableDetails = _.map( details, detail => _.omit( detail, 'Id' ) );

		const event = yield Event.findById( id, {
			'include' : [
				models.Event.joinDetails( models )
			]
		} );

		if ( !event ) {
			return event;
		}

		const clonedEvent = event.toJSON();
		const ev          = yield event.update( ammendableEvent );
		yield updateSetting( ammendableSetting );
		yield eventService.deleteDetails( id );
		yield maybeCreateDetail( ammendableDetails );

		const updatedEvent = yield eventService.getByIdWithDetails( id );

		const date = {
			'Start' : updatedEvent.Start,
			'End'   : updatedEvent.End
		};
		const frequencyInterval = getIntervalAsMilliseconds( updatedEvent.EventSetting );
		const delay             = computeDelay( date, frequencyInterval );

		const jobStatus = yield getJobStatus( ev.JobId )
			.catch( error => log.fatal( error ) );

		const stationService = use( 'services/stations' );
		const station        = yield stationService.getById( event.StationId );

		if ( jobStatus === 'delayed' ) {
			yield eventService.createJobInDelayedEvent( {
				id,
				frequencyInterval,
				station,
				updatedEvent,
				ammendableDetails,
				delay,
				'event' : clonedEvent
			} );
		} else {
			yield eventService.createJobInCompletedEvent( {
				id,
				frequencyInterval,
				station,
				updatedEvent,
				ammendableDetails,
				delay,
				'event' : clonedEvent
			} );
		}

		return updatedEvent;
	} catch ( err ) {
		if ( err instanceof ValidationError ) {
			return err;
		}

		throw err;
	}
} );
