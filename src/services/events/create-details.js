'use strict';

const _ = require( 'lodash' );

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const EventDetail = models.EventDetail;

module.exports = generoute( function* ( data ) {
	// sorry no ids in bulk create
	// https://github.com/sequelize/sequelize/issues/2908
	yield EventDetail.bulkCreate( data );

	const id      = _.first( _.map( data, 'EventId' ) );
	const details = yield EventDetail.findAll( {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		},
		'where' : {
			'EventId' : id
		}
	} );

	return _.map( details, detail => detail.toJSON() );
} );
