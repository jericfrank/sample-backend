'use strict';

const models        = use( 'models' );
const generoute     = use( 'utils/generoute' );
const splitWeekdays = use( 'utils/split-weekdays' );

const Event = models.Event;

module.exports = generoute( function* ( id ) {
	const event = yield Event.findById( id, {
		'attributes' : {
			'exclude' : [ 'deleted_at', 'JobId' ]
		},
		'include' : [
			models.Event.joinSetting( models ),
			models.Event.joinDetails( models ),
			models.Event.joinGamePart( models ),
			models.Event.joinRecurrence( models )
		],
		'where' : {
			'deleted_at' : null
		}
	} );

	if ( !event ) {
		return event;
	}

	const raw = event.toJSON();

	if ( raw.EventRecurrence ) {
		raw.EventRecurrence.Weekdays = splitWeekdays( raw.EventRecurrence );
	}

	return raw;
} );
