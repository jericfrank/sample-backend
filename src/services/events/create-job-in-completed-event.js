'use strict';

const _         = require( 'lodash' );
const moment    = require( 'moment' );

const generoute    = use( 'utils/generoute' );
const log          = use( 'logging' );
const computeDelay = use( 'utils/compute-delay' );

const { Event, EventQueue } = use( 'models' );

const updateJobId = job => {
	return Event.findById( job.data.Event.Id )
		.then( event => {
			event.update( { 'JobId' : job.id } );
			return job;
		} );
};

const getEventEndJob = generoute( function* ( eventId ) {
	const queues = yield EventQueue.findAll( {
		'where' : {
			'EventId' : eventId
		}
	} );

	return _.last( _.map( queues, 'JobId' ) );
} );

const isScoreUpdate  = ( oldEvent, updatedEvent ) => {
	return ( oldEvent.TeamOneScore !== updatedEvent.TeamOneScore ) ||
		( oldEvent.TeamTwoScore !== updatedEvent.TeamTwoScore ) ||
		( oldEvent.PartNo !== updatedEvent.PartNo );
};

const completedStaticLiveMusic = generoute( function* ( { id, updatedEvent, delay, station, ammendableDetails, frequencyInterval, event } ) {
	const jobService = use( 'services/jobs' );

	const now = moment.utc();
	const sd  = moment.utc( updatedEvent.Start );
	const ed  = moment.utc( updatedEvent.End );

	// If the event is already in progress
	// then we want to update it by changing end date
	// then set frequencyInterval as a delay
	// this will forcibly reset the job's delay
	// every time we update
	let newDelay = delay;
	if ( !now.isBefore( sd ) ) {
		newDelay = frequencyInterval;
	}

	const queues = yield EventQueue.findAll( {
		'where' : {
			'EventId' : id
		}
	} );

	const index = _.last( _.map( queues, 'Index' ) ) || 0;

	// current time + interval is not yet equal to end date
	// then we want to update job by changing start date
	// then create new job start instance
	// then update event JobId with the new job start instance id
	let startJob = {
		'id' : updatedEvent.JobId
	};
	if ( now.clone().add( frequencyInterval, 'ms' ).isBefore( ed ) ) {
		const oldStart = moment( event.Start ).format( 'YYYY-MM-DD HH:mm' );
		const newStart = moment( updatedEvent.Start ).format( 'YYYY-MM-DD HH:mm' );
		if ( oldStart !== newStart || isScoreUpdate( event, updatedEvent ) ) {
			log.info( 'CHANGING START DATE ALREADY ENDED' );
			startJob = yield jobService.create( {
				'Name'              : station.Name,
				'Index'             : index,
				'Delay'             : newDelay,
				'FrequencyInterval' : frequencyInterval,
				'Event'             : updatedEvent,
				'EventDetails'      : _.map( ammendableDetails, details => _.pick( details, [ 'Description' ] ) )
			} )
			.then( updateJobId )
			.catch( err => log.fatal( err ) );
		}
	}

	const oldEnd = moment( event.End ).format( 'YYYY-MM-DD HH:mm' );
	const newEnd = moment( updatedEvent.End ).format( 'YYYY-MM-DD HH:mm' );
	if ( now.isBefore( ed ) &&  oldEnd !== newEnd ) {
		log.info( `CURRENT TIME (${now.format()}) IS BEFORE END TIME (${ed.format()})` );
		log.info( `OLD END JOB (${oldEnd} IS NOT EQUAK TO NEW END JOB (${newEnd})` );

		const oldEndJob = yield getEventEndJob( id );
		yield EventQueue.destroy( {
			'where' : {
				'EventId' : id
			}
		} );

		if ( updatedEvent.EventTypeId !== 3 ) {
			log.info( 'CREATE START JOB IN EVENTQUEUE', startJob.id );
			yield EventQueue.create( {
				'JobId'    : _.get( startJob, 'id', 0 ),
				'EventId'  : event.Id,
				'Duration' : delay,
				'Index'    : index
			} );
		}

		yield jobService.deleteById( oldEndJob );

		const endDelay = computeDelay( { 'Start' : updatedEvent.End, 'End' : updatedEvent.End }, 0 );
		const endJob   = yield jobService.create( {
			'Name'              : station.Name,
			'Index'             : 0,
			'Delay'             : endDelay,
			'FrequencyInterval' : frequencyInterval,
			'Event'             : updatedEvent,
			'EventDetails'      : _.map( ammendableDetails, details => _.pick( details, [ 'Description' ] ) )
		} )
		.catch( err => log.fatal( err ) );
		log.info( 'CREATED END JOB in REDIS', endJob.id );

		log.info( 'CREATE START JOB IN EVENTQUEUE', endJob.id );
		yield EventQueue.create( {
			'JobId'    : endJob.id,
			'EventId'  : event.Id,
			'Duration' : endDelay
		} );
	}
} );

module.exports = generoute( function* ( data ) {
	return completedStaticLiveMusic( data );
} );
