'use strict';

const _      = require( 'lodash' );
const moment = require( 'moment' );

const models = use( 'models' );

const dateRange          = use( 'utils/date-range' );
const generoute          = use( 'utils/generoute' );
const generecurrenceType = use( 'utils/generecurrence-type' );
const joinWeekdays       = use( 'utils/join-weekdays' );
const log                = use( './logging' );

const ValidationError = models.Sequelize.ValidationError;

const Event           = models.Event;
const EventRecurrence = models.EventRecurrence;

const formatRecurrence  = ( action, recurrence ) => {
		switch ( action ) {
			case 'DAILY_WITH_REPEATEVERY_AND_END' :
				return _.assign( {}, recurrence, { 'EndAfter' : null } );
			break;

			case 'DAILY_WITH_REPEATEVERY_AND_ENDAFTER' :
				return _.assign( {}, recurrence, { 'End' : null } );
			break;

			case 'DAILY_WITH_WEEKDAY_AND_END' :
				return _.assign( {}, recurrence, {
					'EndAfter'    : null,
					'RepeatEvery' : null
				} );
			break;

			case 'DAILY_WITH_WEEKDAY_AND_ENDAFTER' :
				return _.assign( {}, recurrence, {
					'End'         : null,
					'RepeatEvery' : null
				} );
			break;

			case 'WEEKLY_WITH_RECUR_AND_END' :
				return _.assign( {}, recurrence, {
					'DayOption' : 1,
					'EndAfter'  : null
				} );
			break;

			case 'WEEKLY_WITH_RECUR_AND_ENDAFTER' :
				return _.assign( {}, recurrence, {
					'DayOption' : 1,
					'End'       : null
				} );
			break;

			case 'MONTHLY_RECUR_ONE_DAY_A_MONTH_WITH_END' :
				return _.assign( {}, recurrence, {
					'EndAfter' : null
				} );
			break;

			case 'MONTHLY_RECUR_ONE_DAY_A_MONTH_WITH_ENDAFTER' :
				return _.assign( {}, recurrence, {
					'End' : null
				} );
			break;

			case 'MONTHLY_RECUR_ONE_DAY_ON_EVERY_N_MONTH_WITH_END' :
				return _.assign( {}, recurrence, {
					'EndAfter' : null,
					'Day'      : null
				} );
			break;

			case 'MONTHLY_RECUR_ONE_DAY_ON_EVERY_N_MONTH_WITH_ENDAFTER' :
				return _.assign( {}, recurrence, {
					'End' : null,
					'Day' : null
				} );
			break;
		}

		return recurrence;
};

module.exports = generoute( function* ( id, data ) {
	try {
		const eventService = use( 'services/events' );
		const recurrence   = data.EventRecurrence;
		const parentId     = _.result( data, 'ParentId', id );

		const ammendableEvent = _.pick( data, [
			'StationId',
			'EventTypeId',
			'Title',
			'TeamOne',
			'TeamTwo',
			'TeamOneScore',
			'TeamTwoScore',
			'TeamOneAbbrv',
			'TeamTwoAbbrv',
			'PartNo',
			'GamePartId'
		] );

		const ammendableSetting = _.pick( data.EventSetting, [
			'Id',
			'FrequencyTypeId',
			'FrequencyTypeValue',
			'Color'
		] );

		const ammendableEventDetails = _.map( data.EventDetails, detail => {
			return {
				'Id'          : detail.Id,
				'EventId'     : detail.EventId,
				'Description' : detail.Description
			};
		} );

		const eventOptions = {
			'where' : {
				'ParentId' : parentId
			},
			'include' : [
				models.Event.joinSetting( models )
			]
		};

		if ( !data.ParentId ) {
			eventOptions.where = {
				'Id' : parentId
			};
		}

		const events = yield Event.findAll( eventOptions );

		if ( !events.length ) {
			return null;
		}

		const {
			step,
			action,
			end,
			weekdays,
			day,
			monthWeekOption,
			monthWeekdayOption
		} = generecurrenceType( recurrence );

		const opt = {
			step,
			weekdays,
			day,
			monthWeekOption,
			monthWeekdayOption,
			'arrDate' : [ recurrence.Start, end ]
		};

		const range = dateRange( opt, action, date => date.format( 'YYYY-MM-DD' ) );
		log.info( range );
		const startTime = _.last( data.Start.split( 'T' ) );
		const utcRange  = _.chain( range )
			.map( date => moment.utc( `${date}T${startTime}` ) )
			.filter( date => !date.isBefore( moment.utc() ) )
			.map( date => date.format() )
			.value();

		const dbDate           = _.map( events, event => moment.utc( event.Start ).format() );
		const creationDate     = _.filter( utcRange, date => !_.includes( dbDate, date ) );
		const modificationDate = _.filter( utcRange, date => _.includes( dbDate, date ) );
		const deletionDate     = _.filter( dbDate, date => !_.includes( utcRange, date ) );

		const creationEvent = yield _.map( creationDate, generoute( function* ( date ) {
			const tokenParser = 'YYYY-MM-DDTHH:mm:ssZ';
			const endDate     = _.first( date.split( 'T' ) );
			const endTime     = _.last( data.End.split( 'T' ) );

			const newStart = moment.utc( date );
			const newEnd   = moment.utc( `${endDate}T${endTime}`, tokenParser );

			const underlap = yield Event.findUnderlap( {
				'ParentId'  : data.ParentId,
				'Start'     : newStart.format(),
				'End'       : newEnd.format(),
				'StationId' : data.StationId
			} );

			if ( underlap.length ) {
				const event = _.first( underlap );
				throw new ValidationError( 'ValidationError', [ {
					'message' : `Submitted date range overlaps Event: ${event.Title}.`
				} ] );
			}

			const inBetween = yield Event.findInBetween( {
				'ParentId'  : data.ParentId,
				'Start'     : newStart.format(),
				'End'       : newEnd.format(),
				'StationId' : data.StationId
			} );

			if ( inBetween.length ) {
				const event = _.first( inBetween );
				throw new ValidationError( 'ValidationError', [ {
					'message' : `Submitted date range inbetween Event: ${event.Title}.`
				} ] );
			}

			const newEvent = _.omit( data, [
				'Id','EventRecurrence',
				'CreatedBy', 'UpdatedBy',
				'created_at', 'updated_at',
				'EventSetting'
			] );

			const newEventSetting = _.omit( data.EventSetting, [
				'Id', 'EventId',
				'CreatedBy', 'UpdatedBy',
				'created_at', 'updated_at'
			] );

			const newEventDetails = _.map( data.EventDetails, detail => _.pick( detail, 'Description' ) );

			return _.assign( {}, newEvent, {
				'Start'        : newStart.format(),
				'End'          : newEnd.format(),
				'EventSetting' : newEventSetting,
				'ParentId'     : parentId,
				'EventDetails' : newEventDetails
			} );
		} ) );

		const modificationEvent = yield _.map( modificationDate, generoute( function* ( date ) {
			const tokenParser = 'YYYY-MM-DDTHH:mm:ssZ';
			const endDate     = _.first( date.split( 'T' ) );
			const endTime     = _.last( data.End.split( 'T' ) );

			const newStart = moment.utc( date );
			const newEnd   = moment.utc( `${endDate}T${endTime}`, tokenParser );

			const event               = _.find( events, event => moment.utc( event.Start ).format() === newStart.format() );
			ammendableSetting.EventId = event.EventSetting.Id;

			return _.assign( {}, ammendableEvent, {
				'Id'           : event.Id,
				'ParentId'     : parentId,
				'Start'        : newStart.format(),
				'End'          : newEnd.format(),
				'EventSetting' : ammendableSetting,
				'EventDetails' : ammendableEventDetails
			} );
		} ) );

		const deletionEvent = yield _.map( deletionDate, date => {
			const newStart     = moment.utc( date );
			const event        = _.find( events, event => moment.utc( event.Start ).format() === newStart.format() );

			if ( event ) {
				const eventStart = moment.utc( event.Start );
				const eventEnd   = moment.utc( event.End );
				const now        = moment.utc();

				if ( eventStart.isBefore( now ) && now.isBefore( eventEnd ) ) {
					return Promise.resolve( {
						'Id' : 0
					} );
				}
			}

			return Promise.resolve( {
				'Id'    : _.get( event, 'Id', 0 ),
				'Start' : newStart.format()
			} );
		} );

		const execute = generoute( function* () {
			yield _.map( deletionEvent, function* ( event ) {
				const response = yield eventService.deleteById( event.Id );
				log.info( 'DELETE-RECURRENCE:', response );

				if ( response instanceof ValidationError ) {
					throw response;
				}

				return response;
			} );

			yield _.map( modificationEvent, function* ( event ) {
				const response = yield eventService.updateById( event.Id, event );
				log.info( 'UPDATE-RECURRENCE:', response );

				if ( response instanceof ValidationError ) {
					throw response;
				}
				return response;
			} );

			yield _.map( creationEvent, function* ( event ) {
				const response = yield eventService.create( event );
				log.info( 'CREATE-RECURRENCE:', response );

				if ( response instanceof ValidationError ) {
					throw response;
				}

				return response;
			} );
		} );

		yield execute();

		const parent = yield Event.findById( parentId );
		if ( parent ) {
			yield parent.update( { 'ParentId' : parentId }, { 'fields' : [ 'ParentId' ] } );
		}

		const newRecurrence    = formatRecurrence( action, recurrence );
		newRecurrence.EventId  = parentId;
		newRecurrence.Weekdays = joinWeekdays( newRecurrence );
		yield EventRecurrence.upsert( newRecurrence );

		return data;
	} catch ( err ) {
		log.info( err );
		if ( err instanceof ValidationError ) {
			return err;
		}
		throw err;
	}
} );
