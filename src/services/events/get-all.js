'use strict';

const _ = require( 'lodash' );

const models        = use( 'models' );
const generoute     = use( 'utils/generoute' );
const splitWeekdays = use( 'utils/split-weekdays' );

const Event = models.Event;

module.exports = generoute( function* ( start, end, station ) {
	const options = {
		'attributes' : {
			'exclude' : [ 'deleted_at', 'JobId' ]
		},
		'include' : [
			models.Event.joinSetting( models ),
			models.Event.joinGamePart( models ),
			models.Event.joinRecurrence( models )
		],
		'where' : {
			'deleted_at' : null
		}
	};

	if ( start && end ) {
		options.where.Start = {
			'$between' : [ start, end ]
		};
	}

	if ( station ) {
		options.where.StationId = station;
	}

	const events = yield Event.findAll( options );

	return _.map( events, event => {
		const raw = event.toJSON();

		if ( raw.EventRecurrence ) {
			raw.EventRecurrence.Weekdays = splitWeekdays( raw.EventRecurrence );
		}

		return raw;
	} );
} );
