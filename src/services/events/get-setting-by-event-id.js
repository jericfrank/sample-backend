'use strict';

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const EventSetting = models.EventSetting;

module.exports = generoute( function* ( id ) {
	const setting = yield EventSetting.findOne( {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		},
		'include' : [ models.EventSetting.joinFrequency( models ) ],
		'where'   : {
			'EventId' : id
		}
	} );

	if ( !setting ) {
		return setting;
	}

	return setting.toJSON();
} );

