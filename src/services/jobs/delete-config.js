'use strict';

const _   = require( 'lodash' );
const kue = require( 'kue' );

module.exports = ( options ) => {
	return new Promise( ( resolve ) => {
		const queue  = kue.createQueue();

		const opt = _.assign( {}, options, {
			'title' : `DELETESTATIONCONFIG: ${options.Name}`
		} );

		const job = queue
			.create( 'delete-config', opt );

		job.save( () => {
			queue.shutdown();
		} );

		job.on( 'complete', ( res ) => {
			resolve( res );
		} );
	} );
};
