'use strict';

const kue    = require( 'kue' );

module.exports = ( JobId ) => {
	return new Promise ( ( resolve, reject ) => {
		const queue  = kue.createQueue();

		kue.Job.get( JobId, ( err, job ) => {
			if ( err ) {
				return reject( err );
			}

			return job.remove( err => {
				if ( err ) {
					return reject( err );
				}
				queue.shutdown();
				return resolve( true );
			} );
		} );
	} );
};
