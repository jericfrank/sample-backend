'use strict';

const _   = require( 'lodash' );
const kue = require( 'kue' );

module.exports = ( options ) => {
	return new Promise( ( resolve ) => {
		const queue  = kue.createQueue();

		const opt = _.assign( {}, options, {
			'title' : `CREATEFILE: ${options.Name}`
		} );

		const job = queue
			.create( 'create-file', opt )
			.delay( options.Delay );

		job.save( () => {
			resolve( job );
			queue.shutdown();
		} );
	} );
};
