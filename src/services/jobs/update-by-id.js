'use strict';

const _      = require( 'lodash' );
const kue    = require( 'kue' );
const moment = require( 'moment' );

module.exports = ( JobId, data ) => {
	return new Promise ( ( resolve, reject ) => {
		const queue  = kue.createQueue();

		kue.Job.get( JobId, ( err, job ) => {
			if ( err ) {
				return reject( err );
			}

			job.data = _.assign( {}, job.data, data );

			job.set( 'created_at', moment.utc().valueOf() );

			return job.delay( data.Delay ).save( () => {
				queue.shutdown();
				return resolve();
			} );
		} );
	} );
};
