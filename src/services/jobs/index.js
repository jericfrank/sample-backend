'use strict';

const create       = require( './create' );
const updateById   = require( './update-by-id' );
const createImage  = require( './create-image' );
const deleteById   = require( './delete-by-id' );
const createConfig = require( './create-config' );
const getConfig    = require( './get-config' );
const deleteConfig = require( './delete-config' );
const deleteFile   = require( './delete-file' );

module.exports = {
	create,
	updateById,
	createImage,
	deleteById,
	createConfig,
	getConfig,
	deleteConfig,
	deleteFile
};
