'use strict';

const _   = require( 'lodash' );
const kue = require( 'kue' );

module.exports = ( options ) => {
	return new Promise( ( resolve ) => {
		const queue  = kue.createQueue();

		const opt = _.assign( {}, options, {
			'title' : `CREATEIMAGE: ${options.Name}`
		} );

		const job = queue
			.create( 'create-image', opt );

		job.save( () => {
			queue.shutdown();
		} );

		job.on( 'complete', ( res ) => {
			resolve( res );
		} );
	} );
};
