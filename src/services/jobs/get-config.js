'use strict';

const _   = require( 'lodash' );
const kue = require( 'kue' );

module.exports = ( options ) => {
	return new Promise( ( resolve ) => {
		const queue  = kue.createQueue();

		const opt = _.assign( {}, options, {
			'title' : `GETSTATIONCONFIG: ${options.Name}`
		} );

		const job = queue
			.create( 'get-config', opt );

		job.save( () => {
			queue.shutdown();
		} );

		job.on( 'complete', ( res ) => {
			resolve( res );
		} );

		job.on( 'failed', () => {
			resolve( null );
		} );
	} );
};
