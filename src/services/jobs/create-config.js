'use strict';

const _   = require( 'lodash' );
const kue = require( 'kue' );

module.exports = ( options ) => {
	return new Promise( ( resolve ) => {
		const queue  = kue.createQueue();

		const opt = _.assign( {}, options, {
			'title' : `CREATESTATIONCONFIG: ${options.Name}`
		} );

		const job = queue
			.create( 'create-config', opt );

		job.save( () => {
			queue.shutdown();
		} );

		job.on( 'complete', ( res ) => {
			resolve( res );
		} );
	} );
};
