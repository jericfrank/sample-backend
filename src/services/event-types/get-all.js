'use strict';

const _ = require( 'lodash' );

const generoute = use( 'utils/generoute' );
const models    = use( 'models' );

const EventType = models.EventType;

module.exports = generoute( function* () {
	const types = yield EventType.findAll( {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		}
	} );

	return _.map( types, type => type.toJSON() );
} );
