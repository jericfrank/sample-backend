'use strict';

const _ = require( 'lodash' );

const generoute = use( 'utils/generoute' );
const models    = use( 'models' );

const EventGamePart = models.EventGamePart;

module.exports = generoute( function* () {
	const types = yield EventGamePart.findAll( {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		}
	} );

	return _.map( types, type => type.toJSON() );
} );
