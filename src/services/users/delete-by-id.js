'use strict';

const models    = require( '../../models' );
const generoute = require( '../../utils/generoute' );

const User = models.User;

module.exports = generoute( function* ( id ) {
	const user = yield User.findById( id );

	if ( !user ) {
		return null;
	}

	const deletedUser = yield User.destroy( {
		'where' : {
			'Id' : id
		}
	} );

	return deletedUser;
} );

