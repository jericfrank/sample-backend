'use strict';

const create         = require( './create' );
const getAll         = require( './get-all' );
const updateById     = require( './update-by-id' );
const getById        = require( './get-by-id' );
const deleteById     = require( './delete-by-id' );
const forgotPassword = require( './forgot-password' );
const resetPassword  = require( './reset-password' );
const getCurrentUser = require( './get-current-user' );

module.exports = {
	create,
	getAll,
	updateById,
	getById,
	deleteById,
	forgotPassword,
	resetPassword,
	getCurrentUser
};
