'use strict';

const _         = require( 'lodash' );
const SparkPost = require( 'sparkpost' );

const config               = use( 'config' );
const models               = use( 'models' );
const generoute            = use( 'utils/generoute' );
const geneResetEmail       = use( 'utils/genereset-email' );
const subscriptionExceeded = use( 'utils/subscription-exceeded' );
const userPermission       = use( 'utils/is-admin' );

const User            = models.User;
const UserStation     = models.UserStation;
const ValidationError = models.Sequelize.ValidationError;
const sparky          = new SparkPost( config.app.sparkPostKey );

const assignStations = stationIds => {
	const removeDeletedAt = _.partialRight( _.omit, 'deleted_at' );
	const assign          = _.partialRight( _.assign, { 'Stations' : stationIds } );
	const removeAndAssign = _.flowRight( assign, removeDeletedAt );
	return user => removeAndAssign( user );
};

module.exports = generoute( function* ( id, data ) {
	try {
		const isAdmin = yield userPermission( id );
		if ( !isAdmin ) {
			const isExceeded = yield subscriptionExceeded( 2 );
			if ( isExceeded ) {
				throw new ValidationError( 'ValidationError', [ {
					'message' : 'Subscription max limit exceeded.'
				} ] );
			}
		}

		const defaultData = {
			'ProfileId'       : _.result( data, 'ProfileId', 1 ),
			'MiddleName'      : _.result( data, 'MiddleName', '' ),
			'StationId'       : 1,
			'AccountDisabled' : false
		};

		const updatedData = _.assign( {}, data, defaultData );
		const user        = yield User.create( updatedData );

		const sendOption    = geneResetEmail( user );
		sparky.transmissions.send( sendOption );

		const stations     = _.map( data.Stations, stationId => {
			return { 'StationId' : stationId, 'UserId' : user.Id };
		} );

		yield UserStation.bulkCreate( stations );

		const assignStationsToUser = assignStations( data.Stations );
		return assignStationsToUser( user.toJSON() );
	} catch ( error ) {
		return error;
	}
} );
