'use strict';

const models         = use( 'models' );
const generoute      = use( 'utils/generoute' );
const formatStations = use( 'utils/format-stations' );

const User = models.User;

module.exports = generoute( function* ( id ) {
	const user = yield User.findById( id, {
		'attributes' : {
			'exclude' : [ 'deleted_at', 'Password' ]
		},
		'where' : {
			'deleted_at' : null
		}
	} );

	if ( !user ) {
		return user;
	}

	const stations = yield user.getStations( {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		}
	} );

	const assignStationsToUser = formatStations( stations );

	return assignStationsToUser( user.toJSON() );
} );

