'use strict';

const _ = require( 'lodash' );

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const User        = models.User;
const UserStation = models.UserStation;

module.exports = generoute( function* ( id, payload ) {
	const user = yield User.findById( id );

	if ( !user ) {
		return user;
	}

	const updatedData = _.omit( payload, [
		'Stations',
		'created_at',
		'updated_at',
		'CreatedBy',
		'UpdatedBy'
	] );

	const attrs = _.keys( updatedData );

	const updatedUser = yield user.update( updatedData, { 'fields' : attrs } );

	if ( updatedUser.ProfileId !== 1 ) {
		const stations     = _.map( payload.Stations, stationId => {
			return { 'StationId' : stationId, 'UserId' : user.Id };
		} );

		yield UserStation.destroy( {
			'where' : {
				'UserId' : user.Id
			}
		} );

		yield UserStation.bulkCreate( stations );
	}

	return _.omit( updatedUser.toJSON(), [ 'Password', 'CreatedBy', 'UpdatedBy', 'created_at', 'updated_at', 'deleted_at' ] );
} );
