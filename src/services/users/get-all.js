'use strict';

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const User = models.User;

module.exports =  generoute( function* () {
	const users = yield User.findAll( {
		'attributes' : {
			'exclude' : [ 'Password', 'deleted_at' ]
		},
		'where' : {
			'deleted_at' : null
		}
	} );

	return users;
} );
