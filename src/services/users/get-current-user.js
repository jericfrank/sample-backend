'use strict';

const _   = require( 'lodash' );
const jwt = require( 'jwt-simple' );

const config    = use( 'config' );
const generoute = use( 'utils/generoute' );
const models    = use( 'models' );

const User  = models.User;

const formatStations = stations => {
	const newStation = _.map( stations, station => {
		const removeUserStation      = _.partialRight( _.omit, 'UserStation' );
		const getDefault             = station => _.result( station, 'UserStation.Default', false );
		const assignDefaultToStation = _.partialRight( _.assign, {
			'Default' : getDefault( station )
		} );

		const removeAndAssignToStation = _.flowRight( assignDefaultToStation, removeUserStation );
		return removeAndAssignToStation( station.toJSON() );
	} );

	return user => _.assign( user, { 'Stations' : newStation } );
};

const assignPrivilegesToUser = privileges => {
	const removePassword         = _.partialRight( _.omit, 'Password' );
	const assignPrivileges       = _.partialRight( _.assign, { 'Privileges' : privileges } );
	const getProfile             = _.partialRight( _.result, 'Profile' );
	const removeAndGetProfile    = _.flowRight( getProfile, removePassword );
	const removeAndAssignProfile = _.flowRight( assignPrivileges, removeAndGetProfile );

	return user => _.assign( user, { 'Profile' : removeAndAssignProfile( user ) } );
};

module.exports = generoute( function* ( data ) {
	const privilegeService = use( 'services/privileges' );
	const decoded          = jwt.decode( data.authorization, config.app.jwtKey );
	const user             = yield User.findById( decoded.sub, {
		'attributes' : {
			'exclude' : [ 'Password', 'deleted_at' ]
		},
		'include' : [ {
			'model'      : models.Profile,
			'as'         : 'Profile',
			'attributes' : {
				'exclude' : [ 'deleted_at' ]
			}
		} ]
	} );

	if ( !user ) {
		return user;
	}

	const stations = yield user.getStations( {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		},
		'include' : [ models.Station.joinSetting( models ) ]
	} );

	const privileges           = yield privilegeService.getAll( user.ProfileId );
	const getUserInfo          = assignPrivilegesToUser( privileges );
	const assignStationsToUser = formatStations( stations );

	return assignStationsToUser( getUserInfo( user.toJSON() ) );
} );
