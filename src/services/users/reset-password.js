'use strict';

const jwt = require( 'jwt-simple' );

const config    = use( 'config' );
const log       = use( 'logging' );
const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const User   = models.User;

module.exports = generoute( function* ( data ) {
	try {
		const decoded = jwt.decode( data.Token, config.app.jwtKey );
		const user    = yield User.findOne( {
			'where' : {
				'EmailAddress' : decoded.sub
			}
		} );

		if ( !user ) {
			return user;
		}

		yield user.update( {
			'Password' : data.Password
		} );

		return {
			'Operation' : true
		};
	} catch ( error ) {
		log.fatal( error );
		return error;
	}
} );
