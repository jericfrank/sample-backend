'use strict';

const _         = require( 'lodash' );
const SparkPost = require( 'sparkpost' );

const config         = use( 'config' );
const log            = use( 'logging' );
const models         = use( 'models' );
const generoute      = use( 'utils/generoute' );
const geneResetEmail = use( 'utils/genereset-email' );

const User   = models.User;
const sparky = new SparkPost( config.app.sparkPostKey );

module.exports = generoute( function* ( data ) {
	try {
		const user = yield User.findOne( {
			'where' : _.pick( data, 'EmailAddress' )
		} );

		if ( !user ) {
			return user;
		}

		const sendOption    = geneResetEmail( user );
		const emailResponse = yield sparky.transmissions.send( sendOption );

		return {
			'Operation' : Boolean( emailResponse.results.total_accepted_recipients )
		};
	} catch ( error ) {
		log.fatal( error );
		return error;
	}
} );
