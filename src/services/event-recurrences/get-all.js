'use strict';

const _ = require( 'lodash' );

const generoute     = use( 'utils/generoute' );
const splitWeekdays = use( 'utils/split-weekdays' );

const models              = use( 'models' );
const { EventRecurrence } = models;

module.exports = generoute( function* ( eventId ) {
	const options = {
		'attributes' : {
			'exclude' : [ 'deleted_at', 'created_at', 'updated_at' ]
		}
	};

	if ( eventId ) {
		options.where = {
			'EventId' : eventId
		};
	}

	const recurrences = yield EventRecurrence.findAll( options );

	return _.map( recurrences, recurrence => {
		return _.assign( {}, recurrence.toJSON(), {
			'Weekdays' : splitWeekdays( recurrence )
		} );
	} );
} );
