'use strict';

const _ = require( 'lodash' );

const generoute = use( 'utils/generoute' );
const models    = use( 'models' );

const EventDetail = models.EventDetail;

module.exports = generoute( function* ( eventId ) {
	const options = {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		}
	};

	if ( eventId ) {
		options.where = {
			'EventId' : eventId
		};
	}

	const eventDetails = yield EventDetail.findAll( options );

	return _.map( eventDetails, eventDetail => eventDetail.toJSON() );
} );
