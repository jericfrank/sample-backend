'use strict';

const models    = require( '../../models' );
const generoute = require( '../../utils/generoute' );

const Profile = models.Profile;

module.exports = generoute( function* ( id ) {
	const profile = yield Profile.findById( id );
	return profile;
} );

