'use strict';

const getAll  = require( './get-all' );
const getById = require( './get-by-id' );

module.exports = { getAll, getById };
