'use strict';

const models    = require( '../../models' );
const generoute = require( '../../utils/generoute' );

const Profile = models.Profile;

module.exports = generoute( function* () {
	try {
		const profiles = yield Profile.findAll( {
			'attributes' : {
				'exclude' : [ 'deleted_at' ]
			},
			'order' : [
				[ 'Name' , 'ASC' ]
			]
		} );

		return profiles;
	} catch ( error ) {
		throw error;
	}
} );
