'use strict';

const _ = require( 'lodash' );

const models         = use( 'models' );
const generoute      = use( 'utils/generoute' );
const base64toImage  = use( 'utils/base64-to-img' );
const getContentType = use( 'utils/get-content-type' );
const config         = use( 'config' );

const { CompanyProfile, Sequelize } = models;
const ValidationError               = Sequelize.ValidationError;

module.exports = generoute( function* ( payload ) {
	try {
		const jobService = use( 'services/jobs' );
		const company    = yield CompanyProfile.findOne();

		let CompanyLogo = _.get( company, 'CompanyLogo', null );
		if ( payload.ImageString ) {
			const Name        = 'company-profile';
			const Filepath    = yield base64toImage( payload.ImageString );
			const ContentType = getContentType( payload.ImageString );
			const imageUrl    = yield jobService.createImage( { Filepath, Name, ContentType } );
			CompanyLogo       = `${config.app.imageProxy}//${imageUrl}`;
		}

		const newPayload   = _.assign( {}, payload, { CompanyLogo } );
		const companyAttrs = _.keys( _.omit( newPayload, [ 'ImageString' ] ) );

		if ( !company ) {
			yield CompanyProfile.create( newPayload, { 'fields' : companyAttrs } );
		} else {
			yield company.update( newPayload, { 'fields' : companyAttrs } );
		}

		const companies = yield CompanyProfile.findAll( {
			'attributes' : {
				'exclude' : [ 'Id', 'created_at', 'updated_at', 'deleted_at' ]
			}
		} );

		const newCompany = _.first( companies );

		return newCompany.toJSON();
	} catch ( error ) {
		if ( error.message === 'image base64 data error' ) {
			return new ValidationError( 'ValidationError', [ {
				'message' : 'ImageString is not base64'
			} ] );
		}
		throw error;
	}
} );
