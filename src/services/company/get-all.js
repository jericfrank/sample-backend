'use strict';

const _ = require( 'lodash' );

const generoute = use( 'utils/generoute' );
const models    = use( 'models' );

const { CompanyProfile } = models;

module.exports = generoute( function* () {
	const options = {
		'attributes' : {
			'exclude' : [ 'Id', 'created_at', 'updated_at', 'deleted_at' ]
		}
	};

	const companies = yield CompanyProfile.findAll( options );

	const newCompany = _.first( companies );

	return _.result( newCompany, 'toJSON', null );
} );
