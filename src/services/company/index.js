'use strict';

const createOrUpdate = require( './create-or-update' );
const getAll         = require( './get-all' );

module.exports = { createOrUpdate, getAll };
