'use strict';

const _ = require( 'lodash' );

const generoute = use( 'utils/generoute' );
const models    = use( 'models' );

const EventFrequencyType = models.EventFrequencyType;

module.exports = generoute( function* () {
	const types = yield EventFrequencyType.findAll( {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		},
		'include' : [ {
			'model' : models.UnitOfMeasurement,
			'as'    : 'UnitOfMeasurement'
		} ]
	} );

	return _.map( types, type => type.toJSON() );
} );
