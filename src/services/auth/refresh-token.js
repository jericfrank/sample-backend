'use strict';

const _   = require( 'lodash' );
const jwt = require( 'jwt-simple' );

const config    = use( 'config' );
const generoute = use( 'utils/generoute' );
const genetoken = use( 'utils/genetoken' );

module.exports = generoute( function* ( data ) {
	try {
		const authorization = _.last( data.authorization.split( ' ' ) );
		const decoded       = jwt.decode( authorization, config.app.jwtKey, true );

		const authUser = {
			'Token' : genetoken( decoded.sub )
		};

		return authUser;
	} catch ( error ) {
		return error;
	}
} );
