'use strict';

const authenticate = require( './authenticate' );
const refreshToken = require( './refresh-token' );
const logout       = require( './logout' );

module.exports = { authenticate, refreshToken, logout };
