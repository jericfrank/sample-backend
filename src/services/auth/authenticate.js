'use strict';

const _      = require( 'lodash' );
const bcrypt = require( 'bcrypt' );

const models         = use( 'models' );
const generoute      = use( 'utils/generoute' );
const genetoken      = use( 'utils/genetoken' );
const formatStations = use( 'utils/format-stations' );

const User            = models.User;

const assignPrivilegesToUser = privileges => {
	const removePassword         = _.partialRight( _.omit, 'Password' );
	const assignPrivileges       = _.partialRight( _.assign, { 'Privileges' : privileges } );
	const getProfile             = _.partialRight( _.result, 'Profile' );
	const removeAndAssignProfile = _.flowRight( assignPrivileges, getProfile );

	return user => {
		return removePassword( _.assign( user, {
			'Profile' : removeAndAssignProfile( user )
		} ) );
	};
};

module.exports = generoute( function* ( data ) {
	const privilegeService = use( 'services/privileges' );
	const compare          = function compare ( requestPassword, userPassword ) {
		return new Promise ( ( resolve, reject ) => {
			bcrypt.compare( requestPassword, userPassword, ( err, isMatch ) => {
				if ( err ) {
					return reject( err );
				}

				return resolve( isMatch );
			} );
		} );
	};

	try {
		const options = {
			'attributes' : {
				'exclude' : [ 'deleted_at' ]
			},
			'where'   : _.pick( data, 'UserName' ),
			'include' : [ {
				'model'      : models.Profile,
				'as'         : 'Profile',
				'attributes' : {
					'exclude' : [ 'deleted_at' ]
				}
			} ]
		};

		const user = yield User.findOne( options );

		if ( !user ) {
			return user;
		}

		const isMatch = yield compare( data.Password, user.Password );

		if ( !isMatch ) {
			return isMatch;
		}

		if ( user.AccountDisabled ) {
			return {
				'accountDisabled' : 'Account is disabled.'
			};
		}

		const privileges  = yield privilegeService.getAll( user.ProfileId );
		const getUserInfo = assignPrivilegesToUser( privileges );

		const stations = yield user.getStations( {
			'attributes' : {
				'exclude' : [ 'deleted_at' ]
			},
			'include' : [ models.Station.joinSetting( models ) ]
		} );

		const assignStationsToUser = formatStations( stations );

		const authUser = {
			'Token' : genetoken( user.Id ),
			'User'  : assignStationsToUser( getUserInfo( user.toJSON() ) )
		};

		return authUser;
	} catch ( e ) {
		return e;
	}
} );
