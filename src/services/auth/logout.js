'use strict';

const _      = require( 'lodash' );
const moment = require( 'moment' );
const jwt    = require( 'jsonwebtoken' );
const redis  = require( 'redis' );

const config    = use( 'config' );
const generoute = use( 'utils/generoute' );

module.exports = generoute( function* ( data ) {
	const client          = redis.createClient();
	const authorization   = _.last( data.authorization.split( ' ' ) );
	const decoded         = jwt.decode( authorization, config.app.jwtKey );
	const date            = moment.unix( decoded.exp ).format( 'YYYY-MM-DD' );
	const expirationToken = moment.unix( decoded.exp );
	const duration        = expirationToken.diff( moment(), 'seconds' );

	const add = ( key, value ) => {
		return new Promise( ( resolve, reject ) => {
			client.sadd( key, value, ( err, replies ) => {
				if ( err ) {
					return reject( err );
				}

				return resolve( replies );
			} );
		} );
	};

	const expire = ( key, expiration ) => {
		return new Promise( ( resolve, reject ) => {
			client.expire( key, expiration, ( err, replies ) => {
				if ( err ) {
					return reject( err );
				}

				return resolve( replies );
			} );
		} );
	};

	yield add( date, authorization );
	yield expire( date, duration );

	client.quit();

	return {
		'Operation' : true
	};
} );
