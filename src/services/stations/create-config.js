'use strict';

const generoute = use( 'utils/generoute' );
const models    = use( 'models' );

const { Station, Sequelize } = models;
const { ValidationError }    = Sequelize;

module.exports = generoute( function* ( data ) {
	try {
		const jobService = use( 'services/jobs' );
		const station = yield Station.findById( data.StationId );

		const jobData = {
			'Name' : station.Name,
			'Text' : data.Text
		};

		yield jobService.createConfig( jobData );

		return jobData;
	} catch ( error ) {
		if ( error instanceof ValidationError ) {
			return error;
		}

		throw error;
	}
} );
