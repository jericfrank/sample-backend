'use strict';

const _ = require( 'lodash' );

const createSetting        = use( 'services/stations/create-calendar-setting' );
const generoute            = use( 'utils/generoute' );
const models               = use( 'models' );
const subscriptionExceeded = use( 'utils/subscription-exceeded' );
const userPermission       = use( 'utils/is-admin' );

const Station         = models.Station;
const ValidationError = models.Sequelize.ValidationError;

module.exports = generoute( function* ( id, data ) {
	try {
		const isAdmin = yield userPermission( id );
		if ( !isAdmin ) {
			const isExceeded = yield subscriptionExceeded( 1 );
			if ( isExceeded ) {
				throw new ValidationError( 'ValidationError', [ {
					'message' : 'Subscription max limit exceeded.'
				} ] );
			}
		}

		const station         = yield Station.create( data );
		const calendarSetting = yield createSetting( {
			'Name'        : station.Name,
			'StartOfWeek' : data.CalendarSetting.StartOfWeek,
			'StationId'   : station.Id
		} );

		return _.assign( {}, _.omit( station.toJSON(), 'deleted_at' ), {
			'CalendarSetting' : calendarSetting
		} );
	} catch ( error ) {
		return error;
	}
} );
