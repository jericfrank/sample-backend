'use strict';

const _   = require( 'lodash' );
const kue = require( 'kue' );

const updateSetting = use( 'services/stations/update-calendar-setting' );
const models        = use( 'models' );
const generoute     = use( 'utils/generoute' );

const Station = models.Station;

const renameFileGenerator = function* ( file ) {
	if ( file.OldName === file.NewName ) {
		return file.OldName;
	}

	const queue = kue.createQueue();
	queue.create( 'copy-file', {
		'title'  : `RENAMEFILE: ${file.OldName} to ${file.NewName}`,
		'OldKey' : file.OldName,
		'NewKey' : file.NewName
	} )
	.delay( 5000 )
	.save();

	queue.shutdown();

	return file.NewName;
};


module.exports = generoute( function* ( id, payload ) {
	try {
		const station = yield Station.findById( id );

		if ( !station ) {
			return station;
		}

		const oldName = station.Name;

		const updatedStation = yield station.update( payload );

		const updatedSetting = yield updateSetting( payload.CalendarSetting.Id, {
			'StartOfWeek' : payload.CalendarSetting.StartOfWeek
		} );

		const maybeRenameFile = generoute( _.partialRight( renameFileGenerator, updatedStation ) );
		maybeRenameFile( {
			'OldName' : oldName,
			'NewName' : updatedStation.Name
		} );

		return _.assign( {}, _.omit( updatedStation.toJSON(), 'deleted_at' ), {
			'CalendarSetting' : updatedSetting
		} );
	} catch ( err ) {
		if ( err instanceof models.Sequelize.UniqueConstraintError ) {
			return err;
		}

		throw err;
	}
} );
