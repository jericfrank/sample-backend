'use strict';

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const Station = models.Station;

module.exports =  generoute( function* ( id ) {
	const jobService = use( 'services/jobs' );

	const station = yield Station.findById( id );

	if ( !station ) {
		return station;
	}

	const content = yield jobService.getConfig( station.toJSON() );

	return {
		'Text' : content
	};
} );
