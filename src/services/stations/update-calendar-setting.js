'use strict';

const _ = require( 'lodash' );

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const CalendarSetting = models.CalendarSetting;

module.exports = generoute( function* ( id, payload ) {
	const setting = yield CalendarSetting.findById( id );

	if ( !setting ) {
		return setting;
	}

	const updatedSetting = yield setting.update( payload );

	return _.omit( updatedSetting.toJSON(), 'deleted_at' );
} );
