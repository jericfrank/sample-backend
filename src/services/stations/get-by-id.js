'use strict';

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const Station = models.Station;

module.exports =  generoute( function* ( id ) {
	const station = yield Station.findById( id, {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		},
		'include' : [ {
			'model'      : models.CalendarSetting,
			'as'         : 'CalendarSetting',
			'attributes' : {
				'exclude' : [ 'deleted_at' ]
			}
		} ]
	} );

	if ( !station ) {
		return station;
	}

	return station.toJSON();
} );
