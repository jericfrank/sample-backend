'use strict';

const _ = require( 'lodash' );

const config    = use( 'config' );
const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const CalendarSetting = models.CalendarSetting;

module.exports = generoute( function* ( data ) {
	const name        = _.kebabCase( data.Name.toLowerCase() );
	const folderName  = `${config.s3.url}${name}.txt`;

	const defaultData = {
		'StationId'       : data.StationId,
		'StorageLocation' : folderName,
		'StartOfWeek'     : data.StartOfWeek
	};

	const setting = yield CalendarSetting.create( defaultData );
	return _.omit( setting.toJSON(), 'deleted_at' );
} );
