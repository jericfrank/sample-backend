'use strict';

const _ = require( 'lodash' );

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const Station = models.Station;

module.exports =  generoute( function* () {
	const stations = yield Station.findAll( {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		},
		'include' : [ {
			'model'      : models.CalendarSetting,
			'as'         : 'CalendarSetting',
			'attributes' : {
				'exclude' : [ 'deleted_at' ]
			}
		} ],
		'where' : {
			'deleted_at' : null
		}
	} );

	return _.map( stations, station => station.toJSON() );
} );
