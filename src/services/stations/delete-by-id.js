'use strict';

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const CalendarSetting = models.CalendarSetting;
const Station         = models.Station;
const User            = models.User;

module.exports = generoute( function* ( Id ) {
	const jobService     = use( 'services/jobs' );
	const stationService = use( 'services/stations' );

	const station = yield Station.findById( Id, {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		},
		'where' : {
			'deleted_at' : null
		},
		'include' : [ {
			'model' : User,
			'as'    : 'Users'
		} ]
	} );

	if ( !station ) {
		return station;
	}

	if ( station.Users.length > 0 ) {
		return {
			'errorType' : 'badData',
			'message'   : 'station has a reference to user'
		};
	}

	yield stationService.deleteConfig( Id );
	yield jobService.deleteFile( station.toJSON() );

	yield CalendarSetting.destroy( {
		'where' : {
			'StationId' : Id
		}
	} );

	const deletedStation = yield Station.destroy( {
		'where' : {
			Id
		}
	} );

	return {
		'Operation' : Boolean( deletedStation )
	};
} );

