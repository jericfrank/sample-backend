'use strict';

const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const { Station } = models;

module.exports = generoute( function* ( Id ) {
	const jobService = use( 'services/jobs' );
	const station    = yield Station.findById( Id );

	if ( !station ) {
		return station;
	}

	const response = yield jobService.deleteConfig( station.toJSON() );

	return {
		'Operation' : Boolean( response )
	};
} );

