'use strict';

module.exports = {
	'500' : { 'description' : 'tala tala ang gahimo sa api ( The server encountered an unexpected condition )' },
	'400' : { 'description' : 'Check json payload for malformed syntax or invalid args' },
	'401' : { 'description' : 'The request requires user authentication' }
};
