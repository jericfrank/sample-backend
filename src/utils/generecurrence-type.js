'use strict';

module.exports = function ( recurrence ) {
	let step;
	let end;
	let action;
	let weekdays;
	let day;
	let monthWeekOption;
	let monthWeekdayOption;

	switch ( recurrence.RepeatOption ) {
		case 1 :
			if ( recurrence.DayOption === 1 ) {
				step   = recurrence.RepeatEvery;
				action = 'DAILY_WITH_REPEATEVERY_AND_END';
				end    = recurrence.End;

				if ( recurrence.EndAfter ) {
					action = 'DAILY_WITH_REPEATEVERY_AND_ENDAFTER';
					end    = recurrence.EndAfter;
				}
			}

			if ( recurrence.DayOption === 2 ) {
				step   = 1;
				action = 'DAILY_WITH_WEEKDAY_AND_END';
				end    = recurrence.End;

				if ( recurrence.EndAfter ) {
					action = 'DAILY_WITH_WEEKDAY_AND_ENDAFTER';
					end    = recurrence.EndAfter;
				}
			}
		break;

		case 2 :
			step     = recurrence.RepeatEvery;
			action   = 'WEEKLY_WITH_RECUR_AND_END';
			end      = recurrence.End;
			weekdays = recurrence.Weekdays;

			if ( recurrence.EndAfter ) {
				action = 'WEEKLY_WITH_RECUR_AND_ENDAFTER';
				end    = recurrence.EndAfter;
			}
		break;

		case 3 :
			if ( recurrence.MonthOption === 1 ) {
				step   = recurrence.RepeatEvery;
				action = 'MONTHLY_RECUR_ONE_DAY_A_MONTH_WITH_END';
				end    = recurrence.End;
				day    = recurrence.Day;

				if ( recurrence.EndAfter ) {
					action = 'MONTHLY_RECUR_ONE_DAY_A_MONTH_WITH_ENDAFTER';
					end    = recurrence.EndAfter;
				}
			}

			if ( recurrence.MonthOption === 2 ) {
				step               = recurrence.RepeatEvery;
				action             = 'MONTHLY_RECUR_ONE_DAY_ON_EVERY_N_MONTH_WITH_END';
				end                = recurrence.End;
				monthWeekOption    = recurrence.MonthWeekOption;
				monthWeekdayOption = recurrence.MonthWeekdaysOption;

				if ( recurrence.EndAfter ) {
					action = 'MONTHLY_RECUR_ONE_DAY_ON_EVERY_N_MONTH_WITH_ENDAFTER';
					end    = recurrence.EndAfter;
				}
			}
		break;
	}

	return { step, action, end, weekdays, day, monthWeekOption, monthWeekdayOption };
};
