'use strict';

const _ = require( 'lodash' );

const models    = use( 'models' );
const User      = models.User;

const findWithPrivileges = ( id ) => {
	const options = {
		'attributes' : {
			'exclude' : [ 'deleted_at' ]
		},

		'where' : { id }
	};

	return User.findOne( options );
};

module.exports = ( credentials, callback ) => {
	const privilegeService = use( 'services/privileges' );
	findWithPrivileges( credentials.sub ).then( user => {
		privilegeService.getAll( user.ProfileId ).then( privileges => {
			const reducer = ( permission, privilege ) => {
				const key         = privilege.Module.RouteName;
				const value       = _.pick( privilege, [ 'CanRead', 'CanCreate', 'CanUpdate', 'CanDelete' ] );
				permission[ key ] = value;
				return permission;
			};
			const permissions = _.reduce( privileges, reducer, {} );
			callback( null, permissions );
		} );
	} );
};
