'use strict';

const _       = require( 'lodash' );
const moment  = require( 'moment' );
const ordinal = require( 'ordinal' );

const implementStaticContent = ( event, eventDetails, delay, index ) => {
	const ed   = moment.utc( event.End, 'YYYY-MM-DDTHH:mm:ssZ' );

	let body = 'Static|';
	if ( moment.utc().isBefore( ed ) ) {
		body += _.get( eventDetails[ index ], 'Description', null );
	} else {
		body = '';
	}

	return body;
};

const implementLiveContent = ( event ) => {
	const ed = moment.utc( event.End, 'YYYY-MM-DDTHH:mm:ssZ' );
	const {
		TeamOne, TeamOneAbbrv, TeamOneScore,
		TeamTwo, TeamTwoAbbrv, TeamTwoScore,
		PartNo, GamePart
	} = event;

	const gamePart = _.get( GamePart, 'Name', '' );
	let ord        = `${ordinal( PartNo )} `;

	if ( ord === '0th ' ) {
		ord = '0 ';
	}

	if ( gamePart === 'F' ) {
		ord = '';
	}

	const ordGamePart = `${ord} ${gamePart}`;

	let body = [ 'Live' ];
	if ( moment.utc().isBefore( ed ) ) {
		body = body.concat( TeamOne, TeamTwo, TeamOneAbbrv, TeamTwoAbbrv, TeamOneScore, TeamTwoScore, ordGamePart ).join( '|' );
	} else {
		body = '';
	}

	return body;
};

const implementMusicContent = ( event ) => {
	const ed = moment.utc( event.End, 'YYYY-MM-DDTHH:mm:ssZ' );

	let body;
	if ( moment.utc().isBefore( ed ) ) {
		body = 'Music|';
	} else {
		body = '';
	}

	return body;
};

const builder = {
	'1' : implementStaticContent,
	'2' : implementLiveContent,
	'3' : implementMusicContent
};

module.exports = ( event, eventDetails, Delay, Index ) => {
	return builder[ event.EventTypeId ]( event, eventDetails, Delay, Index );
};
