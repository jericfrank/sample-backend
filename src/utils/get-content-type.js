'use strict';

const _ = require( 'lodash' );

module.exports = string => {
	const arr      = string.split( ';' );
	const metaInfo = _.first( arr ).split( ':' );
	return _.last( metaInfo );
};
