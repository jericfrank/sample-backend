'use strict';

const _               = require( 'lodash' );
const models          = use( 'models' );
const ValidationError = models.Sequelize.ValidationError;

const implementStation = limit => {
	const stationService = use( 'services/stations' );
	return stationService.getAll()
		.then( stations => stations.length >= limit );
};

const implementUser = limit => {
	const userService = use( 'services/users' );
	return userService.getAll().then( users => {
		const notAdminUsers = _.filter( users, user => user.ProfileId !== 1 );
		return notAdminUsers.length >= limit;
	} );
};

const builder = {
	'1' : implementStation,
	'2' : implementUser
};

module.exports = ( type  ) => {
	const companyService = use( 'services/company' );
	return companyService.getAll().then( company => {
		if ( company.PricingType > 2 ) {
			throw new ValidationError( 'ValidationError', [ {
				'message' : 'Unknown Pricing Type.'
			} ] );
		}

		if ( company.PricingType !== type ) {
			return false;
		}

		return builder[ String( type ) ]( company.MaxLimit );
	} );
};
