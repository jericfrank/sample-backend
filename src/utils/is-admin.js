'use strict';

const { User } = use( 'models' );

module.exports = userId => {
	const options = {
		'attributes' : [ 'ProfileId' ]
	};
	return User.findById( userId, options )
		.then( user => user.ProfileId === 1 );
};
