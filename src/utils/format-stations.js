'use strict';

const _ = require( 'lodash' );

module.exports = stations => {
	const newStation = _.map( stations, station => {
		const removeUserStation      = _.partialRight( _.omit, 'UserStation' );
		const getDefault             = station => _.result( station, 'UserStation.Default', false );
		const assignDefaultToStation = _.partialRight( _.assign, {
			'Default' : getDefault( station )
		} );

		const removeAndAssignToStation = _.flowRight( assignDefaultToStation, removeUserStation );
		return removeAndAssignToStation( station.toJSON() );
	} );

	return user => _.assign( user, { 'Stations' : newStation } );
};
