'use strict';

const _ = require( 'lodash' );

module.exports = ( recurrence ) => {
	let strWeekdays = _.result( recurrence, 'Weekdays', '' );

	if ( !strWeekdays ) {
		strWeekdays = '';
	}

	return strWeekdays
		.split( ',' )
		.filter( str => str )
		.map( str => parseInt( str, 10 ) );
};
