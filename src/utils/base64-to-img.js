'use strict';

const base64Img = require( 'base64-img' );
const moment    = require( 'moment' );

module.exports = base64String => {
	return new Promise( ( resolve, reject ) => {
		base64Img.img( base64String, '/tmp', `img-${moment.utc().valueOf()}`, ( err, filepath ) => {
			if ( err ) {
				return reject( err );
			}

			return resolve( filepath );
		} );
	} );
};
