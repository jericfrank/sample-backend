'use strict';

const _    = require( 'lodash' );
const Boom = require( 'boom' );
const Hoek = require( 'hoek' );

const internals = {
	'pluginName' : 'routeAcl'
};

exports.register = function ( server, options, next ) {
	if ( _.isUndefined( options.permissionFunc ) ) {
		return next( new Error( 'options.permissionFunc is required' ) );
	}

	if ( !_.isFunction( options.permissionFunc ) ) {
		return next( new Error( 'options.permissionFunc must be a valid function' ) );
	}

	internals.permissionFunc = options.permissionFunc;
	server.ext( 'onPostAuth', internals.implementation );
	return next();
};

exports.register.attributes = {
	'name'    : 'route acl',
	'version' : '1.0.0'
};


internals.implementation = function ( request, reply ) {
	if ( _.isEmpty( request.route.settings.plugins[ internals.pluginName ] ) ) {
		return reply.continue();
	}

	const permissions = request.route.settings.plugins[ internals.pluginName ].permissions;

	if ( _.isEmpty( permissions ) ) {
		return reply.continue();
	}

	const callback = ( error, userPermissions ) => {
		const hasPermission = internals.checkPermissions( permissions, userPermissions );

		if ( !hasPermission ) {
			return reply( Boom.forbidden( 'You don\'nt have permission to access this resource.' ) );
		}

		return reply.continue();
	};

	return internals.permissionFunc( request.auth.credentials, callback );
};

internals.checkPermissions = function ( permissions, userPermissions ) {
	let perms = permissions;

	if ( _.isString( permissions ) ) {
		perms = [ permissions ];
	}

	const permissionMap = {};

	_.forEach( perms, permission => {
		Hoek.assert( _.isString( permission ), 'permission must be a string' );

		const parts = permission.split( ':' );

		Hoek.assert( parts.length === 2, 'permission must be formatted: [routeName]:[read|create|edit|delete]' );

		const routeName = parts[ 0 ];
		const crud      = parts[ 1 ];

		if ( _.isUndefined( userPermissions[ routeName ] ) || _.isUndefined( userPermissions[ routeName ][crud ] ) ) {
			permissionMap[ permission ] = false;
		} else {
			permissionMap[ permission ] = userPermissions[ routeName ][ crud ];
		}
	} );

	return _.reduce( permissionMap, ( result, permission ) => result && permission );
};
