'use strict';

const config    = use( 'config' );
const genetoken = require( './genetoken' );

module.exports = ( user ) => {
	const token      = genetoken( user.EmailAddress );
	const link       = `${config.app.url}/change-password?Token=${token}`;
	const sendOption = {
		'content' : {
			'from'    : config.app.email,
			'headers' : {
				'CC' : config.app.email
			},
			'subject' : 'Password Reset Confirmation',
			'html'    : `<!DOCTYPE html>
				<html lang="en-US">
					<head>
						<meta charset="utf-8">
					</head>
					<body>
						<h2>Password Reset Confirmation:</h2>
						<div>${user.FirstName} ${user.LastName}, you have requested a new password using this email ${user.EmailAddress}.</div>

						<br />

						<div>Click this <a href="${link}">link</a> to reset your password.</div>
					</body>
				</html>`
		},
		'recipients' : [
			{
				'address' : user.EmailAddress
			}
		]
	};

	return sendOption;
};
