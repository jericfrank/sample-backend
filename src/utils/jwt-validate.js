'use strict';

const _      = require( 'lodash' );
const jwt    = require('jsonwebtoken');
const moment = require( 'moment' );
const redis  = require( 'redis' );

const config    = use( 'config' );
const models    = use( 'models' );
const generoute = use( 'utils/generoute' );

const { User } = models;

const client = redis.createClient();

const verify = ( token, jwtKey ) => {
	return new Promise( ( resolve, reject ) => {
		jwt.verify( token, jwtKey, ( err ) => {
			if ( err ) {
				return reject( err );
			}

			return resolve( true );
		} );
	} );
};

const isAccountDisabled = ( id ) => {
	const options = {
		'attributes' : [ 'AccountDisabled' ]
	};
	return User.findById( id, options )
		.then( user => user.AccountDisabled );
};

const ismember = ( key, value ) => {
	return new Promise( ( resolve, reject ) => {
		client.sismember( key, value, ( err, replies ) => {
			if ( err ) {
				return reject( err );
			}

			return resolve( replies );
		} );
	} );
};

module.exports = function ( decoded, request, callback ) {
	const token = _.last( request.headers.authorization.split( ' ' ) );
	const date  = moment.unix( decoded.exp ).format( 'YYYY-MM-DD' );

	const init = generoute( function* () {
		const isUserVerified = yield verify( token, config.app.jwtKey );

		if ( !isUserVerified ) {
			return false;
		}

		const isMember = yield ismember( date, token );

		if ( isMember ) {
			return false;
		}

		const accountDisabled = yield isAccountDisabled( decoded.sub );

		if ( accountDisabled ) {
			return false;
		}

		return true;
	} );

	init()
	.then( res => callback( null, res ) )
	.catch( error => callback( error ) );
};
