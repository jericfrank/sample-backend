'use strict';

const log = require( '../logging' );

module.exports = function ( errorContext ) {
	log.fatal( errorContext );

	let localContext = errorContext;
	if ( errorContext.message.name === 'JsonWebTokenError' ) {
		localContext = {
			'errorType'  : 'badRequest',
			'message'    : 'Token is invalid',
			'scheme'     : 'Token',
			'attributes' : null
		};
	}

	if ( errorContext.message.name === 'TokenExpiredError' ) {
		localContext = {
			'errorType'  : 'unauthorized',
			'message'    : 'Token Expired',
			'scheme'     : 'Token',
			'attributes' : null
		};
	}

	return localContext;
};
