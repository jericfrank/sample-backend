'use strict';

const moment = require( 'moment' );

module.exports = ( dateRange ) => {
	const now      = moment.utc();
	const utcStart = moment.utc( dateRange.Start );
	const utcEnd   = moment.utc( dateRange.End );

	if ( now.isBetween( utcStart, utcEnd, 'second' ) ) {
		return 0;
	}

	const diff = moment.utc( utcStart.diff( now ) );
	return diff.valueOf();
};
