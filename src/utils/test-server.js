'use strict';

const Hapi   = require( 'hapi' );
const jwt2   = require( 'hapi-auth-jwt2' );
const hapiIO = require( 'hapi-io' );

module.exports = ( option ) => {
	const verifyFunc = ( decoded, request, callback ) => {
		return callback( null, true, {} );
	};

	const server = new Hapi.Server( {
		'debug' : false
	} );

	server.connection( {
		'port' : option.port
	} );

	server.register( jwt2, () => {
		server.auth.strategy( 'jwt', 'jwt', {
			'key'           : '',
			'verifyOptions' : { 'algorithms' : [ 'HS256' ] },

			verifyFunc
		} );

		server.register( [
			{ 'register' : hapiIO },
			{ 'register' : use( `gateways/${option.gateways}` ) }
		] );
	} );

	return server;
};
