/*jshint esversion: 6 */
'use strict';

const moment = require( 'moment' );
const jwt    = require( 'jwt-simple' );
const config = require( '../config' );

module.exports = userId => {
	const payload = {
		'sub' : userId,
		'iat' : moment().unix(),
		'exp' : moment().add( 10, 'days' ).unix()
	};

	return jwt.encode( payload, config.app.jwtKey );
};
