'use strict';

const _ = require( 'lodash' );

module.exports = ( recurrence ) => {
	const strWeekdays = _.result( recurrence, 'Weekdays', [] );

	if ( !strWeekdays ) {
		return strWeekdays;
	}

	return strWeekdays
		.join( ',' );
};
