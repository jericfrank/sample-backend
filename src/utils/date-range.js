'use strict';

const _      = require( 'lodash' );
const moment = require( 'moment' );

module.exports = ( options, action, cb ) => { // eslint-disable-line complexity
	const {
		arrDate,
		step,
		weekdays,
		day,
		monthWeekOption,
		monthWeekdayOption
	} = options;

	const dates = [];
	const sd    = moment.utc( arrDate[ 0 ], 'YYYY-MM-DD' );

	let ed;
	let m;
	let recurEvery;
	let current;

	if ( !step ) {
		throw Error( 'Must provide step.' );
	}

	switch ( action ) {
		case 'DAILY_WITH_REPEATEVERY_AND_END' :
			ed = moment.utc( arrDate[ 1 ], 'YYYY-MM-DD' );

			// If you want an exclusive end date (half-open interval)
			for ( const m = sd.clone(); m.diff( ed, 'days' ) <= 0; m.add( step, 'days' ) ) {
				dates.push( cb( m ) );
			}
		break;

		case 'DAILY_WITH_REPEATEVERY_AND_ENDAFTER' :
			m  = sd.clone();
			ed = arrDate[ 1 ];

			while ( ed ) {
				dates.push( cb( m ) );
				m.add( step, 'days' );
				ed = ed - 1;
			}
		break;

		case 'DAILY_WITH_WEEKDAY_AND_END' :
			ed = moment.utc( arrDate[ 1 ], 'YYYY-MM-DD' );

			// If you want an exclusive end date (half-open interval)
			for ( const m = sd.clone(); m.diff( ed, 'days' ) <= 0; m.add( step, 'days' ) ) {
				const day = m.day();
				if ( day !== 6 && day !== 0 ) {
					dates.push( cb( m ) );
				}
			}
		break;

		case 'DAILY_WITH_WEEKDAY_AND_ENDAFTER' :
			m  = sd.clone();
			ed = arrDate[ 1 ];

			while ( ed ) {
				const day = m.day();
				if ( day !== 6 && day !== 0 ) {
					dates.push( cb( m ) );
					ed = ed - 1;
				}

				m.add( 1, 'days' );
			}
		break;

		case 'WEEKLY_WITH_RECUR_AND_END' :
			recurEvery = step * 7;
			ed         = moment.utc( arrDate[ 1 ], 'YYYY-MM-DD' );

			current       = sd.clone();
			let dayOfWeek = 0;
			while ( current.day( dayOfWeek ).isBefore( ed ) ) {
				const start = current.clone();

				let end = start.clone().endOf( 'week' );
				if ( !end.isBefore( ed ) ) {
					end = ed;
				}

				for ( const m = start.clone(); m.diff( end, 'days' ) <= 0; m.add( 1, 'days' ) ) {
					const day = m.day();

					if ( !m.isBefore( sd ) ) {
						if ( _.includes( weekdays, day ) ) {
							dates.push( cb( m ) );
						}
					}
				}

				dayOfWeek = recurEvery;
			}
		break;

		case 'WEEKLY_WITH_RECUR_AND_ENDAFTER' :
			recurEvery = step * 7;
			ed         = arrDate[ 1 ];

			current = sd.clone();
			while ( ed ) {
				const start = current.clone();
				const end   = start.clone().endOf( 'week' );

				for ( const m = start.clone(); m.diff( end, 'seconds' ) < 0; m.add( 1, 'days' ) ) {
					const day = m.day();
					if ( _.includes( weekdays, day ) && ed !== 0 ) {
						dates.push( cb( m ) );
						ed = ed - 1;
					}
				}

				current = start.day( recurEvery );
			}
		break;

		case 'MONTHLY_RECUR_ONE_DAY_A_MONTH_WITH_END' :
			current = sd.clone();
			ed      = moment.utc( arrDate[ 1 ], 'YYYY-MM-DD' );

			while ( current.isBefore( ed ) ) {
				const start = current.clone().date( day );

				if ( start.isBefore( sd ) ) {
					current.add( step, 'months' );
					continue;
				}

				dates.push( cb( start ) );
				current.add( step, 'months' );
			}
		break;

		case 'MONTHLY_RECUR_ONE_DAY_A_MONTH_WITH_ENDAFTER' :
			current = sd.clone();
			ed      = arrDate[ 1 ];

			while ( ed ) {
				const start = current.clone().date( day );

				if ( start.isBefore( sd ) ) {
					current.add( step, 'months' );
					continue;
				}

				dates.push( cb( start ) );
				current.add( step, 'months' );
				ed = ed - 1;
			}
		break;

		case 'MONTHLY_RECUR_ONE_DAY_ON_EVERY_N_MONTH_WITH_END' :
			current    = sd.clone();
			ed         = moment.utc( arrDate[ 1 ], 'YYYY-MM-DD' );
			recurEvery = step;

			while ( current.isBefore( ed ) ) {
				const start = current.clone().startOf( 'month' );
				const end   = current.clone().endOf( 'month' );

				const occurences = [];
				const m          = start.clone();
				while ( m.isBefore( end ) ) {
					if ( m.isBefore( sd ) ) {
						m.add( 1, 'days' );
						continue;
					}

					if ( m.day() === monthWeekdayOption ) {
						occurences.push( m.clone() );
					}

					m.add( 1, 'days' );
				}

				if ( occurences.length ) {
					if ( monthWeekOption === 5 ) {
						dates.push( cb( _.last( occurences ) ) );
					} else {
						dates.push( cb( occurences[ monthWeekOption - 1 ] ) );
					}
				}

				current.add( recurEvery, 'months' );
			}
		break;

		case 'MONTHLY_RECUR_ONE_DAY_ON_EVERY_N_MONTH_WITH_ENDAFTER' :
			current    = sd.clone();
			ed         = arrDate[ 1 ];
			recurEvery = step;

			while ( ed ) {
				const start = current.clone().startOf( 'month' );
				const end   = current.clone().endOf( 'month' );

				const occurences = [];
				const m          = start.clone();
				while ( m.isBefore( end ) ) {
					if ( m.isBefore( sd ) ) {
						m.add( 1, 'days' );
						continue;
					}

					if ( m.day() === monthWeekdayOption ) {
						occurences.push( m.clone() );
					}

					m.add( 1, 'days' );
				}

				if ( occurences.length ) {
					if ( monthWeekOption === 5 ) {
						dates.push( cb( _.last( occurences ) ) );
					} else {
						dates.push( cb( occurences[ monthWeekOption - 1 ] ) );
					}

					ed = ed - 1;
				}

				current.add( recurEvery, 'months' );
			}
		break;
	}
	return _.uniq( dates );
};

