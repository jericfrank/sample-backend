'use strict';

module.exports = function ( sequelize, DataTypes ) {
	const EventTemplate = sequelize.define( 'EventTemplate', {
		'Id' : {
			'type'          : DataTypes.INTEGER,
			'primaryKey'    : true,
			'field'         : 'id',
			'autoIncrement' : true
		},

		'StationId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'e_s_id'
		},

		'EventTypeId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'e_et_id'
		},

		'Title' : {
			'type'  : DataTypes.STRING,
			'field' : 'e_title'
		},

		'TeamOne' : {
			'type'  : DataTypes.STRING,
			'field' : 'e_team1'
		},

		'TeamOneAbbrv' : {
			'type'  : DataTypes.STRING,
			'field' : 'e_team1_abbrv'
		},

		'TeamOneScore' : {
			'type'  : DataTypes.DOUBLE,
			'field' : 'e_score1'
		},

		'TeamTwo' : {
			'type'  : DataTypes.STRING,
			'field' : 'e_team2'
		},

		'TeamTwoAbbrv' : {
			'type'  : DataTypes.STRING,
			'field' : 'e_team2_abbrv'
		},

		'TeamTwoScore' : {
			'type'  : DataTypes.DOUBLE,
			'field' : 'e_score2'
		},

		'PartNo' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'e_part_no'
		},

		'GamePartId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'e_gamepart_id'
		},

		'CreatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'created_by',
			'defaultValue' : 0
		},

		'UpdatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'updated_by',
			'defaultValue' : 0
		}
	}, {

		// define the table's name
		'tableName' : 'event_templates',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,
		'timestamps'      : true,

		'createdAt' : 'created_at',
		'updatedAt' : 'updated_at',
		'deletedAt' : 'deleted_at',

		// don't delete database entries
		// but set the newly added attribute deletedAt
		'paranoid' : true,

		'classMethods' : {
			associate ( models ) {
				EventTemplate.hasOne( models.EventSettingTemplate, {
					'foreignKey' : 'EventId',
					'as'         : 'EventSetting'
				} );

				EventTemplate.hasMany( models.EventDetailTemplate, {
					'foreignKey' : 'EventId',
					'as'         : 'EventDetails'
				} );

				EventTemplate.belongsTo( models.EventGamePart, {
					'foreignKey' : 'GamePartId',
					'as'         : 'GamePart'
				} );
			},

			joinSetting ( models ) {
				return {
					'model'      : models.EventSettingTemplate,
					'as'         : 'EventSetting',
					'attributes' : {
						'exclude' : [ 'deleted_at', 'created_at', 'updated_at', 'CreatedBy', 'UpdatedBy' ]
					}
				};
			},

			joinDetails ( models ) {
				return {
					'model'      : models.EventDetailTemplate,
					'as'         : 'EventDetails',
					'attributes' : {
						'exclude' : [ 'deleted_at', 'created_at', 'updated_at', 'CreatedBy', 'UpdatedBy' ]
					},
					'limit'  : 10,
					'offset' : 0
				};
			},

			joinGamePart ( models ) {
				return {
					'model' : models.EventGamePart,
					'as'    : 'GamePart'
				};
			},

			findByIdWithDetails ( id, models ) {
				return EventTemplate.findById( id, {
					'attributes' : {
						'exclude' : [ 'deleted_at', 'created_at', 'updated_at', 'CreatedBy', 'UpdatedBy' ]
					},
					'include' : [
						models.EventTemplate.joinSetting( models ),
						models.EventTemplate.joinDetails( models )
					]
				} );
			}
		}
	} );

	return EventTemplate;
};
