'use strict';

const bcrypt = require( 'bcrypt' );

module.exports = function ( sequelize, DataTypes ) {
	const User = sequelize.define( 'User', {
		'Id' : {
			'type'          : DataTypes.INTEGER,
			'primaryKey'    : true,
			'field'         : 'id',
			'autoIncrement' : true
		},

		'UserName' : {
			'type'  : DataTypes.STRING,
			'field' : 'u_name'
		},

		'FirstName' : {
			'type'  : DataTypes.STRING,
			'field' : 'u_first_name'
		},

		'MiddleName' : {
			'type'  : DataTypes.STRING,
			'field' : 'u_middle_name'
		},

		'LastName' : {
			'type'  : DataTypes.STRING,
			'field' : 'u_last_name'
		},

		'EmailAddress' : {
			'type'  : DataTypes.STRING,
			'field' : 'u_email_address'
		},

		'Password' : {
			'type'  : DataTypes.STRING,
			'field' : 'u_password'
		},

		'ProfileId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'u_p_id'
		},

		'StationId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'u_default_s_id'
		},

		'AccountDisabled' : {
			'type'         : DataTypes.BOOLEAN,
			'field'        : 'u_account_disabled',
			'defaultValue' : false
		},

		'CreatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'created_by',
			'defaultValue' : 0
		},

		'UpdatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'updated_by',
			'defaultValue' : 0
		}
	}, {

		// define the table's name
		'tableName' : 'users',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,
		'timestamps'      : true,

		'createdAt' : 'created_at',
		'updatedAt' : 'updated_at',
		'deletedAt' : 'deleted_at',

		// don't delete database entries
		// but set the newly added attribute deletedAt
		'paranoid' : true,

		'classMethods' : {
			associate ( models ) {
				User.belongsTo( models.Profile, {
					'foreignKey' : 'ProfileId',
					'as'         : 'Profile'
				} );

				User.belongsTo( models.Station, {
					'foreignKey' : 'StationId',
					'as'         : 'Station'
				} );

				User.belongsToMany( models.Station, {
					'through'    : models.UserStation,
					'foreignKey' : 'UserId',
					'otherKey'   : 'StationId',
					'as'         : 'Stations'
				} );
			},

			joinStations ( models ) {
				return {
					'model' : models.Station,
					'as'    : 'Stations'
				};
			}
		}
	} );

	const hashPassword = user => {
		try {
			const salt = bcrypt.genSaltSync( 10 );
			const hash = bcrypt.hashSync( user.Password, salt );

			user.Password = hash;
			return Promise.resolve( user );
		} catch ( error ) {
			return Promise.reject( error );
		}
	};

	User.beforeCreate( hashPassword );
	User.beforeUpdate( hashPassword );

	return User;
};
