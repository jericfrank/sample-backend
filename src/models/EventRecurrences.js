'use strict';

module.exports = function ( sequelize, DataTypes ) {
	const Model = sequelize.define( 'EventRecurrence', {
		'Id' : {
			'type'          : DataTypes.INTEGER,
			'primaryKey'    : true,
			'field'         : 'id',
			'autoIncrement' : true
		},

		'EventId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'er_e_id'
		},

		'RepeatOption' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'er_repeatoption_id'
		},

		'DayOption' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'er_day_option'
		},

		'RepeatEvery' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'er_repeat_every'
		},

		'Weekdays' : {
			'type'  : DataTypes.STRING,
			'field' : 'er_repeat_dayofweek'
		},

		'Day' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'er_day'
		},

		'MonthOption' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'er_month_option'
		},

		'MonthWeekOption' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'er_monthweek_option'
		},

		'MonthWeekdaysOption' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'er_monthweekdays_option'
		},

		'Start' : {
			'type'  : DataTypes.DATE,
			'field' : 'er_start_date'
		},

		'End' : {
			'type'  : DataTypes.DATE,
			'field' : 'er_end_date'
		},

		'EndAfter' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'er_end_after'
		}
	}, {

		// define the table's name
		'tableName' : 'event_recurrences',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,
		'timestamps'      : true,

		'createdAt' : 'created_at',
		'updatedAt' : 'updated_at',
		'deletedAt' : 'deleted_at',

		// don't delete database entries
		// but set the newly added attribute deletedAt
		'paranoid' : true
	} );

	return Model;
};
