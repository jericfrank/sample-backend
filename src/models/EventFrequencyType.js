'use strict';

module.exports = function ( sequelize, DataTypes ) {
	const EventFrequencyType = sequelize.define( 'EventFrequencyType', {
		'Id' : {
			'type'          : DataTypes.INTEGER,
			'primaryKey'    : true,
			'field'         : 'id',
			'autoIncrement' : true
		},

		'Name' : {
			'type'  : DataTypes.STRING,
			'field' : 'eft_name'
		},

		'UnitOfMeasurementId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'eft_uom_id'
		},

		'CreatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'created_by',
			'defaultValue' : 0
		},

		'UpdatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'updated_by',
			'defaultValue' : 0
		}
	}, {

		// define the table's name
		'tableName' : 'event_frequency_types',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,
		'timestamps'      : true,

		'createdAt' : 'created_at',
		'updatedAt' : 'updated_at',
		'deletedAt' : 'deleted_at',

		// don't delete database entries
		// but set the newly added attribute deletedAt
		'paranoid' : true,

		'classMethods' : {
			associate ( models ) {
				EventFrequencyType.belongsTo( models.UnitOfMeasurement, {
					'foreignKey' : 'UnitOfMeasurementId',
					'as'         : 'UnitOfMeasurement'
				} );
			},

			joinMeasurement ( models ) {
				return {
					'model'      : models.UnitOfMeasurement,
					'as'         : 'UnitOfMeasurement',
					'attributes' : {
						'exclude' : [ 'deleted_at' ]
					}
				};
			}
		}
	} );

	return EventFrequencyType;
};
