'use strict';

module.exports = function ( sequelize, DataTypes ) {
	const model = sequelize.define( 'EventGamePart', {
		'Id' : {
			'type'          : DataTypes.INTEGER,
			'primaryKey'    : true,
			'field'         : 'id',
			'autoIncrement' : true
		},

		'Name' : {
			'type'  : DataTypes.STRING,
			'field' : 'part_name'
		},

		'Description' : {
			'type'  : DataTypes.STRING,
			'field' : 'part_description'
		}
	}, {

		// define the table's name
		'tableName' : 'event_game_parts',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,
		'timestamps'      : false,

		// don't delete database entries
		// but set the newly added attribute deletedAt
		'paranoid' : false
	} );

	return model;
};
