'use strict';

module.exports = function ( sequelize, DataTypes ) {
	const Module = sequelize.define( 'Module', {
		'Id' : {
			'type'          : DataTypes.INTEGER,
			'primaryKey'    : true,
			'field'         : 'id',
			'autoIncrement' : true
		},

		'Name' : {
			'type'  : DataTypes.STRING,
			'field' : 'm_name'
		},

		'CategoryId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'm_mc_id'
		},

		'RouteName' : {
			'type'  : DataTypes.STRING,
			'field' : 'm_route_name'
		},

		'CreatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'created_by',
			'defaultValue' : 0
		},

		'UpdatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'updated_by',
			'defaultValue' : 0
		}
	}, {

		// define the table's name
		'tableName' : 'modules',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,
		'timestamps'      : true,

		'createdAt' : 'created_at',
		'updatedAt' : 'updated_at',
		'deletedAt' : 'deleted_at',

		// don't delete database entries
		// but set the newly added attribute deletedAt
		'paranoid' : true,

		'classMethods' : {
			associate ( models ) {
				Module.belongsToMany( models.Profile, {
					'through'    : models.ProfileModule,
					'foreignKey' : 'ModuleId',
					'otherKey'   : 'ProfileId'
				} );
			}
		}
	} );

	return Module;
};
