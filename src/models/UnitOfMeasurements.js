'use strict';

module.exports = function ( sequelize, DataTypes ) {
	const Model = sequelize.define( 'UnitOfMeasurement', {
		'Id' : {
			'type'          : DataTypes.INTEGER,
			'primaryKey'    : true,
			'field'         : 'id',
			'autoIncrement' : true
		},

		'Name' : {
			'type'  : DataTypes.STRING,
			'field' : 'uom_name'
		}
	}, {

		// define the table's name
		'tableName' : 'unit_of_measurements',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,

		'timestamps' : false
	} );

	return Model;
};
