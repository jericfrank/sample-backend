'use strict';

module.exports = function ( sequelize, DataTypes ) {
	const Event = sequelize.define( 'Event', {
		'Id' : {
			'type'          : DataTypes.INTEGER,
			'primaryKey'    : true,
			'field'         : 'id',
			'autoIncrement' : true
		},

		'StationId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'e_s_id'
		},

		'EventTypeId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'e_et_id'
		},

		'Title' : {
			'type'  : DataTypes.STRING,
			'field' : 'e_title'
		},

		'Start' : {
			'type'         : DataTypes.DATE,
			'defaultValue' : sequelize.NOW,
			'field'        : 'e_start'
		},

		'End' : {
			'type'         : DataTypes.DATE,
			'defaultValue' : sequelize.NOW,
			'field'        : 'e_end'
		},

		'JobId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'e_job_id'
		},

		'TeamOne' : {
			'type'  : DataTypes.STRING,
			'field' : 'e_team1'
		},

		'TeamOneAbbrv' : {
			'type'  : DataTypes.STRING,
			'field' : 'e_team1_abbrv'
		},

		'TeamOneScore' : {
			'type'  : DataTypes.DOUBLE,
			'field' : 'e_score1'
		},

		'TeamTwo' : {
			'type'  : DataTypes.STRING,
			'field' : 'e_team2'
		},

		'TeamTwoAbbrv' : {
			'type'  : DataTypes.STRING,
			'field' : 'e_team2_abbrv'
		},

		'TeamTwoScore' : {
			'type'  : DataTypes.DOUBLE,
			'field' : 'e_score2'
		},

		'PartNo' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'e_part_no'
		},

		'GamePartId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'e_gamepart_id'
		},

		'CreatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'created_by',
			'defaultValue' : 0
		},

		'UpdatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'updated_by',
			'defaultValue' : 0
		},

		'ParentId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'e_parent_id'
		}
	}, {

		// define the table's name
		'tableName' : 'events',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,
		'timestamps'      : true,

		'createdAt' : 'created_at',
		'updatedAt' : 'updated_at',
		'deletedAt' : 'deleted_at',

		// don't delete database entries
		// but set the newly added attribute deletedAt
		'paranoid' : true,

		'classMethods' : {
			associate ( models ) {
				Event.hasOne( models.EventSetting, {
					'foreignKey' : 'EventId',
					'as'         : 'EventSetting'
				} );

				Event.hasMany( models.EventDetail, {
					'foreignKey' : 'EventId',
					'as'         : 'EventDetails'
				} );

				Event.belongsTo( models.EventType, {
					'foreignKey' : 'EventTypeId',
					'as'         : 'EventType'
				} );

				Event.belongsTo( models.EventGamePart, {
					'foreignKey' : 'GamePartId',
					'as'         : 'GamePart'
				} );

				Event.hasOne( models.EventRecurrence, {
					'foreignKey' : 'EventId',
					'as'         : 'EventRecurrence'
				} );
			},

			joinSetting ( models ) {
				return {
					'model'      : models.EventSetting,
					'as'         : 'EventSetting',
					'attributes' : {
						'exclude' : [ 'deleted_at' ]
					},
					'include' : [ models.EventSetting.joinFrequency( models ) ]
				};
			},

			joinDetails ( models ) {
				return {
					'model'      : models.EventDetail,
					'as'         : 'EventDetails',
					'attributes' : {
						'exclude' : [ 'deleted_at' ]
					},
					'limit'  : 10,
					'offset' : 0
				};
			},

			joinType ( models ) {
				return {
					'model'      : models.EventType,
					'as'         : 'EventType',
					'attributes' : {
						'exclude' : [ 'deleted_at' ]
					}
				};
			},

			joinGamePart ( models ) {
				return {
					'model' : models.EventGamePart,
					'as'    : 'GamePart'
				};
			},

			joinRecurrence ( models ) {
				return {
					'model'      : models.EventRecurrence,
					'as'         : 'EventRecurrence',
					'attributes' : {
						'exclude' : [ 'deleted_at', 'created_at', 'updated_at' ]
					}
				};
			},

			// returns date that could possibly overlap by
			// the date range we pass.
			// NOTE: Sequelize implicitly convert local time to utc
			findUnderlap ( option ) {
				const { Start, End, StationId, Id, ParentId } = option;

				const options = {
					'attributes' : [ 'id', 'Title', 'Start', 'End' ],
					'where'      : {
						StationId,
						'$or' : [
							{ '$and' : { 'Start' : { '$lt' : Start }, 'End' : { '$gt' : Start } } },
							{ '$and' : { 'Start' : { '$lt' : End }, 'End' : { '$gt' : End } } }
						]
					}
				};

				if ( Id ) {
					options.where.id = {
						'$ne' : Id
					};
				}

				if ( ParentId ) {
					options.where.ParentId = {
						'$ne' : ParentId
					};
				}

				return Event.findAll( options );
			},

			findInBetween ( option ) {
				const { Start, End, StationId, Id, ParentId } = option;

				const options = {
					'attributes' : [ 'Id', 'Title', 'Start', 'End' ],
					'where'      : {
						StationId,
						'Start' : {
							'$gte' : Start
						},

						'End' : {
							'$lte' : End
						}
					}
				};

				if ( Id ) {
					options.where.Id = {
						'$ne' : Id
					};
				}

				if ( ParentId ) {
					options.where.ParentId = {
						'$ne' : ParentId
					};
				}

				return Event.findAll( options );
			}
		}
	} );

	return Event;
};
