'use strict';

module.exports = function ( sequelize, DataTypes ) {
	const CompanyProfile = sequelize.define( 'CompanyProfile', {
		'Id' : {
			'type'          : DataTypes.INTEGER,
			'primaryKey'    : true,
			'field'         : 'id',
			'autoIncrement' : true
		},

		'Name' : {
			'type'  : DataTypes.STRING,
			'field' : 'company_name'
		},

		'Address' : {
			'type'  : DataTypes.STRING,
			'field' : 'company_address'
		},

		'ContactPerson' : {
			'type'  : DataTypes.STRING,
			'field' : 'contact_person'
		},

		'ContactNumber' : {
			'type'  : DataTypes.STRING,
			'field' : 'contact_numbers'
		},

		'PricingType' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'pricing_type'
		},

		'MaxLimit' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'max_limit'
		},

		'CompanyLogo' : {
			'type'  : DataTypes.STRING,
			'field' : 'company_logo'
		}
	}, {

		// define the table's name
		'tableName' : 'company_profile',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,
		'timestamps'      : true,

		'createdAt' : 'created_at',
		'updatedAt' : 'updated_at',
		'deletedAt' : 'deleted_at',

		// don't delete database entries
		// but set the newly added attribute deletedAt
		'paranoid' : true
	} );


	return CompanyProfile;
};
