'use strict';

module.exports = function ( sequelize, DataTypes ) {
	const EventSetting = sequelize.define( 'EventSetting', {
		'Id' : {
			'type'          : DataTypes.INTEGER,
			'primaryKey'    : true,
			'field'         : 'id',
			'autoIncrement' : true
		},

		'EventId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'es_e_id'
		},

		'FrequencyTypeId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'es_eft_id'
		},

		'FrequencyTypeValue' : {
			'type'  : DataTypes.DOUBLE,
			'field' : 'es_eft_value'
		},

		'Color' : {
			'type'  : DataTypes.STRING,
			'field' : 'es_color'
		},

		'NoLineTextToExport' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'es_nolineoftxttoexport'
		},

		'CreatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'created_by',
			'defaultValue' : 0
		},

		'UpdatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'updated_by',
			'defaultValue' : 0
		}
	}, {

		// define the table's name
		'tableName' : 'event_settings',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,
		'timestamps'      : true,

		'createdAt' : 'created_at',
		'updatedAt' : 'updated_at',
		'deletedAt' : 'deleted_at',

		// don't delete database entries
		// but set the newly added attribute deletedAt
		'paranoid' : true,

		'classMethods' : {
			associate ( models ) {
				EventSetting.belongsTo( models.EventFrequencyType, {
					'foriegnKey' : 'FrequencyTypeId',
					'as'         : 'FrequencyType'
				} );
			},

			joinFrequency ( models ) {
				return {
					'model'      : models.EventFrequencyType,
					'as'         : 'FrequencyType',
					'attributes' : {
						'exclude' : [ 'deleted_at' ]
					},
					'include' : [ models.EventFrequencyType.joinMeasurement( models ) ]
				};
			}
		}
	} );

	return EventSetting;
};
