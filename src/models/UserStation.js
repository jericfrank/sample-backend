'use strict';

module.exports = function ( sequelize, DataTypes ) {
	const UserStation = sequelize.define( 'UserStation', {
		'Id' : {
			'type'          : DataTypes.INTEGER,
			'primaryKey'    : true,
			'field'         : 'id',
			'autoIncrement' : true
		},

		'StationId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'us_s_id'
		},

		'UserId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'us_u_id'
		},

		'Default' : {
			'type'  : DataTypes.BOOLEAN,
			'field' : 'us_default'
		}
	}, {

		// define the table's name
		'tableName' : 'user_stations',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,
		'timestamps'      : false
	} );

	return UserStation;
};
