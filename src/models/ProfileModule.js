'use strict';

module.exports = function ( sequelize, DataTypes ) {
	const ProfileModule = sequelize.define( 'ProfileModule', {
		'ModuleId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'pm_m_id'
		},

		'ProfileId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'pm_p_id'
		},

		'CanRead' : {
			'type'         : DataTypes.BOOLEAN,
			'field'        : 'pm_can_read',
			'defaultValue' : false
		},

		'CanCreate' : {
			'type'         : DataTypes.BOOLEAN,
			'field'        : 'pm_can_create',
			'defaultValue' : false
		},

		'CanUpdate' : {
			'type'         : DataTypes.BOOLEAN,
			'field'        : 'pm_can_update',
			'defaultValue' : false
		},

		'CanDelete' : {
			'type'         : DataTypes.BOOLEAN,
			'field'        : 'pm_can_delete',
			'defaultValue' : false
		}
	}, {

		// define the table's name
		'tableName' : 'profile_modules',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,
		'timestamps'      : true,

		'createdAt' : 'created_at',
		'updatedAt' : 'updated_at',
		'deletedAt' : 'deleted_at',

		// don't delete database entries
		// but set the newly added attribute deletedAt
		'paranoid' : true,

		'classMethods' : {
			associate ( models ) {
				ProfileModule.belongsTo( models.Module, {
					'foreignKey' : 'ModuleId',
					'as'         : 'Module'
				} );
			}
		}
	}  );

	return ProfileModule;
};
