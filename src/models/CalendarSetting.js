
'use strict';

module.exports = function ( sequelize, DataTypes ) {
	const CalendarSetting = sequelize.define( 'CalendarSetting', {
		'Id' : {
			'type'          : DataTypes.INTEGER,
			'primaryKey'    : true,
			'field'         : 'id',
			'autoIncrement' : true
		},

		'StationId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'scs_s_id'
		},

		'StorageLocation' : {
			'type'  : DataTypes.STRING,
			'field' : 'scs_storage_location'
		},

		'StartOfWeek' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'scs_startofweek'
		},

		'CreatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'created_by',
			'defaultValue' : 0
		},

		'UpdatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'updated_by',
			'defaultValue' : 0
		}
	}, {

		// define the table's name
		'tableName' : 'station_calendar_settings',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,
		'timestamps'      : true,

		'createdAt' : 'created_at',
		'updatedAt' : 'updated_at',
		'deletedAt' : 'deleted_at',

		// don't delete database entries
		// but set the newly added attribute deletedAt
		'paranoid' : true,

		'classMethods' : {
			associate ( models ) {
				CalendarSetting.belongsTo( models.Station, {
					'foreignKey' : 'StationId',
					'as'         : 'CalendarSetting'
				} );
			}
		}
	} );

	return CalendarSetting;
};
