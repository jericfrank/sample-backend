'use strict';

module.exports = function ( sequelize, DataTypes ) {
	const Model = sequelize.define( 'EventQueue', {
		'Id' : {
			'type'          : DataTypes.INTEGER,
			'primaryKey'    : true,
			'field'         : 'id',
			'autoIncrement' : true
		},

		'JobId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'job_id'
		},

		'EventId' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'event_id'
		},

		'Duration' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'duration'
		},

		'Index' : {
			'type'  : DataTypes.INTEGER,
			'field' : 'e_index'
		}
	}, {
		// define the table's name
		'tableName' : 'event_queues',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,
		'timestamps'      : false,

		'classMethods' : {
			findOverridingJob ( eventId, delay ) {
				return this.findAll( {
					'where' : {
						'EventId'  : eventId,
						'Duration' : {
							'$gte' : delay
						}
					}
				} );
			}
		}
	} );

	return Model;
};
