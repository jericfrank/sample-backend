'use strict';

module.exports = function ( sequelize, DataTypes ) {
	const Station = sequelize.define( 'Station', {
		'Id' : {
			'type'          : DataTypes.INTEGER,
			'primaryKey'    : true,
			'field'         : 'id',
			'autoIncrement' : true
		},

		'Name' : {
			'type'  : DataTypes.STRING,
			'field' : 's_name'
		},

		'Location' : {
			'type'  : DataTypes.INTEGER,
			'field' : 's_location'
		},

		'Logo' : {
			'type'  : DataTypes.STRING,
			'field' : 's_logo'
		},

		'IsActive' : {
			'type'  : DataTypes.BOOLEAN,
			'field' : 's_isActive'
		},

		'CreatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'created_by',
			'defaultValue' : 0
		},

		'UpdatedBy' : {
			'type'         : DataTypes.INTEGER,
			'field'        : 'updated_by',
			'defaultValue' : 0
		}
	}, {

		// define the table's name
		'tableName' : 'stations',

		// Disable the modification for table names
		// By default, sequelize will automatically
		// transform all passed model names into plural
		'freezeTableName' : true,
		'timestamps'      : true,

		'createdAt' : 'created_at',
		'updatedAt' : 'updated_at',
		'deletedAt' : 'deleted_at',

		// don't delete database entries
		// but set the newly added attribute deletedAt
		'paranoid' : true,

		'classMethods' : {
			associate ( models ) {
				Station.hasMany( models.User, {
					'foreignKey' : 'StationId',
					'as'         : 'Users'
				} );

				Station.belongsToMany( models.User, {
					'through'    : models.UserStation,
					'foreignKey' : 'StationId',
					'otherKey'   : 'UserId'
				} );

				Station.hasOne( models.CalendarSetting, {
					'foreignKey' : 'StationId',
					'as'         : 'CalendarSetting'
				} );
			},

			joinSetting ( models ) {
				return {
					'model'      : models.CalendarSetting,
					'as'         : 'CalendarSetting',
					'attributes' : {
						'exclude' : [ 'deleted_at' ]
					}
				};
			}
		}
	} );

	return Station;
};
