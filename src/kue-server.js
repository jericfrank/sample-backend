'use strict';

const config = require( './config' );
const kue    = require( 'kue' );
const log    = require( './logging' );

const queue = kue.createQueue();

queue.on( 'error', err => log.fatal( err ) );

kue.app.listen( config.kue.port );
log.info( `kue-server is up and running on port ${config.kue.port}` );
