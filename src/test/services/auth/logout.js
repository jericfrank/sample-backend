'use strict';

require( 'dotenv' ).config();

const Code      = require( 'code' );
const Lab       = require( 'lab' );
const fakeredis = require( 'fakeredis' );
const jwt       = require( 'jsonwebtoken' );
const moment    = require( 'moment' );
const redis     = require( 'redis' );
const sinon     = require( 'sinon' );

const config = use( 'config' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let authService;
let data;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	data = {
		'authorization' : jwt.sign( payload, config.app.jwtKey )
	};

	authService = use( 'services/auth' );

	sinon.stub( redis, 'createClient', fakeredis.createClient );

	done();
} );

after( done => {
	redis.createClient.restore();
	done();
} );

describe( 'AuthService::logout', () => {
	it ( 'should be able to invalidate token', done => {
		authService.logout( data )
			.then( response => {
				expect( response ).to.be.an.object();
				done();
			} );
	} );
} );
