'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const bcrypt = require( 'bcrypt' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const User = models.User;

let authService;
let privilegeService;
let data;
before( done => {
	data = {
		'UserName' : 'danswater',
		'Password' : 'secret'
	};

	authService      = use( 'services/auth' );
	privilegeService = use( 'services/privileges' );

	done();
} );

describe( 'AuthService::authenticate', () => {
	it( 'should be able return null when user is empty', done => {
		sinon.stub( User, 'findOne', () => {
			return Promise.resolve( null );
		} );

		authService.authenticate( data )
			.then( response => {
				expect( response ).to.be.equal( null );

				User.findOne.restore();
				done();
			} );
	} );

	it( 'should be able to handle bcrypt error when comparing password', done => {
		sinon.stub( User, 'findOne', () => {
			return Promise.resolve( {
				'UserName'        : 'danswater',
				'FistName'        : 'Francis',
				'MiddleName'      : 'Viscayno',
				'LastName'        : 'Lamayo',
				'EmailAddress'    : 'dansisabot@gmail.com',
				'Password'        : '$2a$04$VDsMg1wtIa2Pe1Q/M2334.NNXt/ToH0UGvAkqC8TSvmXnnmU.yySO',
				'ProfileId'       : 1,
				'StationId'       : 1,
				'AccountDisabled' : false
			} );
		} );

		sinon.stub( bcrypt, 'compare', ( rPass, uPass, callback ) => {
			callback( new Error( 'comparing error' ) );
		} );

		authService.authenticate( data )
			.then( response => {
				expect( response ).to.be.an.error( 'comparing error' );

				User.findOne.restore();
				bcrypt.compare.restore();
				done();
			} );
	} );

	it( 'should be able to return false when password does not match', done => {
		sinon.stub( User, 'findOne', () => {
			return Promise.resolve( {
				'UserName'        : 'danswater',
				'FistName'        : 'Francis',
				'MiddleName'      : 'Viscayno',
				'LastName'        : 'Lamayo',
				'EmailAddress'    : 'dansisabot@gmail.com',
				'Password'        : '$2a$04$VDsMg1wtIa2Pe1Q/M2334.NNXt/ToH0UGvAkqC8TSvmXnnmU.yySOs',
				'ProfileId'       : 1,
				'StationId'       : 1,
				'AccountDisabled' : false
			} );
		} );

		authService.authenticate( data )
			.then( response => {
				expect( response ).to.be.equal( false );

				User.findOne.restore();
				done();
			} );
	} );

	it( 'should be able to return auth object when successful', done => {
		const response = {
			'UserName'        : 'danswater',
			'FistName'        : 'Francis',
			'MiddleName'      : 'Viscayno',
			'LastName'        : 'Lamayo',
			'EmailAddress'    : 'dansisabot@gmail.com',
			'Password'        : '$2a$04$VDsMg1wtIa2Pe1Q/M2334.NNXt/ToH0UGvAkqC8TSvmXnnmU.yySO',
			'ProfileId'       : 1,
			'StationId'       : 1,
			'AccountDisabled' : false,

			toJSON () {
				return response;
			},

			getStations () {
				return Promise.resolve( [ {
					'Id'         : 1,
					'Name'       : 'MWFB',
					'Location'   : 'Wisconsin',
					'Logo'       : null,
					'IsActive'   : true,
					'CreatedBy'  : 1,
					'UpdatedBy'  : 1,
					'created_at' : '2017-02-06T06:04:00.000Z',
					'updated_at' : '2017-03-02T06:48:27.000Z',
					'Default'    : false,

					toJSON () {
						return response.getStations();
					}
				} ] );
			}
		};

		sinon.stub( User, 'findOne', () => {
			return Promise.resolve( response );
		} );

		sinon.stub( privilegeService, 'getAll', () => Promise.resolve( [ {
			'ModuleId'  : 1,
			'ProfileId' : 1,
			'CanRead'   : true,
			'CanCreate' : true,
			'CanUpdate' : true,
			'CanDelete' : true,
			'Module'    : {
				'Id'   : 1,
				'Name' : 'User Management'
			}
		} ] ) );

		authService.authenticate( data )
			.then( response => {
				expect( response ).to.be.an.object();
				expect( response.Token ).to.be.a.string();
				expect( response.User ).to.be.an.object();

				User.findOne.restore();
				privilegeService.getAll.restore();
				done();
			} );
	} );
} );
