'use strict';

require( 'dotenv' ).config();

const Code      = require( 'code' );
const Lab       = require( 'lab' );
const jwt       = require( 'jsonwebtoken' );
const jwtSimple = require( 'jwt-simple' );
const moment    = require( 'moment' );
const sinon     = require( 'sinon' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

let authService;
let data;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token         = jwt.sign( payload, process.env.JWT_KEY );
	const authorization = `Bearer ${token}`;

	data = {
		authorization
	};

	authService = use( 'services/auth' );

	done();
} );

describe( 'AuthService::authenticate', () => {
	it( 'should be able to handle jwt error', done => {
		sinon.stub( jwtSimple, 'encode', () => {
			throw new Error( 'jwt error' );
		} );

		authService.refreshToken( data )
			.then( response => {
				expect( response ).to.be.an.error( 'jwt error' );

				jwtSimple.encode.restore();
				done();
			} );
	} );

	it ( 'should be able to return authUser when successful', done => {
		authService.refreshToken( data )
			.then( response => {
				expect( response ).to.be.an.object();
				expect( response.Token ).to.be.a.string();

				done();
			} );
	} );
} );
