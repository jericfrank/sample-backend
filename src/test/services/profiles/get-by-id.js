'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const Profile = models.Profile;

let profileService;
let data;

before( done => {
	data = {
		'Id' : 1
	};

	profileService = use( 'services/profiles' );

	done();
} );

describe( 'ProfileService::getById', () => {
	it( 'should be able to return null when profile does not exist', done => {
		sinon.stub( Profile, 'findById', () => Promise.resolve( null ) );

		profileService.getById( data.Id )
			.then( response => {
				expect( response ).to.be.equal( null );

				Profile.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return profile', done => {
		sinon.stub( Profile, 'findById', () => {
			return Promise.resolve( {
				toJSON () {
					return {
						'Id'         : 2,
						'Name'       : 'power users',
						'CreatedBy'  : 1,
						'UpdatedBy'  : 1,
						'created_at' : '2017-02-06T05:59:56.000Z',
						'updated_at' : '0000-00-00 00:00:00',
						'deleted_at' : null
					};
				}
			} );
		} );

		profileService.getById( data.Id )
			.then( response => {
				expect( response ).to.be.an.object();

				Profile.findById.restore();
				done();
			} );
	} );
});
