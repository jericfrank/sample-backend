'use strict';

require( 'dotenv' ).config();

const Code  = require( 'code' );
const Lab   = require( 'lab' );
const sinon = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const Profile = models.Profile;

let profileService;

before( done => {
	profileService = use( 'services/profiles' );

	done();
} );

describe( 'ProfileService::getAll', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( Profile, 'findAll', () => {
			throw new Error( 'Error' );
		} );

		profileService.getAll()
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				Profile.findAll.restore();
				done();
			} );
	} );

	it ( 'should be able to fetch all resource', done => {
		sinon.stub( Profile, 'findAll', () => {
			return Promise.resolve( [ {
				'Id'         : 1,
				'Name'       : 'admin',
				'CreatedBy'  : 1,
				'UpdatedBy'  : 1,
				'created_at' : '2017-02-06T05:59:56.000Z',
				'updated_at' : '0000-00-00 00:00:00'
			} ] );
		} );

		profileService.getAll()
			.then( response => {
				expect( response ).to.be.an.array();

				Profile.findAll.restore();
				done();
			} );
	} );
} );
