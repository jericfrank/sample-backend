'use strict';

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );


const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const Station = models.Station;

let stationService;
let data;

before( done => {
	data = {
		'Id' : 1
	};

	stationService = use( 'services/stations' );

	done();
} );

describe( 'StationService::getById', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( Station, 'findById', () => {
			throw new Error( 'Error' );
		} );

		stationService.getById( data.Id )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				Station.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return null when user does not exist', done => {
		sinon.stub( Station, 'findById', () => Promise.resolve( null ) );

		stationService.getById( data.Id )
			.then( response => {
				expect( response ).to.be.equal( null );

				Station.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return user', done => {
		sinon.stub( Station, 'findById', () => {
			return Promise.resolve( {
				toJSON () {
					return {
						'Id'         : 1,
						'Name'       : 'MWFB',
						'Location'   : 'Wisconsin',
						'Logo'       : null,
						'IsActive'   : true,
						'CreatedBy'  : 1,
						'UpdatedBy'  : 1,
						'created_at' : '2017-02-06T06:04:00.000Z',
						'updated_at' : '0000-00-00 00:00:00',
						'deleted_at' : null
					};
				}
			} );
		} );

		stationService.getById( data.Id )
			.then( response => {
				expect( response ).to.be.an.object();

				Station.findById.restore();
				done();
			} );
	} );
});
