'use strict';

require( 'dotenv' ).config();

const _     = require( 'lodash' );
const Code  = require( 'code' );
const Lab   = require( 'lab' );
const sinon = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const Station         = models.Station;
const CalendarSetting = models.CalendarSetting;
const User            = models.User;

let stationService;
let companyService;
let data;

before( done => {
	data = {
		'Name'            : 'EnergyFM',
		'Location'        : 'Cebu',
		'Logo'            : '',
		'CalendarSetting' : {
			'StartOfWeek' : 1
		}
	};

	stationService = use( 'services/stations' );
	companyService = use( 'services/company' );

	done();
} );

describe( 'StationService::create', () => {
	it( 'should be able to return an error', done => {
		sinon.stub( User, 'findById', () => {
			return Promise.resolve( {
				'ProfileId' : 1
			} );
		} );

		sinon.stub( companyService, 'getAll', () => {
			return Promise.resolve( {
				'PricingType' : 2,
				'MaxLimit'    : 5
			} );
		} );

		sinon.stub( stationService, 'getAll', () => {
			return Promise.resolve( _.range( 1 ) );
		} );

		sinon.stub( Station, 'create', () => {
			throw new Error( 'Creation Error' );
		} );

		stationService.create( 1, data )
			.then( response => {
				expect( response ).to.be.an.error( Error, 'Creation Error' );

				User.findById.restore();
				companyService.getAll.restore();
				stationService.getAll.restore();
				Station.create.restore();
				done();
			} );
	} );

	it( 'should be able to check permission, check subscription and return success', done => {
		sinon.stub( User, 'findById', () => {
			return Promise.resolve( {
				'ProfileId' : 1
			} );
		} );

		sinon.stub( companyService, 'getAll', () => {
			return Promise.resolve( {
				'PricingType' : 2,
				'MaxLimit'    : 5
			} );
		} );

		sinon.stub( stationService, 'getAll', () => {
			return Promise.resolve( _.range( 1 ) );
		} );

		sinon.stub( Station, 'create', () => {
			const response = {
				'Id'         : 1,
				'Name'       : 'MWFB',
				'Location'   : 'Wisconsin',
				'Logo'       : null,
				'CreatedBy'  : 1,
				'UpdatedBy'  : 1,
				'created_at' : '2017-02-06T06:04:00.000Z',
				'updated_at' : '0000-00-00 00:00:00',
				'deleted_at' : null,

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( CalendarSetting, 'create', () => {
			const response = {
				'CreatedBy'       : 0,
				'UpdatedBy'       : 0,
				'Id'              : 20,
				'StationId'       : 35,
				'StorageLocation' : 'gs-a51732e2-fd4f-4143-9ce9-122e729b041d',
				'StartOfWeek'     : 1,
				'updated_at'      : '2017-03-09T02:39:26.000Z',
				'created_at'      : '2017-03-09T02:39:26.000Z',
				'CalendarSetting' : {
					'CreatedBy'       : 1,
					'UpdatedBy'       : 1,
					'StationId'       : 53,
					'StorageLocation' : '<path-to-storage>',
					'StartOfWeek'     : 5,
					'created_at'      : '2017-02-06T06:04:00.000Z',
					'updated_at'      : '0000-00-00 00:00:00'
				},

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		stationService.create( 1, data )
			.then( response => {
				expect( response ).to.be.an.object();

				User.findById.restore();
				companyService.getAll.restore();
				stationService.getAll.restore();
				Station.create.restore();
				CalendarSetting.create.restore();
				done();
			} );
	} );

	it( 'should be able to check permission and throw subscription error', done => {
		sinon.stub( User, 'findById', () => {
			return Promise.resolve( {
				'ProfileId' : 2
			} );
		} );

		sinon.stub( companyService, 'getAll', () => {
			return Promise.resolve( {
				'PricingType' : 1,
				'MaxLimit'    : 1
			} );
		} );

		sinon.stub( stationService, 'getAll', () => {
			return Promise.resolve( _.range( 1 ) );
		} );

		sinon.stub( Station, 'create', () => {
			const response = {
				'Id'         : 1,
				'Name'       : 'MWFB',
				'Location'   : 'Wisconsin',
				'Logo'       : null,
				'CreatedBy'  : 1,
				'UpdatedBy'  : 1,
				'created_at' : '2017-02-06T06:04:00.000Z',
				'updated_at' : '0000-00-00 00:00:00',
				'deleted_at' : null,

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( CalendarSetting, 'create', () => {
			const response = {
				'CreatedBy'       : 0,
				'UpdatedBy'       : 0,
				'Id'              : 20,
				'StationId'       : 35,
				'StorageLocation' : 'gs-a51732e2-fd4f-4143-9ce9-122e729b041d',
				'StartOfWeek'     : 1,
				'updated_at'      : '2017-03-09T02:39:26.000Z',
				'created_at'      : '2017-03-09T02:39:26.000Z',
				'CalendarSetting' : {
					'CreatedBy'       : 1,
					'UpdatedBy'       : 1,
					'StationId'       : 53,
					'StorageLocation' : '<path-to-storage>',
					'StartOfWeek'     : 5,
					'created_at'      : '2017-02-06T06:04:00.000Z',
					'updated_at'      : '0000-00-00 00:00:00'
				},

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		stationService.create( 1, data )
			.then( response => {
				expect( response ).to.be.an.object();

				expect( response ).to.be.an.error();

				User.findById.restore();
				companyService.getAll.restore();
				stationService.getAll.restore();
				Station.create.restore();
				CalendarSetting.create.restore();
				done();
			} );
	} );

	it( 'should be able to check permission false, and return success', done => {
		sinon.stub( User, 'findById', () => {
			return Promise.resolve( {
				'ProfileId' : 2
			} );
		} );

		sinon.stub( companyService, 'getAll', () => {
			return Promise.resolve( {
				'PricingType' : 2,
				'MaxLimit'    : 5
			} );
		} );

		sinon.stub( stationService, 'getAll', () => {
			return Promise.resolve( _.range( 1 ) );
		} );

		sinon.stub( Station, 'create', () => {
			const response = {
				'Id'         : 1,
				'Name'       : 'MWFB',
				'Location'   : 'Wisconsin',
				'Logo'       : null,
				'CreatedBy'  : 1,
				'UpdatedBy'  : 1,
				'created_at' : '2017-02-06T06:04:00.000Z',
				'updated_at' : '0000-00-00 00:00:00',
				'deleted_at' : null,

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( CalendarSetting, 'create', () => {
			const response = {
				'CreatedBy'       : 0,
				'UpdatedBy'       : 0,
				'Id'              : 20,
				'StationId'       : 35,
				'StorageLocation' : 'gs-a51732e2-fd4f-4143-9ce9-122e729b041d',
				'StartOfWeek'     : 1,
				'updated_at'      : '2017-03-09T02:39:26.000Z',
				'created_at'      : '2017-03-09T02:39:26.000Z',
				'CalendarSetting' : {
					'CreatedBy'       : 1,
					'UpdatedBy'       : 1,
					'StationId'       : 53,
					'StorageLocation' : '<path-to-storage>',
					'StartOfWeek'     : 5,
					'created_at'      : '2017-02-06T06:04:00.000Z',
					'updated_at'      : '0000-00-00 00:00:00'
				},

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		stationService.create( 1, data )
			.then( response => {
				expect( response ).to.be.an.object();

				User.findById.restore();
				companyService.getAll.restore();
				stationService.getAll.restore();
				Station.create.restore();
				CalendarSetting.create.restore();
				done();
			} );
	} );
} );
