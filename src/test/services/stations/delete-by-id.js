'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const CalendarSetting = models.CalendarSetting;
const Station         = models.Station;


let stationService;
let jobService;
let data;

before( done => {
	data = {
		'Id' : 1
	};

	stationService = use( 'services/stations' );
	jobService     = use( 'services/jobs' );

	done();
} );

describe( 'StationService::deleteById', () => {
	it ( 'should be able to catch 500 server error', done => {
		sinon.stub( Station, 'findById', () => {
			throw new Error( 'Finding Error' );
		} );

		stationService.deleteById( data.Id )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Finding Error' );

				Station.findById.restore();
				done();
			} );
	} );

	it ( 'should be able to return null when resource does not exist', done => {
		sinon.stub( Station, 'findById', () => Promise.resolve( null ) );

		stationService.deleteById( data.Id )
			.then( response => {
				expect( response ).to.be.equal( null );

				Station.findById.restore();
				done();
			} );
	} );

	it ( 'should be able to return badData when resource still has a reference to other resource', done => {
		sinon.stub( Station, 'findById', () => {
			return Promise.resolve( {
				'Id'         : 1,
				'Name'       : 'MWFB',
				'Location'   : 'Wisconsin',
				'Logo'       : null,
				'CreatedBy'  : 1,
				'UpdatedBy'  : 1,
				'created_at' : '2017-02-06T06:04:00.000Z',
				'updated_at' : '0000-00-00 00:00:00',
				'deleted_at' : null,
				'Users'      : [ {
					'Id' : 1
				} ]
			} );
		} );

		stationService.deleteById( data.Id )
			.then( response => {
				expect( response ).to.be.an.object();
				expect( response.errorType ).to.exist();

				Station.findById.restore();
				done();
			} );
	} );

	it( 'should be able to delete', done => {
		sinon.stub( Station, 'findById', () => {
			const response = {
					'Id'         : 1,
					'Name'       : 'MWFB',
					'Location'   : 'Wisconsin',
					'Logo'       : null,
					'CreatedBy'  : 1,
					'UpdatedBy'  : 1,
					'created_at' : '2017-02-06T06:04:00.000Z',
					'updated_at' : '0000-00-00 00:00:00',
					'deleted_at' : null,
					'Users'      : [ ],

					toJSON () {
						return response;
					}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( CalendarSetting, 'destroy', () => Promise.resolve( 1 ) );
		sinon.stub( Station, 'destroy', () => Promise.resolve( 1 ) );

		sinon.stub( stationService, 'deleteConfig', () => Promise.resolve( true ) );
		sinon.stub( jobService, 'deleteFile', () => Promise.resolve( true ) );

		stationService.deleteById( data.Id )
			.then( response => {
				expect( response ).to.be.an.object();
				expect( response.Operation ).to.exist();

				Station.findById.restore();
				Station.destroy.restore();
				CalendarSetting.destroy.restore();
				stationService.deleteConfig.restore();
				jobService.deleteFile.restore();
				done();
			} );
	} );
} );
