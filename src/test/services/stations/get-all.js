'use strict';

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );


const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const Station = models.Station;

let stationService;

before( done => {
	stationService = use( 'services/stations' );

	done();
} );

describe( 'StationService::getAll', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( Station, 'findAll', () => {
			throw new Error( 'Finding Error' );
		} );

		stationService.getAll()
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Finding Error' );

				Station.findAll.restore();
				done();
			} );
	} );

	it ( 'should be able to fetch all resource', done => {
		sinon.stub( Station, 'findAll', () => {
			const response = {
				'Id'         : 1,
				'Name'       : 'MWFB',
				'Location'   : 'Wisconsin',
				'Logo'       : null,
				'CreatedBy'  : 1,
				'UpdatedBy'  : 1,
				'created_at' : '2017-02-06T06:04:00.000Z',
				'updated_at' : '0000-00-00 00:00:00',
				'deleted_at' : null,

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( [ response ] );
		} );

		stationService.getAll()
			.then( response => {
				expect( response ).to.be.an.array();

				Station.findAll.restore();
				done();
			} );
	} );
} );
