'use strict';

const Code  = require( 'code' );
const Lab   = require( 'lab' );
const kue   = require( 'kue' );
const sinon = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const Station         = models.Station;
const CalendarSetting = models.CalendarSetting;

let stationService;
let data;

before( done => {
	data = {
		'Id'              : 1,
		'Name'            : 'MWFBs',
		'Location'        : 'Wisconsin',
		'Logo'            : 'path-to-image.jpg',
		'CreatedBy'       : 1,
		'UpdatedBy'       : 1,
		'created_at'      : '2017-02-06T06:04:00.000Z',
		'updated_at'      : '0000-00-00 00:00:00',
		'CalendarSetting' : {
			'Id'          : 1,
			'StartOfWeek' : 1
		}
	};

	stationService = use( 'services/stations' );

	done();
} );

describe( 'StationService::updateById', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( Station, 'findById', () => {
			throw new Error( 'Error' );
		} );

		stationService.updateById( data.Id, data )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				Station.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return null when resource does not exist', done => {
		sinon.stub( Station, 'findById', () => Promise.resolve( null ) );

		stationService.updateById( data.Id, data )
			.then( response => {
				expect( response ).to.be.equal( null );

				Station.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return resource', done => {
		const calendarSetting = {
			'CreatedBy'       : 1,
			'UpdatedBy'       : 1,
			'StationId'       : 53,
			'StorageLocation' : '<path-to-storage>',
			'StartOfWeek'     : 5,
			'created_at'      : '2017-02-06T06:04:00.000Z',
			'updated_at'      : '0000-00-00 00:00:00',

			update () {
				return Promise.resolve( {
					toJSON () {
						return calendarSetting;
					}
				} );
			}
		};

		sinon.stub( CalendarSetting, 'findById', () => {
			return Promise.resolve( calendarSetting );
		} );

		const station = {
			'Id'         : 1,
			'Name'       : 'MWFB',
			'Location'   : 'Wisconsin',
			'Logo'       : 'path-to-image.jpg',
			'IsActive'   : true,
			'CreatedBy'  : 1,
			'UpdatedBy'  : 1,
			'created_at' : '2017-02-06T06:04:00.000Z',
			'updated_at' : '0000-00-00 00:00:00',

			update () {
				return Promise.resolve( {
					toJSON () {
						return station;
					}
				} );
			}
		};

		sinon.stub( Station, 'findById', () => {
			return Promise.resolve( station );
		} );

		sinon.stub( kue, 'createQueue', () => {
			const queue = {
				create () {
					return queue;
				},

				delay () {
					return queue;
				},

				save () {
					return {
						'id' : 1
					};
				},

				shutdown () {
					return queue;
				}
			};

			return queue;
		} );

		stationService.updateById( data.Id, data )
			.then( response => {
				expect( response ).to.be.an.object();

				Station.findById.restore();
				CalendarSetting.findById.restore();
				kue.createQueue.restore();
				done();
			} );
	} );
} );
