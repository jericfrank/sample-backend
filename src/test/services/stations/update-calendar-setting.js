'use strict';

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const CalendarSetting = models.CalendarSetting;

let stationService;
let data;

before( done => {
	data = {
		'Id'          : 1,
		'StartOfWeek' : 1
	};

	stationService = use( 'services/stations' );

	done();
} );

describe( 'StationService::updateCalendarSetting', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( CalendarSetting, 'findById', () => {
			throw new Error( 'Error' );
		} );

		stationService.updateCalendarSetting( data.Id, data )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				CalendarSetting.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return null when user does not exist', done => {
		sinon.stub( CalendarSetting, 'findById', () => Promise.resolve( null ) );

		stationService.updateCalendarSetting( data.Id, data )
			.then( response => {
				expect( response ).to.be.equal( null );

				CalendarSetting.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return user', done => {
		sinon.stub( CalendarSetting, 'findById', () => {
			const response = {
				'Id'              : 1,
				'StationId'       : 49,
				'StorageLocation' : 'gs-abafm-1489118452',
				'StartOfWeek'     : 1,
				'CreatedBy'       : 1,
				'UpdatedBy'       : 1,
				'created_at'      : '2017-02-06T06:04:00.000Z',
				'updated_at'      : '0000-00-00 00:00:00',

				update () {
					return Promise.resolve( {
						toJSON () {
							return response;
						}
					} );
				}
			};

			return Promise.resolve( response );
		} );


		stationService.updateCalendarSetting( data.Id, data )
			.then( response => {
				expect( response ).to.be.an.object();

				CalendarSetting.findById.restore();
				done();
			} );
	} );
} );
