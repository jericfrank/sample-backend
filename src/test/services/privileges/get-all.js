'use strict';

require( 'dotenv' ).config();

const Code  = require( 'code' );
const Lab   = require( 'lab' );
const sinon = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const ProfileModule = models.ProfileModule;

let privilegesService;
let response;

before( done => {
	response = {
		'Id'         : 1,
		'Name'       : 'admin',
		'CreatedBy'  : 1,
		'UpdatedBy'  : 1,
		'created_at' : '2017-02-06T05:59:56.000Z',
		'updated_at' : '0000-00-00 00:00:00',

		toJSON () {
			return response;
		}
	};

	privilegesService = use( 'services/privileges' );

	done();
} );

describe( 'PrivilegesService::getAll', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( ProfileModule, 'findAll', () => {
			throw new Error( 'Error' );
		} );

		privilegesService.getAll()
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				ProfileModule.findAll.restore();
				done();
			} );
	} );

	it ( 'should be able to fetch resource filtered by profile query string', done => {
		sinon.stub( ProfileModule, 'findAll', () => {
			return Promise.resolve( [ response ] );
		} );

		privilegesService.getAll( 1 )
			.then( response => {
				expect( response ).to.be.an.array();

				ProfileModule.findAll.restore();
				done();
			} );
	} );

	it ( 'should be able to fetch all resource', done => {
		sinon.stub( ProfileModule, 'findAll', () => {
			return Promise.resolve( [ response ] );
		} );

		privilegesService.getAll()
			.then( response => {
				expect( response ).to.be.an.array();

				ProfileModule.findAll.restore();
				done();
			} );
	} );
} );
