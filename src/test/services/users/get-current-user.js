'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const config = use( 'config' );
const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const User = models.User;

let userService;
let privilegeService;
let data;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token = jwt.sign( payload, config.app.jwtKey );

	data = {
		'authorization' : token
	};

	userService      = use( 'services/users' );
	privilegeService = use( 'services/privileges' );

	done();
} );

describe( 'UserService::getCurrentUser', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( User, 'findById', () => {
			throw new Error( 'Error' );
		} );

		userService.getCurrentUser( data )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				User.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return null when user does not exist', done => {
		sinon.stub( User, 'findById', () => Promise.resolve( null ) );

		userService.getCurrentUser( data )
			.then( response => {
				expect( response ).to.be.equal( null );

				User.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return user', done => {
		sinon.stub( User, 'findById', () => {
			const response = {
				'Id'              : 1,
				'UserName'        : 'danswater',
				'FirstName'       : 'Francis',
				'MiddleName'      : 'Viscayno',
				'LastName'        : 'Lamayo',
				'EmailAddress'    : 'dansisabot@gmail.com',
				'ProfileId'       : 1,
				'StationId'       : 1,
				'AccountDisabled' : false,
				'CreatedBy'       : 0,
				'UpdatedBy'       : 0,
				'created_at'      : '2017-02-06T06:04:16.000Z',
				'updated_at'      : '2017-02-22T07:13:04.000Z',

				toJSON () {
					return response;
				},

				getStations () {
					const station = {
						'Name'        : 'Station 1',
						'UserStation' : {
							'Default' : false
						},

						toJSON () {
							return station;
						}
					};
					return Promise.resolve( [ station ] );
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( privilegeService, 'getAll', () => Promise.resolve( [ {
			'ModuleId'  : 1,
			'ProfileId' : 1,
			'CanRead'   : true,
			'CanCreate' : true,
			'CanUpdate' : true,
			'CanDelete' : true,
			'Module'    : {
				'Id'   : 1,
				'Name' : 'User Management'
			}
		} ] ) );

		userService.getCurrentUser( data )
			.then( response => {
				expect( response ).to.be.an.object();

				User.findById.restore();
				done();
			} );
	} );
});
