'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const User = models.User;

let userService;
let data;

before( done => {
	data = {
		'Id' : 1
	};

	userService = use( 'services/users' );

	done();
} );

describe( 'UserService::deleteById', () => {
	it ( 'should be able to catch 500 server error', done => {
		sinon.stub( User, 'findById', () => {
			throw new Error( 'Finding Error' );
		} );

		userService.deleteById( data.Id )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Finding Error' );

				User.findById.restore();
				done();
			} );
	} );

	it ( 'should be able to return null when user does not exist', done => {
		sinon.stub( User, 'findById', () => Promise.resolve( null ) );

		userService.deleteById( data.Id )
			.then( response => {
				expect( response ).to.be.equal( null );

				User.findById.restore();
				done();
			} );
	} );

	it( 'should be able to delete', done => {
		sinon.stub( User, 'findById', () => {
			return {
				'Id'         : 1,
				'UserName'   : 'danswater',
				'FistName'   : 'Francis',
				'MiddleName' : 'Viscayno',
				'LastName'   : 'Lamayo'
			};
		} );

		sinon.stub( User, 'destroy', () => Promise.resolve( 1 ) );

		userService.deleteById( data.Id )
			.then( response => {
				expect( response ).to.be.equal( 1 );

				User.findById.restore();
				User.destroy.restore();
				done();
			} );
	} );
} );
