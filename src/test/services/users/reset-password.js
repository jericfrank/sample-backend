'use strict';

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const log    = use( 'logging' );
const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

const User = models.User;

let userService;
let data;

before( done => {
	const payload = {
		'sub' : 'Forgiagether46@superrito.com',
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, process.env.JWT_KEY );

	data = {
		'Token'    : token,
		'Password' : 'valar-morghulis'
	};

	userService = use( 'services/users' );

	sinon.stub( log, 'fatal' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	done();
} );

describe( 'UserService::reset-password', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( User, 'findOne', () => {
			throw new Error( 'Error' );
		} );

		userService.resetPassword( data )
			.then( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				User.findOne.restore();
				done();
			} );
	} );

	it( 'should return null when resource does not exists', done => {
		sinon.stub( User, 'findOne' ).returns( Promise.resolve( null ) );

		userService.resetPassword( data )
			.then( response => {
				expect( response ).to.be.equal( null );

				User.findOne.restore();
				done();
			} );
	} );

	it( 'should return Operation true after update', done => {
		sinon.stub( User, 'findOne' ).returns( Promise.resolve( {
			'Id'              : 1,
			'UserName'        : 'danswater',
			'FirstName'       : 'Francis',
			'MiddleName'      : 'Viscayno',
			'LastName'        : 'Lamayo',
			'EmailAddress'    : 'dansisabot@gmail.com',
			'Password'        : 'secret',
			'ProfileId'       : 1,
			'StationId'       : 1,
			'AccountDisabled' : false,

			update () {
				return Promise.resolve( {
					toJSON () {
						return true;
					}
				} );
			}
		} ) );

		userService.resetPassword( data )
			.then( response => {
				expect( response ).to.be.an.object();

				User.findOne.restore();
				done();
			} );
	} );
} );
