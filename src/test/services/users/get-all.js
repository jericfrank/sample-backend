'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const User = models.User;

let userService;

before( done => {
	userService = use( 'services/users' );

	done();
} );

describe( 'UserService::getAll', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( User, 'findAll', () => {
			throw new Error( 'Finding Error' );
		} );

		userService.getAll()
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Finding Error' );

				User.findAll.restore();
				done();
			} );
	} );

	it ( 'should be able to fetch all resource', done => {
		sinon.stub( User, 'findAll', () => {
			return Promise.resolve( [ {
				'Id'              : 1,
				'UserName'        : 'danswater',
				'FirstName'       : 'Francis',
				'MiddleName'      : 'Viscayno',
				'LastName'        : 'Lamayo',
				'EmailAddress'    : 'dansisabot@gmail.com',
				'ProfileId'       : 1,
				'StationId'       : 1,
				'AccountDisabled' : false,
				'CreatedBy'       : 0,
				'UpdatedBy'       : 0,
				'created_at'      : '2017-02-06T06:04:16.000Z',
				'updated_at'      : '2017-02-22T07:13:04.000Z'
			} ] );
		} );

		userService.getAll()
			.then( response => {
				expect( response ).to.be.an.array();

				User.findAll.restore();
				done();
			} );
	} );
} );
