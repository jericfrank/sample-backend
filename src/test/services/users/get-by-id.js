'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const User = models.User;

let userService;
let data;

before( done => {
	data = {
		'Id' : 1
	};

	userService = use( 'services/users' );

	done();
} );

describe( 'UserService::getById', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( User, 'findById', () => {
			throw new Error( 'Error' );
		} );

		userService.getById( data.Id )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				User.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return null when user does not exist', done => {
		sinon.stub( User, 'findById', () => Promise.resolve( null ) );

		userService.getById( data.Id )
			.then( response => {
				expect( response ).to.be.equal( null );

				User.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return user', done => {
		sinon.stub( User, 'findById', () => {
			const response = {
				'Id'              : 1,
				'UserName'        : 'danswater',
				'FirstName'       : 'Francis',
				'MiddleName'      : 'Viscayno',
				'LastName'        : 'Lamayo',
				'EmailAddress'    : 'dansisabot@gmail.com',
				'ProfileId'       : 1,
				'StationId'       : 1,
				'AccountDisabled' : false,
				'CreatedBy'       : 0,
				'UpdatedBy'       : 0,
				'created_at'      : '2017-02-06T06:04:16.000Z',
				'updated_at'      : '2017-02-22T07:13:04.000Z',

				toJSON () {
					return response;
				},

				getStations () {
					const station = {
						'Name'        : 'Station 1',
						'UserStation' : {
							'Default' : false
						},

						toJSON () {
							return station;
						}
					};
					return Promise.resolve( [ station ] );
				}
			};

			return Promise.resolve( response );
		} );

		userService.getById( data.Id )
			.then( response => {
				expect( response ).to.be.an.object();

				User.findById.restore();
				done();
			} );
	} );
});
