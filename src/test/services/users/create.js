'use strict';

require( 'dotenv' ).config();

const _     = require( 'lodash' );
const Code  = require( 'code' );
const Lab   = require( 'lab' );
const sinon = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const User        = models.User;
const UserStation = models.UserStation;

let userService;
let companyService;
let data;

before( done => {
	data = {
		'UserName'     : 'danswater',
		'FirstName'    : 'Francis',
		'MiddleName'   : 'Viscayno',
		'LastName'     : 'Lamayo',
		'EmailAddress' : 'dansisabot@gmail.com',
		'Password'     : 'secret',
		'Stations'     : [ 1, 52, 53 ]
	};

	userService    = use( 'services/users' );
	companyService = use( 'services/company' );

	done();
} );

describe( 'UserService::create', () => {
	it( 'should be able to return an error', done => {
		sinon.stub( User, 'findById', () => {
			return Promise.resolve( {
				'ProfileId' : 1
			} );
		} );

		sinon.stub( companyService, 'getAll', () => {
			return Promise.resolve( {
				'PricingType' : 2,
				'MaxLimit'    : 5
			} );
		} );

		sinon.stub( userService, 'getAll', () => {
			return Promise.resolve( _.range( 1 ) );
		} );

		sinon.stub( User, 'create', () => {
			throw new Error( 'Creation Error' );
		} );

		userService.create( 1, data )
			.then( response => {
				expect( response ).to.be.an.error( Error, 'Creation Error' );

				User.findById.restore();
				userService.getAll.restore();
				companyService.getAll.restore();
				User.create.restore();
				done();
			} );
	} );

	it( 'should be able to check permission, check subscription and return success', done => {
		sinon.stub( User, 'findById', () => {
			return Promise.resolve( {
				'ProfileId' : 1
			} );
		} );

		sinon.stub( companyService, 'getAll', () => {
			return Promise.resolve( {
				'PricingType' : 2,
				'MaxLimit'    : 5
			} );
		} );

		sinon.stub( userService, 'getAll', () => {
			return Promise.resolve( _.range( 1 ) );
		} );

		sinon.stub( User, 'create', () => {
			return Promise.resolve( {
				toJSON () {
					return {
						'Id'              : 1,
						'UserName'        : 'danswater',
						'FirstName'       : 'Francis',
						'MiddleName'      : 'Viscayno',
						'LastName'        : 'Lamayo',
						'EmailAddress'    : 'dansisabot@gmail.com',
						'ProfileId'       : 1,
						'StationId'       : 1,
						'AccountDisabled' : false,
						'CreatedBy'       : 0,
						'UpdatedBy'       : 0,
						'created_at'      : '2017-02-06T06:04:16.000Z',
						'updated_at'      : '2017-02-22T07:13:04.000Z'
					};
				}
			} );
		} );

		sinon.stub( UserStation, 'bulkCreate', () => Promise.resolve() );

		userService.create( 1, data )
			.then( response => {
				expect( response ).to.be.an.object();

				User.findById.restore();
				userService.getAll.restore();
				companyService.getAll.restore();
				User.create.restore();
				UserStation.bulkCreate.restore();

				done();
			} );
	} );

	it( 'should be able to check permission and throw subscription error', done => {
		sinon.stub( User, 'findById', () => {
			return Promise.resolve( {
				'ProfileId' : 2
			} );
		} );

		sinon.stub( companyService, 'getAll', () => {
			return Promise.resolve( {
				'PricingType' : 2,
				'MaxLimit'    : 1
			} );
		} );

		sinon.stub( userService, 'getAll', () => {
			return Promise.resolve( _.range( 1 ) );
		} );

		sinon.stub( User, 'create', () => {
			return Promise.resolve( {
				toJSON () {
					return {
						'Id'              : 1,
						'UserName'        : 'danswater',
						'FirstName'       : 'Francis',
						'MiddleName'      : 'Viscayno',
						'LastName'        : 'Lamayo',
						'EmailAddress'    : 'dansisabot@gmail.com',
						'ProfileId'       : 1,
						'StationId'       : 1,
						'AccountDisabled' : false,
						'CreatedBy'       : 0,
						'UpdatedBy'       : 0,
						'created_at'      : '2017-02-06T06:04:16.000Z',
						'updated_at'      : '2017-02-22T07:13:04.000Z'
					};
				}
			} );
		} );

		sinon.stub( UserStation, 'bulkCreate', () => Promise.resolve() );

		userService.create( 1, data )
			.then( response => {
				expect( response ).to.be.an.object();

				expect( response ).to.be.an.error();

				User.findById.restore();
				userService.getAll.restore();
				companyService.getAll.restore();
				User.create.restore();
				UserStation.bulkCreate.restore();

				done();
			} );
	} );

	it( 'should be able to check permission false, and return success', done => {
		sinon.stub( User, 'findById', () => {
			return Promise.resolve( {
				'ProfileId' : 2
			} );
		} );

		sinon.stub( companyService, 'getAll', () => {
			return Promise.resolve( {
				'PricingType' : 2,
				'MaxLimit'    : 1
			} );
		} );

		sinon.stub( userService, 'getAll', () => {
			return Promise.resolve( _.range( 1 ) );
		} );

		sinon.stub( User, 'create', () => {
			return Promise.resolve( {
				toJSON () {
					return {
						'Id'              : 1,
						'UserName'        : 'danswater',
						'FirstName'       : 'Francis',
						'MiddleName'      : 'Viscayno',
						'LastName'        : 'Lamayo',
						'EmailAddress'    : 'dansisabot@gmail.com',
						'ProfileId'       : 1,
						'StationId'       : 1,
						'AccountDisabled' : false,
						'CreatedBy'       : 0,
						'UpdatedBy'       : 0,
						'created_at'      : '2017-02-06T06:04:16.000Z',
						'updated_at'      : '2017-02-22T07:13:04.000Z'
					};
				}
			} );
		} );

		sinon.stub( UserStation, 'bulkCreate', () => Promise.resolve() );

		userService.create( 1, data )
			.then( response => {
				expect( response ).to.be.an.object();

				User.findById.restore();
				userService.getAll.restore();
				companyService.getAll.restore();
				User.create.restore();
				UserStation.bulkCreate.restore();

				done();
			} );
	} );
} );
