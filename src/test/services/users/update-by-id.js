'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const User        = models.User;
const UserStation = models.UserStation;

let userService;
let data;

before( done => {
	data = {
		'Id'              : 1,
		'UserName'        : 'danswater',
		'FirstName'       : 'Franco',
		'MiddleName'      : 'Viscayno',
		'LastName'        : 'Escobar',
		'EmailAddress'    : 'dansisabot@gmail.com',
		'ProfileId'       : 1,
		'StationId'       : 1,
		'AccountDisabled' : false,
		'CreatedBy'       : 0,
		'UpdatedBy'       : 0,
		'created_at'      : '2017-02-06T06:04:16.000Z',
		'updated_at'      : '2017-02-22T07:13:04.000Z',
		'Stations'        : [ 1, 52, 53 ]
	};

	userService = use( 'services/users' );

	done();
} );

describe( 'UserService::updateById', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( User, 'findById', () => {
			throw new Error( 'Error' );
		} );

		userService.updateById( data.Id )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				User.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return null when user does not exist', done => {
		sinon.stub( User, 'findById', () => Promise.resolve( null ) );

		userService.updateById( data.Id )
			.then( response => {
				expect( response ).to.be.equal( null );

				User.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return user', done => {
		sinon.stub( User, 'findById', function () {
			return Promise.resolve( {
				'Id'              : 1,
				'UserName'        : 'danswater',
				'FirstName'       : 'Francis',
				'MiddleName'      : 'Viscayno',
				'LastName'        : 'Lamayo',
				'EmailAddress'    : 'dansisabot@gmail.com',
				'Password'        : 'secret',
				'ProfileId'       : 1,
				'StationId'       : 1,
				'AccountDisabled' : false,

				update () {
					return Promise.resolve( {
						toJSON () {
							return data;
						}
					} );
				}
			} );
		} );


		sinon.stub( UserStation, 'destroy', () => Promise.resolve() );
		sinon.stub( UserStation, 'bulkCreate', () => Promise.resolve() );

		userService.updateById( data.Id, data )
			.then( response => {
				expect( response ).to.be.an.object();

				User.findById.restore();
				UserStation.destroy.restore();
				UserStation.bulkCreate.restore();

				done();
			} );
	} );
} );
