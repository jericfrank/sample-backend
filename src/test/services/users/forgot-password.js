'use strict';

const Code      = require( 'code' );
const Lab       = require( 'lab' );
const sinon     = require( 'sinon' );

const log    = use( 'logging' );
const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

const User = models.User;

let userService;
let data;

before( done => {
	data = {
		'Email' : 'Forgiagether46@superrito.com'
	};

	userService = use( 'services/users' );

	sinon.stub( log, 'fatal' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	done();
} );

describe( 'UserService::forgot-password', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( User, 'findOne', () => {
			throw new Error( 'Error' );
		} );

		userService.forgotPassword( data )
			.then( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				User.findOne.restore();
				done();
			} );
	} );

	it( 'should return null when resource does not exists', done => {
		sinon.stub( User, 'findOne' ).returns( Promise.resolve( null ) );

		userService.forgotPassword( data )
			.then( response => {
				expect( response ).to.be.equal( null );

				User.findOne.restore();
				done();
			} );
	} );
} );
