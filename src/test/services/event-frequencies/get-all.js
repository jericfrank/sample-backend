'use strict';

require( 'dotenv' ).config();

const Code  = require( 'code' );
const Lab   = require( 'lab' );
const sinon = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const EventFrequencyType = models.EventFrequencyType;

let eventFrequencyService;

before( done => {
	eventFrequencyService = use( 'services/event-frequencies' );

	done();
} );

describe( 'EventFrequencesService::getAll', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( EventFrequencyType, 'findAll', () => {
			throw new Error( 'Error' );
		} );

		eventFrequencyService.getAll()
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				EventFrequencyType.findAll.restore();
				done();
			} );
	} );

	it ( 'should be able to fetch all resource', done => {
		sinon.stub( EventFrequencyType, 'findAll', () => {
			const response = {
				'Id'         : 1,
				'Name'       : 'Static',
				'CreatedBy'  : 1,
				'UpdatedBy'  : 1,
				'created_at' : '2017-02-06T05:59:56.000Z',
				'updated_at' : '0000-00-00 00:00:00',

				toJSON () {
					return response;
				}
			};
			return Promise.resolve( [ response ] );
		} );

		eventFrequencyService.getAll()
			.then( response => {
				expect( response ).to.be.an.array();

				EventFrequencyType.findAll.restore();
				done();
			} );
	} );
} );
