'use strict';

require( 'dotenv' ).config();

const Code  = require( 'code' );
const Lab   = require( 'lab' );
const sinon = require( 'sinon' );
const kue   = require( 'kue' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

let jobService;

before( done => {
	jobService = use( 'services/jobs' );
	done();
} );

describe( 'JobService::createConfig', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( kue, 'createQueue', () => {
			throw new Error( 'Error' );
		} );

		jobService.deleteById()
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				kue.createQueue.restore();
				done();
			} );
	} );

	it( 'should be able to throw an error when .get throws an error', done => {
		sinon.stub( kue, 'createQueue', () => {
			const response = {
				save ( callback ) {
					callback();
				},

				create () {
					return response;
				},

				on ( event, callback ) {
					callback( true );
				},

				shutdown () {
					return true;
				}
			};
			return response;
		} );

		kue.Job = {
			get ( id, callback ) {
				callback( new Error( 'Error' ) );
			}
		};


		jobService.deleteById( 1 )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				kue.createQueue.restore();
				done();
			} );
	} );

	it( 'should be able to throw an error when .get throws an error', done => {
		sinon.stub( kue, 'createQueue', () => {
			const response = {
				save ( callback ) {
					callback();
				},

				create () {
					return response;
				},

				on ( event, callback ) {
					callback( true );
				},

				shutdown () {
					return true;
				}
			};
			return response;
		} );

		kue.Job = {
			get ( id, callback ) {
				const job = {
					remove ( callback ) {
						callback( new Error( 'Error' ) );
					}
				};
				callback( null, job );
			}
		};


		jobService.deleteById( 1 )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				kue.createQueue.restore();
				done();
			} );
	} );

	it ( 'should be able to remove resource', done => {
		sinon.stub( kue, 'createQueue', () => {
			const response = {
				save ( callback ) {
					callback();
				},

				create () {
					return response;
				},

				on ( event, callback ) {
					callback( true );
				},

				shutdown () {
					return true;
				}
			};
			return response;
		} );

		const originalJob = kue.Job;

		kue.Job = {
			get ( id, callback ) {
				const job = {
					remove ( callback ) {
						callback( null );
					}
				};
				callback( null, job );
			}
		};

		jobService.deleteById( 1 )
			.then( response => {
				expect( response ).to.be.equal( true );

				kue.createQueue.restore();
				kue.Job = originalJob;
				done();
			} );
	} );
} );
