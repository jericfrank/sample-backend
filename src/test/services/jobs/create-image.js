'use strict';

require( 'dotenv' ).config();

const Code  = require( 'code' );
const Lab   = require( 'lab' );
const sinon = require( 'sinon' );
const kue   = require( 'kue' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

let jobService;

before( done => {
	jobService = use( 'services/jobs' );
	done();
} );

describe( 'JobService::createConfig', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( kue, 'createQueue', () => {
			throw new Error( 'Error' );
		} );

		jobService.createImage()
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				kue.createQueue.restore();
				done();
			} );
	} );

	it ( 'should be able to save resource', done => {
		sinon.stub( kue, 'createQueue', () => {
			const response = {
				save ( callback ) {
					callback();
				},

				create () {
					return response;
				},

				on ( event, callback ) {
					callback( true );
				},

				shutdown () {
					return true;
				}
			};
			return response;
		} );

		jobService.createImage( {
			'Name' : 'Testing'
		} )
			.then( response => {
				expect( response ).to.be.equal( true );

				kue.createQueue.restore();
				done();
			} );
	} );
} );
