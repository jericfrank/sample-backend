'use strict';

require( 'dotenv' ).config();

const Code  = require( 'code' );
const Lab   = require( 'lab' );
const sinon = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const { CompanyProfile } = models;

let companyService;
let data;

before( done => {
	data = {
		'Name'          : 'John Wick Bro. and Co.',
		'Address'       : 'Cebu PH',
		'ContactPerson' : 'John Wick',
		'ContactNumber' : '12345567',
		'PricingType'   : 1,
		'MaxLimit'      : 5,
		'CompanyLogo'   : null
	};

	companyService = use( 'services/company' );

	done();
} );

describe( 'CompanyService::create-or-update', () => {
	it( 'should be able to return an error', done => {
		sinon.stub( CompanyProfile, 'findAll', () => {
			throw new Error( 'Creation Error' );
		} );

		companyService.getAll( data )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Creation Error' );

				CompanyProfile.findAll.restore();
				done();
			} );
	} );

	it( 'should be able to create', done => {
		sinon.stub( CompanyProfile, 'findAll', () => {
			return Promise.resolve( [ {
				'Id' : 1,

				toJSON () {
					return {
						'Id' : 1
					};
				}
			} ] );
		} );

		companyService.getAll( data )
			.then( response => {
				expect( response ).to.be.an.object();

				CompanyProfile.findAll.restore();
				done();
			} );
	} );

	// it( 'should be able to update', done => {
	// 	sinon.stub( CompanyProfile, 'findOne', () => {
	// 		return Promise.resolve( {
	// 			update () {
	// 				return Promise.resolve( true );
	// 			}
	// 		} );
	// 	} );

	// 	sinon.stub( CompanyProfile, 'update', () => {
	// 		return Promise.resolve( true );
	// 	} );

	// 	sinon.stub( CompanyProfile, 'findAll', () => {
	// 		return Promise.resolve( [ {
	// 			'Id' : 1,

	// 			toJSON () {
	// 				return {
	// 					'Id' : 1
	// 				};
	// 			}
	// 		} ] );
	// 	} );

	// 	companyService.createOrUpdate( data )
	// 		.then( response => {
	// 			expect( response ).to.be.an.object();

	// 			CompanyProfile.findOne.restore();
	// 			CompanyProfile.update.restore();
	// 			CompanyProfile.findAll.restore();
	// 			done();
	// 		} );
	// } );
} );
