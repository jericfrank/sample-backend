'use strict';

require( 'dotenv' ).config();

const Code  = require( 'code' );
const Lab   = require( 'lab' );
const sinon = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const EventGamePart = models.EventGamePart;

let gamePartsService;

before( done => {
	gamePartsService = use( 'services/game-parts' );

	done();
} );

describe( 'EventFrequencesService::getAll', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( EventGamePart, 'findAll', () => {
			throw new Error( 'Error' );
		} );

		gamePartsService.getAll()
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				EventGamePart.findAll.restore();
				done();
			} );
	} );

	it ( 'should be able to fetch all resource', done => {
		sinon.stub( EventGamePart, 'findAll', () => {
			const response = {
				'Id'          : 1,
				'Name'        : 'INN',
				'Description' : 'Inn',

				toJSON () {
					return response;
				}
			};
			return Promise.resolve( [ response ] );
		} );

		gamePartsService.getAll()
			.then( response => {
				expect( response ).to.be.an.array();

				EventGamePart.findAll.restore();
				done();
			} );
	} );
} );
