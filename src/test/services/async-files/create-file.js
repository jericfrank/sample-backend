'use strict';

require( 'dotenv' ).config();

const AWS    = require( 'aws-sdk-mock' );
const Code   = require( 'code' );
const Lab    = require( 'lab' );
const kue    = require( 'kue' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const log            = use( 'logging' );
const { EventQueue } = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let createFile;
let job;

before( done => {
	const start = moment().format( 'YYYY-MM-DD 00:00:00' );
	const end   = moment().format( 'YYYY-MM-DD 23:59:59' );
	job = {
		'data' : {
			'Name'              : 'Sports FM',
			'Index'             : 0,
			'Delay'             : 6000,
			'FrequencyInterval' : 6000,
			'Event'             : {
				'Start'       : start,
				'End'         : end,
				'EventTypeId' : 1
			},

			'EventDetails' : [ {
				'Description' : 'hello string 1'
			}, {
				'Description' : 'hello string 2'
			} ]
		}
	};

	createFile = use( 'services/async-files/create-file' );

	sinon.stub( log, 'fatal' ).returns( '' );
	sinon.stub( log, 'info' ).returns( '' );
	done();
} );

after( done => {
	log.fatal.restore();
	log.info.restore();
	done();
} );

describe( 'FileService::create', () => {
	it( 'should be able catch queue error', done => {
		sinon.stub( kue, 'createQueue', () => {
			return {
				process () {
					throw new Error( 'error' );
				},

				on ( errorEvent, cb ) {
					cb( errorEvent );
				}
			};
		} );

		createFile()
			.catch( err => {
				expect( err ).to.be.an.error( Error, 'error' );
				kue.createQueue.restore();
				done();
			} );
	} );

	it( 'should be able to generate file', done => {
		let isCalled = false;

		AWS.mock( 'S3', 'putObject', ( params, callback ) => {
			isCalled = true;
			callback( null, true );
		} );

		sinon.stub( kue, 'Job', () => {
			return {
				rangeByType ( name, type, from, to, order, cb ) {
					return cb( null, [] );
				}
			};
		} );

		sinon.stub( kue, 'createQueue', () => {
			const queue = {
				'id' : 1,

				process ( name, cb ) {
					cb( job, () => true );
				},

				create () {
					return queue;
				},

				delay () {
					return queue;
				},

				save ( cb ) {
					cb();
					return queue;
				},

				shutdown () {
					return queue;
				},

				on ( errorEvent, cb ) {
					cb( errorEvent );
				}
			};

			return queue;
		} );

		sinon.stub( EventQueue, 'update', () => Promise.resolve( true ) );

		createFile().then( () => {
			expect( isCalled ).to.be.equal( true );
			kue.createQueue.restore();
			kue.Job.restore();
			EventQueue.update.restore();
			AWS.restore( 'S3', 'putObject' );
			done();
		} );
	} );
} );
