'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const {
	Event,
	EventSetting,
	EventDetail,
	EventQueue
} = models;


let eventService;
let jobService;
let data;

before( done => {
	data = {
		'Id' : 1
	};

	eventService = use( 'services/events' );
	jobService   = use( 'services/jobs' );

	done();
} );

describe( 'EventService::deleteById', () => {
	it ( 'should be able to catch 500 server error', done => {
		sinon.stub( Event, 'findById', () => {
			throw new Error( 'Finding Error' );
		} );

		eventService.deleteById( data.Id )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Finding Error' );

				Event.findById.restore();
				done();
			} );
	} );

	it ( 'should be able to return null when resource does not exist', done => {
		sinon.stub( Event, 'findById', () => Promise.resolve( null ) );

		eventService.deleteById( data.Id )
			.then( response => {
				expect( response ).to.be.equal( null );

				Event.findById.restore();
				done();
			} );
	} );

	it( 'should be able to delete', done => {
		sinon.stub( Event, 'findById', () => {
			return Promise.resolve( {
				'Id'         : 1,
				'Name'       : 'MWFB',
				'Location'   : 'Wisconsin',
				'Logo'       : null,
				'CreatedBy'  : 1,
				'UpdatedBy'  : 1,
				'created_at' : '2017-02-06T06:04:00.000Z',
				'updated_at' : '0000-00-00 00:00:00',
				'deleted_at' : null,
				'Users'      : [ ]
			} );
		} );

		sinon.stub( EventSetting, 'findOne', () => {
			const response = {
				'Id'                 : 1,
				'EventId'            : 1,
				'FrequencyTypeId'    : 1,
				'FrequencyTypeValue' : 'Hello',
				'Color'              : 'pink',
				'NoLineTextToExport' : 2,

				destroy () {
					return Promise.resolve( true );
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventDetail, 'findAll', () => {
			const response = {
				'Id'          : 1,
				'EventId'     : 1,
				'Description' : 'some-random-string',
				'CreatedBy'   : 1,
				'UpdatedBy'   : 1,
				'created_at'  : '2017-02-06T05:59:56.000Z',
				'updated_at'  : '0000-00-00 00:00:00'
			};

			return Promise.resolve( [ response ] );
		} );

		sinon.stub( EventDetail, 'destroy', () => Promise.resolve( true ) );

		sinon.stub( EventQueue, 'findAll', () => {
			const response = {
				'Id'       : 1,
				'JobId'    : 925,
				'EventId'  : 1,
				'Duration' : 55987
			};

			return Promise.resolve( [ response ] );
		} );

		sinon.stub( EventQueue, 'destroy', () => Promise.resolve( true ) );
		sinon.stub( jobService, 'deleteById', () => Promise.resolve( true ) );
		sinon.stub( Event, 'destroy', () => Promise.resolve( true ) );

		eventService.deleteById( data.Id )
			.then( response => {
				expect( response ).to.be.an.object();
				expect( response.Operation ).to.exist();

				Event.findById.restore();
				EventSetting.findOne.restore();
				EventDetail.findAll.restore();
				EventDetail.destroy.restore();
				EventQueue.findAll.restore();
				EventQueue.destroy.restore();
				jobService.deleteById.restore();
				Event.destroy.restore();
				done();
			} );
	} );
} );
