'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const EventSetting = models.EventSetting;

let eventService;
let data;

before( done => {
	data = {
		'EventId'            : 1,
		'FrequencyTypeId'    : 1,
		'FrequencyTypeValue' : 'Hello',
		'Color'              : 'pink',
		'NoLineTextToExport' : 2
	};

	eventService = use( 'services/events' );

	done();
} );

describe( 'EventService::createSetting', () => {
	it( 'should be able to return an error', done => {
		sinon.stub( EventSetting, 'create', () => {
			throw new Error( 'Creation Error' );
		} );

		eventService.createSetting( data )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Creation Error' );

				EventSetting.create.restore();
				done();
			} );
	} );

	it( 'should be able to create setting', done => {
		sinon.stub( EventSetting, 'create', () => {
			return Promise.resolve( {
				toJSON () {
					return {
						'Id'                 : 1,
						'EventId'            : 1,
						'FrequencyTypeId'    : 1,
						'FrequencyTypeValue' : 'Hello',
						'Color'              : 'pink',
						'NoLineTextToExport' : 2,
						'CreatedBy'          : 0,
						'UpdatedBy'          : 0,
						'created_at'         : '2017-02-06T06:04:16.000Z',
						'updated_at'         : '2017-02-22T07:13:04.000Z'
					};
				}
			} );
		} );

		eventService.createSetting( data )
			.then( response => {
				expect( response ).to.be.an.object();

				EventSetting.create.restore();
				done();
			} );
	} );
} );
