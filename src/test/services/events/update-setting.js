'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const EventSetting = models.EventSetting;

let eventService;
let data;

before( done => {
	data = {
		'EventId'            : 1,
		'FrequencyTypeId'    : 1,
		'FrequencyTypeValue' : 'Hello',
		'Color'              : 'pink',
		'NoLineTextToExport' : 2
	};

	eventService = use( 'services/events' );

	done();
} );

describe( 'EventService::updateSettingById', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( EventSetting, 'findById', () => {
			throw new Error( 'Error' );
		} );

		eventService.updateSetting( data.Id, data )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				EventSetting.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return null when user does not exist', done => {
		sinon.stub( EventSetting, 'findById', () => Promise.resolve( null ) );

		eventService.updateSetting( data.Id, data )
			.then( response => {
				expect( response ).to.be.equal( null );

				EventSetting.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return resource', done => {
		sinon.stub( EventSetting, 'findById', function () {
			return Promise.resolve( {
				'Id'                 : 1,
				'EventId'            : 1,
				'FrequencyTypeId'    : 1,
				'FrequencyTypeValue' : 'Hello',
				'Color'              : 'pink',
				'NoLineTextToExport' : 2,

				update () {
					return Promise.resolve( {
						toJSON () {
							return data;
						}
					} );
				}
			} );
		} );

		eventService.updateSetting( data.Id, data )
			.then( response => {
				expect( response ).to.be.an.object();

				EventSetting.findById.restore();
				done();
			} );
	} );
} );
