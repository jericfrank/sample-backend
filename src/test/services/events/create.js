'use strict';

require( 'dotenv' ).config();

const _     = require( 'lodash' );
const Code  = require( 'code' );
const Lab   = require( 'lab' );
const kue   = require( 'kue' );
const sinon = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const Event      = models.Event;
const EventQueue = models.EventQueue;

let eventService;
let stationService;
let data;

before( done => {
	data = {
		'StationId'    : 1,
		'EventTypeId'  : 1,
		'Title'        : 'Hello',
		'Start'        : '2017-05-08 13:00:00',
		'End'          : '2017-05-08 14:00:00',
		'TeamOne'      : 'Bulls',
		'TeamOneScore' : 0,
		'TeamTwo'      : 'Heat',
		'TeamTwoScore' : 0,
		'EventSetting' : {
			'FrequencyTypeId'    : 1,
			'FrequencyTypeValue' : 1,
			'Color'              : 'Red',
			'FrequencyType'      : {
				'UnitOfMeasurement' : {
					'Name' : 'hours'
				}
			}
		},
		'EventDetails' : [
			{ 'Description' : 'NBA' },
			{ 'Description' : 'WNBA' }
		]
	};

	eventService   = use( 'services/events' );
	stationService = use( 'services/stations' );

	done();
} );

describe( 'EventService::create', () => {
	it( 'should be able to return an error', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			throw new Error( 'Error' );
		} );

		eventService.create( data )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				Event.findUnderlap.restore();
				done();
			} );
	} );

	it( 'should be able to return overlapping bad request when date pass is overlapping some registered date range', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [ {} ] );
		} );

		eventService.create( data )
			.then( response => {
				expect( response ).to.be.an.error();
				Event.findUnderlap.restore();
				done();
			} );
	} );

	it( 'should be able to return overlapping bad request when start date pass is greater than registed start date and end date is greater than reg end date', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [ {} ] );
		} );

		eventService.create( data )
			.then( response => {
				expect( response ).to.be.an.error();
				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				done();
			} );
	} );

	it( 'should be able to create event there is no details', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'create', () => {
			const response = {
				'CreatedBy'    : 0,
				'UpdatedBy'    : 0,
				'Id'           : 2,
				'StationId'    : 1,
				'EventTypeId'  : 1,
				'Title'        : 'Hello World',
				'Start'        : '2017-03-15T00:00:00.000Z',
				'End'          : '2017-03-15T00:00:00.000Z',
				'TeamOne'      : 'Bulls',
				'TeamOneScore' : 0,
				'TeamTwo'      : 'Heat',
				'TeamTwoScore' : 0,
				'updated_at'   : '2017-03-15T07:25:16.000Z',
				'created_at'   : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				},

				update () {
					return Promise.resolve( response );
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( stationService, 'getById', () => Promise.resolve( {
			'Id'              : 52,
			'Name'            : 'yesFM',
			'Location'        : 'L.A gun',
			'Logo'            : null,
			'CreatedBy'       : 0,
			'UpdatedBy'       : 0,
			'created_at'      : '2017-03-15T00:09:07.000Z',
			'updated_at'      : '2017-03-15T00:09:07.000Z',
			'CalendarSetting' : {
				'Id'              : 3,
				'StationId'       : 52,
				'StorageLocation' : 'yesfm-1489536547',
				'StartOfWeek'     : 1,
				'CreatedBy'       : 0,
				'UpdatedBy'       : 0,
				'created_at'      : '2017-03-15T00:09:09.000Z',
				'updated_at'      : '2017-03-15T00:09:09.000Z'
			}
		} ) );

		sinon.stub( eventService, 'createSetting', () => Promise.resolve( 1 ) );

		sinon.stub( eventService, 'getSettingByEventId', () => {
			return Promise.resolve( {
				'Id'                 : 55,
				'EventId'            : 43,
				'FrequencyTypeId'    : 1,
				'FrequencyTypeValue' : 1,
				'Color'              : 'Red',
				'NoLineTextToExport' : 0,
				'CreatedBy'          : 0,
				'UpdatedBy'          : 0,
				'FrequencyType'      : {
					'Id'                : 1,
					'Name'              : 'immediate',
					'UnitOfMeasurement' : null,
					'CreatedBy'         : 1,
					'UpdatedBy'         : 1
				}
			} );
		} );

		sinon.stub( kue, 'createQueue', () => {
			const queue = {
				'id' : 1,

				create () {
					return queue;
				},

				delay () {
					return queue;
				},

				save ( cb ) {
					cb();
					return queue;
				},

				shutdown () {
					return queue;
				}
			};

			return queue;
		} );

		sinon.stub( EventQueue, 'create', () => Promise.resolve() );

		const newData = _.omit( data, 'EventDetails' );
		eventService.create( newData )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				Event.create.restore();
				stationService.getById.restore();
				eventService.createSetting.restore();
				eventService.getSettingByEventId.restore();
				kue.createQueue.restore();
				EventQueue.create.restore();
				done();
			} );
	} );

	it( 'should be able to create', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'create', () => {
			const response = {
				'CreatedBy'    : 0,
				'UpdatedBy'    : 0,
				'Id'           : 2,
				'StationId'    : 1,
				'EventTypeId'  : 1,
				'Title'        : 'Hello World',
				'Start'        : '2017-03-15T00:00:00.000Z',
				'End'          : '2017-03-15T00:00:00.000Z',
				'TeamOne'      : 'Bulls',
				'TeamOneScore' : 0,
				'TeamTwo'      : 'Heat',
				'TeamTwoScore' : 0,
				'updated_at'   : '2017-03-15T07:25:16.000Z',
				'created_at'   : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				},

				update () {
					return Promise.resolve( response );
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( stationService, 'getById', () => Promise.resolve( {
			'Id'              : 52,
			'Name'            : 'yesFM',
			'Location'        : 'L.A gun',
			'Logo'            : null,
			'CreatedBy'       : 0,
			'UpdatedBy'       : 0,
			'created_at'      : '2017-03-15T00:09:07.000Z',
			'updated_at'      : '2017-03-15T00:09:07.000Z',
			'CalendarSetting' : {
				'Id'              : 3,
				'StationId'       : 52,
				'StorageLocation' : 'greasy-dev/yesfm-1489536547/',
				'StartOfWeek'     : 1,
				'CreatedBy'       : 0,
				'UpdatedBy'       : 0,
				'created_at'      : '2017-03-15T00:09:09.000Z',
				'updated_at'      : '2017-03-15T00:09:09.000Z'
			}
		} ) );

		sinon.stub( eventService, 'createSetting', () => Promise.resolve( 1 ) );

		sinon.stub( eventService, 'getSettingByEventId', () => {
			return Promise.resolve( {
				'Id'                 : 55,
				'EventId'            : 43,
				'FrequencyTypeId'    : 1,
				'FrequencyTypeValue' : 1,
				'Color'              : 'Red',
				'NoLineTextToExport' : 0,
				'CreatedBy'          : 0,
				'UpdatedBy'          : 0,
				'FrequencyType'      : {
					'Id'                : 1,
					'Name'              : 'immediate',
					'UnitOfMeasurement' : null,
					'CreatedBy'         : 1,
					'UpdatedBy'         : 1
				}
			} );
		} );

		sinon.stub( eventService, 'createDetails', () => {
			return Promise.resolve( [
				{
					'id'         : null,
					'ed_e_id'    : 43,
					'ed_e_desc'  : 'NBA',
					'created_by' : 0,
					'updated_by' : 0,
					'created_at' : '2017-03-23T01:06:33.000Z',
					'updated_at' : '2017-03-23T01:06:33.000Z'
				},
				{
					'id'         : null,
					'ed_e_id'    : 43,
					'ed_e_desc'  : 'WNBA',
					'created_by' : 0,
					'updated_by' : 0,
					'created_at' : '2017-03-23T01:06:33.000Z',
					'updated_at' : '2017-03-23T01:06:33.000Z'
				}
			] );
		} );

		sinon.stub( kue, 'createQueue', () => {
			const queue = {
				create () {
					return queue;
				},

				delay () {
					return queue;
				},

				save ( cb ) {
					cb();
					return queue;
				},

				shutdown () {
					return queue;
				}
			};

			return queue;
		} );

		sinon.stub( EventQueue, 'create', () => Promise.resolve() );

		data.EventSetting.FrequencyType.UnitOfMeasurement = {
			'Name' : null
		};
		eventService.create( data )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				Event.create.restore();
				stationService.getById.restore();
				eventService.createSetting.restore();
				eventService.getSettingByEventId.restore();
				eventService.createDetails.restore();
				kue.createQueue.restore();
				EventQueue.create.restore();
				done();
			} );
	} );
} );
