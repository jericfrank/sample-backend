'use strict';

require( 'dotenv' ).config();

const _      = require( 'lodash' );
const Code   = require( 'code' );
const Lab    = require( 'lab' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const models = use( 'models' );
const log    = use( 'logging' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

const ValidationError = models.Sequelize.ValidationError;
const Event           = models.Event;
const EventRecurrence = models.EventRecurrence;

let eventService;
let data;

before( done => {
	data = {
		'Id'           : 1,
		'StationId'    : 69,
		'EventTypeId'  : 1,
		'Title'        : 'NBA Recap: GSW vs Spurs',
		'Start'        : moment().add( 1, 'h' ).format(),
		'End'          : moment().add( 2, 'h' ).format(),
		'EventSetting' : {
			'FrequencyTypeId'    : 1,
			'FrequencyTypeValue' : 1,
			'Color'              : 'red'
		},

		'EventDetails' : [
			{ 'Description' : 'GSW win in Game 1' }
		]
	};

	eventService = use( 'services/events' );

	sinon.stub( log, 'info' ).returns( '' );

	done();
} );

after( done => {
	log.info.restore();
	done();
} );

describe( 'EventService::updateWithRecurrence', () => {
	it( 'should be able to return an error', done => {
		sinon.stub( Event, 'findAll', () => {
			throw new Error( 'Error' );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 1,
				'RepeatEvery'  : 1,
				'Start'        : '2017-05-15',
				'End'          : '2017-05-19'
			}
		} );

		eventService.updateWithRecurrence( data.Id, recurrence )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				Event.findAll.restore();
				done();
			} );
	} );

	it( 'should be able to throw an step error when it cannot identify type of reccuring', done => {
		sinon.stub( Event, 'findAll', () => {
			return Promise.resolve( [ {} ] );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 3,
				'DayOption'    : 1,
				'RepeatEvery'  : 1,
				'Start'        : '2017-05-15',
				'End'          : '2017-05-19'
			}
		} );

		eventService.updateWithRecurrence( data.Id, recurrence )
			.catch( response => {
				expect( response ).to.be.an.error( Error );

				Event.findAll.restore();
				done();
			} );
	} );

	it( 'should be able to return 404 when this recurring event does not exists', done => {
		sinon.stub( Event, 'findAll', () => {
			return Promise.resolve( [] );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 1,
				'RepeatEvery'  : 1,
				'Start'        : '2017-05-15',
				'End'          : '2017-05-19'
			}
		} );

		eventService.updateWithRecurrence( data.Id, recurrence )
			.then( response => {
				expect( response ).to.be.equal( null );

				Event.findAll.restore();
				done();
			} );
	} );

	it( 'should be able to return overlapping bad request when date pass is overlapping some registered date range', done => {
		sinon.stub( Event, 'findAll', () => {
			return Promise.resolve( [ {
				'Id'           : 1,
				'Start'        : moment.utc().add( 1, 'h' ).format(),
				'EventSetting' : {
					'Id' : 1
				}
			}, {
				'Id'           : 2,
				'Start'        : moment.utc().add( 2, 'days' ).format(),
				'EventSetting' : {
					'Id' : 1
				}
			} ] );
		} );

		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [ {} ] );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 1,
				'RepeatEvery'  : 1,
				'Start'        : moment.utc().add( 1, 'h' ).format(),
				'End'          : moment.utc().add( 3, 'days' ).format()
			}
		} );

		eventService.updateWithRecurrence( data.Id, recurrence )
			.then( response => {
				expect( response ).to.be.an.error();
				Event.findAll.restore();
				Event.findUnderlap.restore();
				done();
			} );
	} );

	it( 'should be able to return overlapping bad request when start date pass is greater than registed start date and end date is greater than reg end date', done => {
		sinon.stub( Event, 'findAll', () => {
			return Promise.resolve( [ {
				'Id'           : 1,
				'Start'        : moment.utc().add( 1, 'h' ).format(),
				'EventSetting' : {
					'Id' : 1
				}
			}, {
				'Id'           : 2,
				'Start'        : moment.utc().add( 2, 'days' ).format(),
				'EventSetting' : {
					'Id' : 1
				}
			} ] );
		} );

		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [ {} ] );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 1,
				'RepeatEvery'  : 1,
				'Start'        : moment.utc().add( 1, 'h' ).format(),
				'End'          : moment.utc().add( 3, 'days' ).format()
			}
		} );

		eventService.updateWithRecurrence( Event.Id, recurrence )
			.then( response => {
				expect( response ).to.be.an.error();
				Event.findAll.restore();
				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				done();
			} );
	} );

	it( 'should be able to throw bad request when there is an error inside execute function', done => {
		sinon.stub( Event, 'findAll', () => {
			return Promise.resolve( [ {
				'Id'           : 1,
				'Start'        : '2017-05-15',
				'EventSetting' : {
					'Id' : 1
				}
			}, {
				'Id'           : 2,
				'Start'        : '2017-05-16',
				'EventSetting' : {
					'Id' : 1
				}
			} ] );
		} );

		sinon.stub( eventService, 'updateById', () => {
			return Promise.resolve();
		} );

		sinon.stub( Event, 'findById', () => {
			const response =  {
				'Id'           : 1,
				'Start'        : '2017-05-15',
				'EventSetting' : {
					'Id' : 1
				},

				update () {
					return Promise.resolve( response );
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventRecurrence, 'upsert', () => {
			return Promise.resolve();
		} );

		sinon.stub( eventService, 'deleteById', () => {
			return Promise.resolve( new ValidationError( 'ValidationError', [ {
				'message' : 'error occured.'
			} ] ) );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 1,
				'RepeatEvery'  : 1,
				'Start'        : moment.utc().format(),
				'End'          : moment.utc().format()
			}
		} );

		eventService.updateWithRecurrence( data.Id, recurrence )
			.then( response => {
				expect( response ).to.be.an.error();
				Event.findAll.restore();
				eventService.updateById.restore();
				eventService.deleteById.restore();
				Event.findById.restore();
				EventRecurrence.upsert.restore();
				done();
			} );
	} );

	it( 'should be able to update', done => {
		sinon.stub( Event, 'findAll', () => {
			return Promise.resolve( [ {
				'Id'           : 1,
				'Start'        : '2017-05-15',
				'EventSetting' : {
					'Id' : 1
				}
			}, {
				'Id'           : 2,
				'Start'        : '2017-05-16',
				'EventSetting' : {
					'Id' : 1
				}
			} ] );
		} );

		sinon.stub( eventService, 'updateById', () => {
			return Promise.resolve();
		} );

		sinon.stub( Event, 'findById', () => {
			const response =  {
				'Id'           : 1,
				'Start'        : '2017-05-15',
				'EventSetting' : {
					'Id' : 1
				},

				update () {
					return Promise.resolve( response );
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventRecurrence, 'upsert', () => {
			return Promise.resolve();
		} );

		sinon.stub( eventService, 'deleteById', () => {
			return Promise.resolve( {
				'Operation' : true
			} );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 1,
				'RepeatEvery'  : 1,
				'Start'        : moment.utc().format(),
				'End'          : moment.utc().format()
			}
		} );

		eventService.updateWithRecurrence( data.Id, recurrence )
			.then( response => {
				expect( response ).to.be.an.object();
				Event.findAll.restore();
				eventService.updateById.restore();
				eventService.deleteById.restore();
				Event.findById.restore();
				EventRecurrence.upsert.restore();
				done();
			} );
	} );

	it( 'should be able to update and create', done => {
		sinon.stub( Event, 'findAll', () => {
			return Promise.resolve( [ {
				'Id'           : 1,
				'Start'        : '2017-05-15',
				'EventSetting' : {
					'Id' : 1
				}
			}, {
				'Id'           : 2,
				'Start'        : '2017-05-16',
				'EventSetting' : {
					'Id' : 1
				}
			} ] );
		} );

		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'updateById', () => {
			return Promise.resolve();
		} );

		sinon.stub( eventService, 'create', () => {
			return Promise.resolve();
		} );

		sinon.stub( Event, 'findById', () => {
			return Promise.resolve( null );
		} );

		sinon.stub( EventRecurrence, 'upsert', () => {
			return Promise.resolve();
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 1,
				'RepeatEvery'  : 1,
				'Start'        : moment.utc().format(),
				'End'          : moment.utc().format()
			}
		} );

		eventService.updateWithRecurrence( data.Id, recurrence )
			.then( response => {
				expect( response ).to.be.an.object();
				Event.findAll.restore();
				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.updateById.restore();
				eventService.create.restore();
				Event.findById.restore();
				EventRecurrence.upsert.restore();
				done();
			} );
	} );

	it( 'should be able to update and create', done => {
		sinon.stub( Event, 'findAll', () => {
			return Promise.resolve( [ {
				'Id'           : 1,
				'Start'        : '2017-05-15',
				'EventSetting' : {
					'Id' : 1
				}
			}, {
				'Id'           : 2,
				'Start'        : '2017-05-16',
				'EventSetting' : {
					'Id' : 1
				}
			} ] );
		} );

		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'updateById', () => {
			return Promise.resolve();
		} );

		sinon.stub( eventService, 'create', () => {
			return Promise.resolve();
		} );

		sinon.stub( eventService, 'deleteById', () => {
			return Promise.resolve();
		} );

		sinon.stub( Event, 'findById', () => {
			return Promise.resolve( null );
		} );

		sinon.stub( EventRecurrence, 'upsert', () => {
			return Promise.resolve();
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 1,
				'RepeatEvery'  : 1,
				'Start'        : '2017-05-16',
				'End'          : '2017-05-17'
			}
		} );

		eventService.updateWithRecurrence( data.Id, recurrence )
			.then( response => {
				expect( response ).to.be.an.object();
				Event.findAll.restore();
				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.updateById.restore();
				eventService.create.restore();
				eventService.deleteById.restore();
				Event.findById.restore();
				EventRecurrence.upsert.restore();
				done();
			} );
	} );
} );
