'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const Event = models.Event;

let eventService;
before( done => {
	eventService = use( 'services/events' );

	done();
} );

describe( 'EventService::getAll', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( Event, 'findAll', () => {
			throw new Error( 'Finding Error' );
		} );

		eventService.getAll()
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Finding Error' );

				Event.findAll.restore();
				done();
			} );
	} );

	it ( 'should be able to fetch resource by month range and station id', done => {
		sinon.stub( Event, 'findAll', () => {
			const response = {
				'CreatedBy'    : 0,
				'UpdatedBy'    : 0,
				'Id'           : 2,
				'StationId'    : 1,
				'EventTypeId'  : 1,
				'Title'        : 'Hello World',
				'Start'        : '2014-03-15T00:00:00.000Z',
				'End'          : '2015-03-15T00:00:00.000Z',
				'TeamOne'      : 'Bulls',
				'TeamOneScore' : 0,
				'TeamTwo'      : 'Heat',
				'TeamTwoScore' : 0,
				'updated_at'   : '2017-03-15T07:25:16.000Z',
				'created_at'   : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( [ response ] );
		} );

		eventService.getAll( '2014-03-15T00:00:00.000Z', '2016-03-15T00:00:00.000Z', 1 )
			.then( response => {
				expect( response ).to.be.an.array();

				Event.findAll.restore();
				done();
			} );
	} );

	it ( 'should be able to fetch all resource', done => {
		sinon.stub( Event, 'findAll', () => {
			const response = {
				'CreatedBy'    : 0,
				'UpdatedBy'    : 0,
				'Id'           : 2,
				'StationId'    : 1,
				'EventTypeId'  : 1,
				'Title'        : 'Hello World',
				'Start'        : '2017-03-15T00:00:00.000Z',
				'End'          : '2017-03-15T00:00:00.000Z',
				'TeamOne'      : 'Bulls',
				'TeamOneScore' : 0,
				'TeamTwo'      : 'Heat',
				'TeamTwoScore' : 0,
				'updated_at'   : '2017-03-15T07:25:16.000Z',
				'created_at'   : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( [ response ] );
		} );

		eventService.getAll()
			.then( response => {
				expect( response ).to.be.an.array();

				Event.findAll.restore();
				done();
			} );
	} );
} );
