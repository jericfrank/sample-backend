'use strict';

require( 'dotenv' ).config();

const _      = require( 'lodash' );
const Code   = require( 'code' );
const Lab    = require( 'lab' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const Event           = models.Event;
const EventRecurrence = models.EventRecurrence;

let eventService;
let data;

before( done => {
	data = {
		'StationId'    : 69,
		'EventTypeId'  : 1,
		'Title'        : 'NBA Recap: GSW vs Spurs',
		'Start'        : moment.utc().add( 1, 'h' ).format(),
		'End'          : moment.utc().add( 2, 'h' ).format(),
		'EventSetting' : {
			'FrequencyTypeId'    : 1,
			'FrequencyTypeValue' : 1,
			'Color'              : 'red'
		},

		'EventDetails' : [
			{ 'Description' : 'GSW win in Game 1' }
		]
	};

	eventService = use( 'services/events' );

	done();
} );

describe( 'EventService::createWithRecurrence', () => {
	it( 'should be able to return an error', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'create', () => {
			throw new Error( 'Error' );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 1,
				'RepeatEvery'  : 1,
				'Start'        : '2017-05-15',
				'End'          : '2017-05-19'
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.create.restore();
				done();
			} );
	} );

	it( 'should be able to return overlapping bad request when date pass is overlapping some registered date range', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [ {} ] );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 1,
				'RepeatEvery'  : 1,
				'Start'        : moment.utc().add( 2, 'h' ).format(),
				'End'          : moment.utc().add( 3, 'h' ).format()
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.then( response => {
				expect( response ).to.be.an.error();
				Event.findUnderlap.restore();
				done();
			} );
	} );

	it( 'should be able to return overlapping bad request when start date pass is greater than registed start date and end date is greater than reg end date', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [ {} ] );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 1,
				'RepeatEvery'  : 1,
				'Start'        : moment.utc().add( 2, 'h' ).format(),
				'End'          : moment.utc().add( 3, 'h' ).format()
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.then( response => {
				expect( response ).to.be.an.error();
				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				done();
			} );
	} );

	it( 'should be able to create event using `DAILY_WITH_REPEATEVERY_AND_END`', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'create', () => {
			const response = {
				'CreatedBy'   : 0,
				'UpdatedBy'   : 0,
				'Id'          : 2,
				'StationId'   : 69,
				'EventTypeId' : 1,
				'Title'       : 'NBA Recap: GSW vs Spurs',
				'Start'       : '2017-03-15T00:00:00.000Z',
				'End'         : '2017-03-15T00:00:00.000Z',
				'updated_at'  : '2017-03-15T07:25:16.000Z',
				'created_at'  : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventRecurrence, 'create', () => {
			return Promise.resolve( {} );
		} );

		sinon.stub( Event, 'upsert', () => {
			return Promise.resolve( {} );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 1,
				'RepeatEvery'  : 1,
				'Start'        : '2017-05-15',
				'End'          : '2017-05-19'
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.create.restore();
				EventRecurrence.create.restore();
				Event.upsert.restore();
				done();
			} );
	} );

	it( 'should be able to create event using `DAILY_WITH_REPEATEVERY_AND_ENDAFTER`', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'create', () => {
			const response = {
				'CreatedBy'   : 0,
				'UpdatedBy'   : 0,
				'Id'          : 2,
				'StationId'   : 69,
				'EventTypeId' : 1,
				'Title'       : 'NBA Recap: GSW vs Spurs',
				'Start'       : '2017-03-15T00:00:00.000Z',
				'End'         : '2017-03-15T00:00:00.000Z',
				'updated_at'  : '2017-03-15T07:25:16.000Z',
				'created_at'  : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventRecurrence, 'create', () => {
			return Promise.resolve( {} );
		} );

		sinon.stub( Event, 'upsert', () => {
			return Promise.resolve( {} );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 1,
				'RepeatEvery'  : 1,
				'Start'        : '2017-05-15',
				'EndAfter'     : 5
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.create.restore();
				EventRecurrence.create.restore();
				Event.upsert.restore();
				done();
			} );
	} );

	it( 'should be able to create event using `DAILY_WITH_WEEKDAY_AND_END`', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'create', () => {
			const response = {
				'CreatedBy'   : 0,
				'UpdatedBy'   : 0,
				'Id'          : 2,
				'StationId'   : 69,
				'EventTypeId' : 1,
				'Title'       : 'NBA Recap: GSW vs Spurs',
				'Start'       : '2017-03-15T00:00:00.000Z',
				'End'         : '2017-03-15T00:00:00.000Z',
				'updated_at'  : '2017-03-15T07:25:16.000Z',
				'created_at'  : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventRecurrence, 'create', () => {
			return Promise.resolve( {} );
		} );

		sinon.stub( Event, 'upsert', () => {
			return Promise.resolve( {} );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 2,
				'RepeatEvery'  : 1,
				'Start'        : '2017-05-15',
				'End'          : '2017-06-02'
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.create.restore();
				EventRecurrence.create.restore();
				Event.upsert.restore();
				done();
			} );
	} );

	it( 'should be able to create event using `DAILY_WITH_WEEKDAY_AND_ENDAFTER`', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'create', () => {
			const response = {
				'CreatedBy'   : 0,
				'UpdatedBy'   : 0,
				'Id'          : 2,
				'StationId'   : 69,
				'EventTypeId' : 1,
				'Title'       : 'NBA Recap: GSW vs Spurs',
				'Start'       : '2017-03-15T00:00:00.000Z',
				'End'         : '2017-03-15T00:00:00.000Z',
				'updated_at'  : '2017-03-15T07:25:16.000Z',
				'created_at'  : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventRecurrence, 'create', () => {
			return Promise.resolve( {} );
		} );

		sinon.stub( Event, 'upsert', () => {
			return Promise.resolve( {} );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 1,
				'DayOption'    : 2,
				'RepeatEvery'  : 1,
				'Start'        : '2017-05-15',
				'EndAfter'     : 5
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.create.restore();
				EventRecurrence.create.restore();
				Event.upsert.restore();
				done();
			} );
	} );

	it( 'should be able to create event using `WEEKLY_WITH_RECUR_AND_END`', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'create', () => {
			const response = {
				'CreatedBy'   : 0,
				'UpdatedBy'   : 0,
				'Id'          : 2,
				'StationId'   : 69,
				'EventTypeId' : 1,
				'Title'       : 'NBA Recap: GSW vs Spurs',
				'Start'       : '2017-03-15T00:00:00.000Z',
				'End'         : '2017-03-15T00:00:00.000Z',
				'updated_at'  : '2017-03-15T07:25:16.000Z',
				'created_at'  : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventRecurrence, 'create', () => {
			return Promise.resolve( {} );
		} );

		sinon.stub( Event, 'upsert', () => {
			return Promise.resolve( {} );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 2,
				'RepeatEvery'  : 2,
				'Weekdays'     : [ 1, 2 ],
				'Start'        : '2017-05-15',
				'End'          : '2017-06-02'
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.create.restore();
				EventRecurrence.create.restore();
				Event.upsert.restore();
				done();
			} );
	} );

	it( 'should be able to create event using `WEEKLY_WITH_RECUR_AND_ENDAFTER`', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'create', () => {
			const response = {
				'CreatedBy'   : 0,
				'UpdatedBy'   : 0,
				'Id'          : 2,
				'StationId'   : 69,
				'EventTypeId' : 1,
				'Title'       : 'NBA Recap: GSW vs Spurs',
				'Start'       : '2017-03-15T00:00:00.000Z',
				'End'         : '2017-03-15T00:00:00.000Z',
				'updated_at'  : '2017-03-15T07:25:16.000Z',
				'created_at'  : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventRecurrence, 'create', () => {
			return Promise.resolve( {} );
		} );

		sinon.stub( Event, 'upsert', () => {
			return Promise.resolve( {} );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 2,
				'RepeatEvery'  : 2,
				'Weekdays'     : [ 1, 2 ],
				'Start'        : '2017-05-15',
				'EndAfter'     : 5
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.create.restore();
				EventRecurrence.create.restore();
				Event.upsert.restore();
				done();
			} );
	} );

	it( 'should be able to create event using `MONTHLY_RECUR_ONE_DAY_A_MONTH_WITH_END`', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'create', () => {
			const response = {
				'CreatedBy'   : 0,
				'UpdatedBy'   : 0,
				'Id'          : 2,
				'StationId'   : 69,
				'EventTypeId' : 1,
				'Title'       : 'NBA Recap: GSW vs Spurs',
				'Start'       : '2017-03-15T00:00:00.000Z',
				'End'         : '2017-03-15T00:00:00.000Z',
				'updated_at'  : '2017-03-15T07:25:16.000Z',
				'created_at'  : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventRecurrence, 'create', () => {
			return Promise.resolve( {} );
		} );

		sinon.stub( Event, 'upsert', () => {
			return Promise.resolve( {} );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 3,
				'RepeatEvery'  : 1,
				'Start'        : '2017-06-01',
				'End'          : '2017-09-30',
				'MonthOption'  : 1,
				'Day'          : 11
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.create.restore();
				EventRecurrence.create.restore();
				Event.upsert.restore();
				done();
			} );
	} );

	it( 'should be able to create event using `MONTHLY_RECUR_ONE_DAY_A_MONTH_WITH_ENDAFTER`', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'create', () => {
			const response = {
				'CreatedBy'   : 0,
				'UpdatedBy'   : 0,
				'Id'          : 2,
				'StationId'   : 69,
				'EventTypeId' : 1,
				'Title'       : 'NBA Recap: GSW vs Spurs',
				'Start'       : '2017-03-15T00:00:00.000Z',
				'End'         : '2017-03-15T00:00:00.000Z',
				'updated_at'  : '2017-03-15T07:25:16.000Z',
				'created_at'  : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventRecurrence, 'create', () => {
			return Promise.resolve( {} );
		} );

		sinon.stub( Event, 'upsert', () => {
			return Promise.resolve( {} );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption' : 3,
				'RepeatEvery'  : 1,
				'Start'        : '2017-06-01',
				'EndAfter'     : 5,
				'MonthOption'  : 1,
				'Day'          : 11
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.create.restore();
				EventRecurrence.create.restore();
				Event.upsert.restore();
				done();
			} );
	} );

	it( 'should be able to create event using `MONTHLY_RECUR_ONE_DAY_ON_EVERY_N_MONTH_WITH_END`', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'create', () => {
			const response = {
				'CreatedBy'   : 0,
				'UpdatedBy'   : 0,
				'Id'          : 2,
				'StationId'   : 69,
				'EventTypeId' : 1,
				'Title'       : 'NBA Recap: GSW vs Spurs',
				'Start'       : '2017-03-15T00:00:00.000Z',
				'End'         : '2017-03-15T00:00:00.000Z',
				'updated_at'  : '2017-03-15T07:25:16.000Z',
				'created_at'  : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventRecurrence, 'create', () => {
			return Promise.resolve( {} );
		} );

		sinon.stub( Event, 'upsert', () => {
			return Promise.resolve( {} );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption'       : 3,
				'RepeatEvery'        : 1,
				'Start'              : '2017-06-01',
				'End'                : '2017-09-30',
				'MonthOption'        : 2,
				'MonthWeekOption'    : 5,
				'MonthWeekdayOption' : 1
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.create.restore();
				EventRecurrence.create.restore();
				Event.upsert.restore();
				done();
			} );
	} );

	it( 'should be able to create event using `MONTHLY_RECUR_ONE_DAY_ON_EVERY_N_MONTH_WITH_END` with month week option set to first', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'create', () => {
			const response = {
				'CreatedBy'   : 0,
				'UpdatedBy'   : 0,
				'Id'          : 2,
				'StationId'   : 69,
				'EventTypeId' : 1,
				'Title'       : 'NBA Recap: GSW vs Spurs',
				'Start'       : '2017-03-15T00:00:00.000Z',
				'End'         : '2017-03-15T00:00:00.000Z',
				'updated_at'  : '2017-03-15T07:25:16.000Z',
				'created_at'  : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventRecurrence, 'create', () => {
			return Promise.resolve( {} );
		} );

		sinon.stub( Event, 'upsert', () => {
			return Promise.resolve( {} );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption'       : 3,
				'RepeatEvery'        : 1,
				'Start'              : '2017-06-01',
				'End'                : '2017-09-30',
				'MonthOption'        : 2,
				'MonthWeekOption'    : 1,
				'MonthWeekdayOption' : 1
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.create.restore();
				EventRecurrence.create.restore();
				Event.upsert.restore();
				done();
			} );
	} );

	it( 'should be able to create event using `MONTHLY_RECUR_ONE_DAY_ON_EVERY_N_MONTH_WITH_ENDAFTER`', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'create', () => {
			const response = {
				'CreatedBy'   : 0,
				'UpdatedBy'   : 0,
				'Id'          : 2,
				'StationId'   : 69,
				'EventTypeId' : 1,
				'Title'       : 'NBA Recap: GSW vs Spurs',
				'Start'       : '2017-03-15T00:00:00.000Z',
				'End'         : '2017-03-15T00:00:00.000Z',
				'updated_at'  : '2017-03-15T07:25:16.000Z',
				'created_at'  : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventRecurrence, 'create', () => {
			return Promise.resolve( {} );
		} );

		sinon.stub( Event, 'upsert', () => {
			return Promise.resolve( {} );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption'        : 3,
				'RepeatEvery'         : 1,
				'Start'               : '2017-06-01',
				'EndAfter'            : 5,
				'MonthOption'         : 2,
				'MonthWeekOption'     : 5,
				'MonthWeekdaysOption' : 1
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.create.restore();
				EventRecurrence.create.restore();
				Event.upsert.restore();
				done();
			} );
	} );

	it( 'should be able to create event using `MONTHLY_RECUR_ONE_DAY_ON_EVERY_N_MONTH_WITH_END` with month week option set to first', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( eventService, 'create', () => {
			const response = {
				'CreatedBy'   : 0,
				'UpdatedBy'   : 0,
				'Id'          : 2,
				'StationId'   : 69,
				'EventTypeId' : 1,
				'Title'       : 'NBA Recap: GSW vs Spurs',
				'Start'       : '2017-03-15T00:00:00.000Z',
				'End'         : '2017-03-15T00:00:00.000Z',
				'updated_at'  : '2017-03-15T07:25:16.000Z',
				'created_at'  : '2017-03-15T07:25:16.000Z',

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( EventRecurrence, 'create', () => {
			return Promise.resolve( {} );
		} );

		sinon.stub( Event, 'upsert', () => {
			return Promise.resolve( {} );
		} );

		const recurrence = _.assign( {}, data, {
			'EventRecurrence' : {
				'RepeatOption'        : 3,
				'RepeatEvery'         : 1,
				'Start'               : '2017-06-01',
				'EndAfter'            : 5,
				'MonthOption'         : 2,
				'MonthWeekOption'     : 1,
				'MonthWeekdaysOption' : 1
			}
		} );

		eventService.createWithRecurrence( recurrence )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				eventService.create.restore();
				EventRecurrence.create.restore();
				Event.upsert.restore();
				done();
			} );
	} );
} );
