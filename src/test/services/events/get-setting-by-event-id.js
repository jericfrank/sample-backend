'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const EventSetting = models.EventSetting;

let eventService;
let data;

before( done => {
	data = {
		'Id' : 1
	};

	eventService = use( 'services/events' );

	done();
} );

describe( 'EventService::getSettingByEventId', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( EventSetting, 'findOne', () => {
			throw new Error( 'Error' );
		} );

		eventService.getSettingByEventId( data.Id )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				EventSetting.findOne.restore();
				done();
			} );
	} );

	it( 'should be able to return null when user does not exist', done => {
		sinon.stub( EventSetting, 'findOne', () => Promise.resolve( null ) );

		eventService.getSettingByEventId( data.Id )
			.then( response => {
				expect( response ).to.be.equal( null );

				EventSetting.findOne.restore();
				done();
			} );
	} );

	it( 'should be able to return resource', done => {
		sinon.stub( EventSetting, 'findOne', () => {
			return Promise.resolve( {
				toJSON () {
					return {
						'CreatedBy'          : 0,
						'UpdatedBy'          : 0,
						'Id'                 : 0,
						'EventId'            : 0,
						'FrequencyTypeId'    : 0,
						'FrequencyTypeValue' : 0,
						'Color'              : 'string',
						'NoLineTextToExport' : 0,
						'created_at'         : '2017-02-06T06:04:00.000Z',
						'updated_at'         : '0000-00-00 00:00:00'
					};
				}
			} );
		} );

		eventService.getSettingByEventId( data.Id )
			.then( response => {
				expect( response ).to.be.an.object();

				EventSetting.findOne.restore();
				done();
			} );
	} );
});
