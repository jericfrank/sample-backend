'use strict';

require( 'dotenv' ).config();

const _      = require( 'lodash' );
const Code   = require( 'code' );
const Lab    = require( 'lab' );
const kue    = require( 'kue' );
const sinon  = require( 'sinon' );
const moment = require( 'moment' );

const models = use( 'models' );
const log    = use( 'logging' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

const Event      = models.Event;
const EventQueue = models.EventQueue;

let eventService;
let stationService;
let data;

before( done => {
	data = {
		'Id'           : 1,
		'StationId'    : 1,
		'Title'        : 1,
		'Start'        : moment.utc().format(),
		'End'          : moment.utc().add( 1, 'h' ).format(),
		'TeamOne'      : 'bulls',
		'TeamOneScore' : 0,
		'TeamTwo'      : 'heat',
		'TeamTwoScore' : 0,
		'EventSetting' : {
			'FrequencyTypeId'    : 1,
			'FrequencyTypeValue' : 1,
			'Color'              : 'Red',
			'FrequencyType'      : {
				'UnitOfMeasurement' : {
					'Name' : 'hours'
				}
			}
		},

		'EventDetails' : [ {
			'Description' : 'Hello world'
		} ]
	};

	eventService   = use( 'services/events' );
	stationService = use( 'services/stations' );

	sinon.stub( log, 'fatal' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	done();
} );

describe( 'EventService::updateById', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			throw new Error( 'Error' );
		} );

		eventService.updateById( data.Id, data )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				Event.findUnderlap.restore();
				done();
			} );
	} );

	it( 'should be able to throw bad request when date is past date', done => {
		data.End = moment.utc().subtract( 2, 'h' ).format();
		eventService.updateById( data.Id, data )
			.then( response => {
				expect( response ).to.be.an.error();
				done();
			} );
	} );

	it( 'should be able to return overlapping bad request when date pass is overlapping some registered date range', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [ {} ] );
		} );

		data.End = moment.utc().add( 4, 'h' ).format();
		eventService.updateById( data.Id, data )
			.then( response => {
				expect( response ).to.be.an.error();
				Event.findUnderlap.restore();
				done();
			} );
	} );

	it( 'should be able to return overlapping bad request when start date pass is greater than registed start date and end date is greater than reg end date', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [ {} ] );
		} );

		eventService.updateById( data.Id, data )
			.then( response => {
				expect( response ).to.be.an.error();
				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				done();
			} );
	} );

	it( 'should be able to return null when resource does not exist', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findById', () => Promise.resolve( null ) );

		data.End = moment.utc().add( 4, 'h' ).format();
		eventService.updateById( data.Id, data )
			.then( response => {
				expect( response ).to.be.equal( null );

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				Event.findById.restore();
				done();
			} );
	} );

	it( 'should be able to throw error when settings does not exists', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findById', () => {
			const response = {
				'CreatedBy'    : 0,
				'UpdatedBy'    : 0,
				'Id'           : 2,
				'StationId'    : 1,
				'EventTypeId'  : 1,
				'Title'        : 'Hello World',
				'Start'        : '2017-03-15T00:00:00.000Z',
				'End'          : '2017-03-15T00:00:00.000Z',
				'TeamOne'      : 'Bulls',
				'TeamOneScore' : 0,
				'TeamTwo'      : 'Heat',
				'TeamTwoScore' : 0,
				'updated_at'   : '2017-03-15T07:25:16.000Z',
				'created_at'   : '2017-03-15T07:25:16.000Z',

				update () {
					return Promise.resolve( {
						toJSON () {
							return data;
						}
					} );
				},

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( eventService, 'updateSetting', () => Promise.resolve( false ) );

		eventService.updateById( data.Id, data )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Event Settings does not exists' );

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				Event.findById.restore();
				eventService.updateSetting.restore();
				done();
			} );
	} );

	it( 'should be able to return resource even there is no event details', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findById', () => {
			const response = {
				'CreatedBy'    : 0,
				'UpdatedBy'    : 0,
				'Id'           : 2,
				'StationId'    : 1,
				'EventTypeId'  : 1,
				'Title'        : 'Hello World',
				'Start'        : '2017-03-15T00:00:00.000Z',
				'End'          : '2017-03-15T00:00:00.000Z',
				'JobId'        : 1,
				'TeamOne'      : 'Bulls',
				'TeamOneScore' : 0,
				'TeamTwo'      : 'Heat',
				'TeamTwoScore' : 0,
				'updated_at'   : '2017-03-15T07:25:16.000Z',
				'created_at'   : '2017-03-15T07:25:16.000Z',
				'EventDetails' : [ {
					'Id'          : 1,
					'Description' : 'Hello World'
				} ],

				update () {
					return Promise.resolve( {
						toJSON () {
							return data;
						}
					} );
				},

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );

		sinon.stub( stationService, 'getById', () => {
			return Promise.resolve( {
				'Id'              : 1,
				'CalendarSetting' : {
					'StorageLocation' : 'funkwreckords-4-149032538/'
				}
			} );
		} );

		sinon.stub( eventService, 'updateSetting', () => Promise.resolve( true ) );
		sinon.stub( eventService, 'deleteDetails', () => Promise.resolve( true ) );
		sinon.stub( eventService, 'createDetails', () => Promise.resolve( true ) ) ;
		sinon.stub( eventService, 'getByIdWithDetails', () => Promise.resolve( data ) );

		sinon.stub( kue, 'createQueue', () => {
			const queue = {
				create () {
					return queue;
				},

				delay () {
					return queue;
				},

				save ( cb ) {
					cb();
					return queue;
				},

				shutdown () {
					return queue;
				}
			};

			return queue;
		} );

		sinon.stub( kue.Job, 'get', ( id, cb ) => {
			cb( null, {
				state () {
					return 'delayed';
				}
			} );
		} );
		sinon.stub( eventService, 'createJobInDelayedEvent', () => Promise.resolve( true ) );
		sinon.stub( kue.Job, 'remove', () => Promise.resolve() );
		sinon.stub( EventQueue, 'destroy', () => Promise.resolve() );
		sinon.stub( EventQueue, 'create', () => Promise.resolve() );
		sinon.stub( EventQueue, 'findOverridingJob', () => {
			return [ {
				'Id'       : 1,
				'JobId'    : 1,
				'EventId'  : 1,
				'Duration' : 33131312
			} ];
		} );

		const event = _.omit( data, 'EventDetails' );
		eventService.updateById( data.Id, event )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				Event.findById.restore();
				stationService.getById.restore();
				eventService.updateSetting.restore();
				eventService.deleteDetails.restore();
				eventService.createDetails.restore();
				eventService.getByIdWithDetails.restore();
				kue.createQueue.restore();
				kue.Job.get.restore();
				kue.Job.remove.restore();
				EventQueue.destroy.restore();
				EventQueue.create.restore();
				EventQueue.findOverridingJob.restore();
				done();
			} );
	} );

	it( 'should be able to return resource', done => {
		sinon.stub( Event, 'findUnderlap', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findInBetween', () => {
			return Promise.resolve( [] );
		} );

		sinon.stub( Event, 'findById', () => {
			const response =  {
				'CreatedBy'    : 0,
				'UpdatedBy'    : 0,
				'Id'           : 2,
				'StationId'    : 1,
				'EventTypeId'  : 1,
				'Title'        : 'Hello World',
				'Start'        : '2017-03-15T00:00:00.000Z',
				'End'          : '2017-03-15T00:00:00.000Z',
				'JobId'        : 1,
				'TeamOne'      : 'Bulls',
				'TeamOneScore' : 0,
				'TeamTwo'      : 'Heat',
				'TeamTwoScore' : 0,
				'updated_at'   : '2017-03-15T07:25:16.000Z',
				'created_at'   : '2017-03-15T07:25:16.000Z',

				update () {
					return Promise.resolve( {
						toJSON () {
							return data;
						}
					} );
				},

				toJSON () {
					return response;
				}
			};

			return Promise.resolve( response );
		} );


		sinon.stub( stationService, 'getById', () => {
			return Promise.resolve( {
				'Id'              : 1,
				'CalendarSetting' : {
					'StorageLocation' : 'funkwreckords-4-149032538/'
				}
			} );
		} );

		sinon.stub( eventService, 'updateSetting', () => Promise.resolve( true ) );
		sinon.stub( eventService, 'deleteDetails', () => Promise.resolve( true ) );
		sinon.stub( eventService, 'createDetails', () => Promise.resolve( true ) ) ;
		sinon.stub( eventService, 'getByIdWithDetails', () => Promise.resolve( data ) );

		sinon.stub( kue, 'createQueue', () => {
			const queue = {
				create () {
					return queue;
				},

				delay () {
					return queue;
				},

				save ( cb ) {
					cb();
					return queue;
				},

				shutdown () {
					return queue;
				}
			};

			return queue;
		} );

		sinon.stub( kue.Job, 'get', ( id, cb ) => {
			cb( null, {
				state () {
					return 'delayed';
				}
			} );
		} );
		sinon.stub( kue.Job, 'remove', () => Promise.resolve() );
		sinon.stub( EventQueue, 'destroy', () => Promise.resolve() );
		sinon.stub( EventQueue, 'create', () => Promise.resolve() );
		sinon.stub( EventQueue, 'findOverridingJob', () => {
			return [ {
				'Id'       : 1,
				'JobId'    : 1,
				'EventId'  : 1,
				'Duration' : 33131312
			} ];
		} );

		data.EventSetting.FrequencyType.UnitOfMeasurement = {
			'Name' : null
		};
		eventService.updateById( data.Id, data )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findUnderlap.restore();
				Event.findInBetween.restore();
				Event.findById.restore();
				stationService.getById.restore();
				eventService.updateSetting.restore();
				eventService.deleteDetails.restore();
				eventService.createDetails.restore();
				eventService.getByIdWithDetails.restore();
				kue.createQueue.restore();
				kue.Job.get.restore();
				kue.Job.remove.restore();
				EventQueue.destroy.restore();
				EventQueue.create.restore();
				EventQueue.findOverridingJob.restore();
				done();
			} );
	} );
} );
