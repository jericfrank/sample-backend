'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const Event = models.Event;

let eventService;
let data;

before( done => {
	data = {
		'Id' : 1
	};

	eventService = use( 'services/events' );

	done();
} );

describe( 'EventService::getById', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( Event, 'findById', () => {
			throw new Error( 'Error' );
		} );

		eventService.getById( data.Id )
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				Event.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return null when user does not exist', done => {
		sinon.stub( Event, 'findById', () => Promise.resolve( null ) );

		eventService.getById( data.Id )
			.then( response => {
				expect( response ).to.be.equal( null );

				Event.findById.restore();
				done();
			} );
	} );

	it( 'should be able to return resource', done => {
		sinon.stub( Event, 'findById', () => {
			return Promise.resolve( {
				toJSON () {
					return {
						'CreatedBy'    : 0,
						'UpdatedBy'    : 0,
						'Id'           : 2,
						'StationId'    : 1,
						'EventTypeId'  : 1,
						'Title'        : 'Hello World',
						'Start'        : '2017-03-15T00:00:00.000Z',
						'End'          : '2017-03-15T00:00:00.000Z',
						'TeamOne'      : 'Bulls',
						'TeamOneScore' : 0,
						'TeamTwo'      : 'Heat',
						'TeamTwoScore' : 0,
						'updated_at'   : '2017-03-15T07:25:16.000Z',
						'created_at'   : '2017-03-15T07:25:16.000Z'
					};
				}
			} );
		} );

		eventService.getById( data.Id )
			.then( response => {
				expect( response ).to.be.an.object();

				Event.findById.restore();
				done();
			} );
	} );
});
