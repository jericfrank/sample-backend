'use strict';

require( 'dotenv' ).config();

const Code  = require( 'code' );
const Lab   = require( 'lab' );
const sinon = require( 'sinon' );

const models = use( 'models' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const expect   = Code.expect;

const EventDetail = models.EventDetail;

let eventDetailService;
let response;

before( done => {
	response = {
		'Id'          : 1,
		'EventId'     : 1,
		'Description' : 'some-random-string',
		'CreatedBy'   : 1,
		'UpdatedBy'   : 1,
		'created_at'  : '2017-02-06T05:59:56.000Z',
		'updated_at'  : '0000-00-00 00:00:00',

		toJSON () {
			return response;
		}
	};

	eventDetailService = use( 'services/event-details' );

	done();
} );

describe( 'EventDetailsService::getAll', () => {
	it( 'should be able to throw an error', done => {
		sinon.stub( EventDetail, 'findAll', () => {
			throw new Error( 'Error' );
		} );

		eventDetailService.getAll()
			.catch( response => {
				expect( response ).to.be.an.error( Error, 'Error' );

				EventDetail.findAll.restore();
				done();
			} );
	} );

	it ( 'should be able to fetch resource filtered by query string', done => {
		sinon.stub( EventDetail, 'findAll', () => {
			return Promise.resolve( [ response ] );
		} );

		eventDetailService.getAll( 1 )
			.then( response => {
				expect( response ).to.be.an.array();

				EventDetail.findAll.restore();
				done();
			} );
	} );

	it ( 'should be able to fetch all resource', done => {
		sinon.stub( EventDetail, 'findAll', () => {
			return Promise.resolve( [ response ] );
		} );

		eventDetailService.getAll()
			.then( response => {
				expect( response ).to.be.an.array();

				EventDetail.findAll.restore();
				done();
			} );
	} );
} );
