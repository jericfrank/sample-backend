'use strict';

require( 'dotenv' ).config();

const Code = require( 'code' );
const Lab  = require( 'lab' );
const Hapi = require( 'hapi' );

const lab        = exports.lab = Lab.script();
const before     = lab.before;
const beforeEach = lab.beforeEach;
const describe   = lab.describe;
const it         = lab.it;
const expect     = Code.expect;



const internals = {};
let server;

before( done => {
	internals.permissionFunc = ( credentials, callback ) => {
		const userPermissions = {
			'event-frequencies' : {
				'CanRead'   : true,
				'CanCreate' : false,
				'CanEdit'   : true,
				'CanDelete' : true
			}
		};

		callback( null, userPermissions );
	};

	done();
} );

beforeEach( done => {
	server = new Hapi.Server();
	server.connection();
	done();
} );

describe( 'hapi-route-acl', () => {
	describe('registration', () => {
		it( 'should return an error if options.permissionsFunc is not defined', done => {
			server.register( {
				'register' : use( 'utils/route-acl' )
			}, err => {
				expect( err ).to.exist();
				done();
			} );
		} );

		it( 'should return an error if options.permissionsFunc is not a function', done => {
			server.register( {
				'register' : use( 'utils/route-acl' ),
				'options'  : {
					'permissionFunc' : 123
				}
			}, err => {
				expect( err ).to.exist();
				done();
			} );
		} );
	} );

	describe( 'route protection', () => {
		it('should allow access to a route if plugin configuration is not defined in route config', done => {
			server.register( {
				'register' : use( 'utils/route-acl' ),
				'options'  : {
					'permissionFunc' : internals.permissionFunc
				}
			}, err => {
				if ( err ) {
					throw err;
				}
			} );

			server.route( {
				'method' : 'GET',
				'path'   : '/unprotected1',
				'config' : { 'handler' : ( request, reply ) => reply( 'hola mi amigo' ) }
			} );

			server.inject( {
				'method' : 'GET',
				'url'    : '/unprotected1'
			}, res => {
				expect( res.statusCode ).to.equal( 200 );
				done();
			} );
		} );

		it( 'should allow access to a route if required permission array is empty', done => {
			server.register( {
				'register' : use( 'utils/route-acl' ),
				'options'  : {
					'permissionFunc' : internals.permissionFunc
				}
			}, err => {
				if ( err ) {
					throw err;
				}
			} );

			server.route({
				'method' : 'GET',
				'path'   : '/unprotected2',
				'config' : {
					'handler' : ( request, reply ) => reply( 'como estas?' ),
					'plugins' : {
						'routeAcl' : {
							'permissions' : []
						}
					}
				}
			} );

			server.inject( {
				'method' : 'GET',
				'url'    : '/unprotected2'
			}, res => {
				expect( res.statusCode ).to.equal( 200 );
				done();
			} );
		} );

		it( 'should allow access to a route if user has permission', done => {
			server.register( {
				'register' : use( 'utils/route-acl' ),
				'options'  : {
					'permissionFunc' : internals.permissionFunc
				}
			}, err => {
				if ( err ) {
					throw err;
				}
			} );

			server.route({
				'method' : 'GET',
				'path'   : '/cars',
				'config' : {
					'handler' : ( request, reply ) => reply( [ 'Toyota Camry', 'Honda Accord', 'Ford Fusion' ] ),
					'plugins' : {
						'routeAcl' : {
							'permissions' : [ 'event-frequencies:CanRead' ]
						}
					}
				}
			} );

			server.inject( {
				'method' : 'GET',
				'url'    : '/cars'
			}, res => {
				expect(res.statusCode).to.equal(200);
				done();
			} );
		} );

		it( 'should allow access for permissions defined as a string', done => {
			server.register( {
				'register' : use( 'utils/route-acl' ),
				'options'  : {
					'permissionFunc' : internals.permissionFunc
				}
			}, err => {
				if ( err ) {
					throw err;
				}
			} );

			server.route({
				'method' : 'GET',
				'path'   : '/cars/{id}',
				'config' : {
					'handler' : ( request, reply ) => reply( 'Toyota Camry' ),
					'plugins' : {
						'routeAcl' : {
							'permissions' : 'event-frequencies:CanRead'
						}
					}
				}
			} );

			server.inject( {
				'method' : 'GET',
				'url'    : '/cars/1'
			}, res => {
				expect( res.statusCode ).to.equal( 200 );
				done();
			} );
		} );

		it( 'should deny access to a route if user does not have permission', done => {
			server.register( {
				'register' : use( 'utils/route-acl' ),
				'options'  : {
					'permissionFunc' : internals.permissionFunc
				}
			}, err => {
				if ( err ) {
					throw err;
				}
			} );

			server.route({
				'method' : 'POST',
				'path'   : '/cars',
				'config' : {
					'handler' : ( request, reply ) => reply( 'car created!' ),
					'plugins' : {
						'routeAcl' : {
							'permissions' : [ 'event-frequencies:CanCreate' ]
						}
					}
				}
			} );

			server.inject( {
				'method' : 'POST',
				'url'    : '/cars'
			}, res => {
				expect( res.statusCode ).to.equal( 403 );
				done();
			} );
		} );
	} );
} );

