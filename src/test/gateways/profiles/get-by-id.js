'use strict';

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const log            = use( 'logging' );
const profileService = use( 'services/profiles' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, process.env.JWT_KEY );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'profiles'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	done();
} );

describe( 'GET: /api/profiles/{Id}', () => {
	it( 'should be able to catch 500 server error', done => {
		sinon.stub( profileService, 'getById', () => {
			return Promise.reject( {
				'message' : 'Finding Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/profiles/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			profileService.getById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 404 not found', done => {
		sinon.stub( profileService, 'getById', () => Promise.resolve( null ) );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/profiles/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 404 );
			expect( res.result.error ).to.equal( 'Not Found' );

			profileService.getById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to fetch resource', done => {
		sinon.stub( profileService, 'getById', () => {
			return {
				'Id'         : 2,
				'Name'       : 'power users',
				'CreatedBy'  : 1,
				'UpdatedBy'  : 1,
				'created_at' : '2017-02-06T05:59:56.000Z',
				'updated_at' : '0000-00-00 00:00:00',
				'deleted_at' : null
			};
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/profiles/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = res.result.data;

			expect( response.Id ).to.be.a.number();
			expect( response.Name ).to.be.a.string();
			expect( response.CreatedBy ).to.be.a.number();
			expect( response.UpdatedBy ).to.be.a.number();

			profileService.getById.restore();
			server.stop( done );
		} );
	} );
} );
