'use strict';

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const config      = use( 'config' );
const log         = use( 'logging' );
const userService = use( 'services/users' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, config.app.jwtKey );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'users'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	done();
} );

describe( 'PUT: /api/users/{Id}', () => {
	it( 'should be able to catch 500 bad request', done => {
		sinon.stub( userService, 'updateById', function () {
			return Promise.reject( {
				'message' : 'Validation error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'PUT',
			'url'     : '/api/users/1',
			'headers' : {
				authorization
			},
			'payload' : {
				'Id'              : 1,
				'UserName'        : 'johndoe',
				'FirstName'       : 'john',
				'MiddleName'      : 'beta',
				'LastName'        : 'doe',
				'EmailAddress'    : 'john@sample.com',
				'Password'        : 'secret',
				'ProfileId'       : 1,
				'StationId'       : 1,
				'AccountDisabled' : false,
				'Stations'        : [ 1 ],
				'CreatedBy'       : 0,
				'UpdatedBy'       : 0,
				'created_at'      : 'string',
				'updated_at'      : 'string'
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			userService.updateById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 404 not found', done => {
		sinon.stub( userService, 'updateById', () => Promise.resolve( null ) );

		const request = {
			'method'  : 'PUT',
			'url'     : '/api/users/1',
			'headers' : {
				authorization
			},
			'payload' : {
				'Id'              : 1,
				'UserName'        : 'johndoe',
				'FirstName'       : 'john',
				'MiddleName'      : 'beta',
				'LastName'        : 'doe',
				'EmailAddress'    : 'john@sample.com',
				'Password'        : 'secret',
				'ProfileId'       : 1,
				'StationId'       : 1,
				'AccountDisabled' : false,
				'Stations'        : [ 1 ],
				'CreatedBy'       : 0,
				'UpdatedBy'       : 0,
				'created_at'      : 'string',
				'updated_at'      : 'string'
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 404 );
			expect( res.result.error ).to.equal( 'Not Found' );

			userService.updateById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to update resource', done => {
		sinon.stub( userService, 'updateById', function () {
			return Promise.resolve( {
				'Id'              : 1,
				'UserName'        : 'johndoe',
				'FirstName'       : 'john',
				'MiddleName'      : 'beta',
				'LastName'        : 'doe',
				'EmailAddress'    : 'john@sample.com',
				'Password'        : 'secret',
				'ProfileId'       : 1,
				'StationId'       : 1,
				'AccountDisabled' : false,
				'Stations'        : [ 1 ],
				'CreatedBy'       : 0,
				'UpdatedBy'       : 0,
				'created_at'      : 'string',
				'updated_at'      : 'string'
			} );
		} );

		const request = {
			'method'  : 'PUT',
			'url'     : '/api/users/1',
			'headers' : {
				authorization
			},
			'payload' : {
				'Id'              : 1,
				'UserName'        : 'johndoe',
				'FirstName'       : 'john',
				'MiddleName'      : 'beta',
				'LastName'        : 'doe',
				'EmailAddress'    : 'john@sample.com',
				'Password'        : 'secret',
				'ProfileId'       : 1,
				'StationId'       : 1,
				'AccountDisabled' : false,
				'Stations'        : [ 1 ],
				'CreatedBy'       : 0,
				'UpdatedBy'       : 0,
				'created_at'      : 'string',
				'updated_at'      : 'string'
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = res.result.data;

			expect( response.Id ).to.be.a.number();
			expect( response.UserName ).to.be.a.string();
			expect( response.FirstName ).to.be.a.string();
			expect( response.MiddleName ).to.be.a.string();
			expect( response.LastName ).to.be.a.string();
			expect( response.EmailAddress ).to.be.a.string();
			expect( response.Password ).to.be.a.string();
			expect( response.ProfileId ).to.be.a.number();
			expect( response.StationId ).to.be.a.number();
			expect( response.AccountDisabled ).to.be.an.boolean();
			expect( response.Stations ).to.be.an.array();

			userService.updateById.restore();
			server.stop( done );
		} );
	} );
} );
