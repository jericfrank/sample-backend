'use strict';

require( 'dotenv' ).config();

const Code        = require( 'code' );
const Lab         = require( 'lab' );
const sinon       = require( 'sinon' );
const jwt         = require( 'jsonwebtoken' );
const moment      = require( 'moment' );

const config      = use( 'config' );
const log         = use( 'logging' );
const userService = use( 'services/users' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, config.app.jwtKey );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'users'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );
	sinon.stub( log, 'info' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	log.info.restore();
	done();
} );

describe( 'GET: /api/users/{id}', () => {
	it( 'should be able to catch 500 server error', done => {
		sinon.stub( userService, 'getById', () => {
			return Promise.reject( {
				'message' : 'Finding Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/users/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			userService.getById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 404 not found', done => {
		sinon.stub( userService, 'getById', () => Promise.resolve( null ) );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/users/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 404 );
			expect( res.result.error ).to.equal( 'Not Found' );

			userService.getById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to fetch all resource', done => {
		sinon.stub( userService, 'getById', () => {
			return {
				'Id'              : 1,
				'UserName'        : 'danswater',
				'FirstName'       : 'Francis',
				'MiddleName'      : 'Viscayno',
				'LastName'        : 'Lamayo',
				'EmailAddress'    : 'dansisabot@gmail.com',
				'ProfileId'       : 1,
				'StationId'       : 1,
				'AccountDisabled' : false,
				'CreatedBy'       : 0,
				'UpdatedBy'       : 0,
				'created_at'      : '2017-02-06T06:04:16.000Z',
				'updated_at'      : '2017-02-22T07:13:04.000Z'
			};
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/users/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = res.result.data;

			expect( response.Id ).to.be.a.number();
			expect( response.UserName ).to.be.a.string();
			expect( response.FirstName ).to.be.a.string();
			expect( response.MiddleName ).to.be.a.string();
			expect( response.LastName ).to.be.a.string();
			expect( response.EmailAddress ).to.be.a.string();
			expect( response.ProfileId ).to.be.a.number();
			expect( response.StationId ).to.be.a.number();
			expect( response.AccountDisabled ).to.be.boolean();
			expect( response.CreatedBy ).to.be.a.number();
			expect( response.UpdatedBy ).to.be.a.number();

			userService.getById.restore();
			server.stop( done );
		} );
	} );
} );
