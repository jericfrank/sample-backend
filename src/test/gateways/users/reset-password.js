'use strict';

require( 'dotenv' ).config();

const Code        = require( 'code' );
const Lab         = require( 'lab' );
const sinon       = require( 'sinon' );

const log         = use( 'logging' );
const userService = use( 'services/users' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;

before( done => {
	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'users'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );
	sinon.stub( log, 'info' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	log.info.restore();
	done();
} );

describe( 'POST: /api/users/reset-password', () => {
	it( 'should be able to response 400 bad request when response contains an error', done => {
		sinon.stub( userService, 'resetPassword' ).returns( Promise.resolve( {
			'message' : 'Sparky Error',
			'errors'  : [ {
				'message' : 'Error was emitted'
			} ]
		} ) );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/users/reset-password',
			'payload' : {
				'Token'    : 'somerandomstring',
				'Password' : 'secrecy'
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 400 );
			expect( res.result.error ).to.equal( 'Bad Request' );
			expect( res.result.message ).to.equal( 'Sparky Error' );

			userService.resetPassword.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to response 404 not found', done => {
		sinon.stub( userService, 'resetPassword' ).returns( Promise.resolve( null ) );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/users/reset-password',
			'payload' : {
				'Token'    : 'somerandomstring',
				'Password' : 'secrecy'
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 404 );
			expect( res.result.error ).to.equal( 'Not Found' );
			expect( res.result.message ).to.equal( 'Resource not found' );

			userService.resetPassword.restore();
			server.stop( done );
		} );
	} );

	it ( 'should be able to catch 500 server error', done => {
		sinon.stub( userService, 'resetPassword', () => {
			return Promise.reject( {
				'message' : 'Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/users/reset-password',
			'payload' : {
				'Token'    : 'somerandomstring',
				'Password' : 'secrecy'
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );

			userService.resetPassword.restore();
			server.stop( done );
		} );
	} );

	it( 'it should be able to reset password', done => {
		sinon.stub( userService, 'resetPassword' ).returns( Promise.resolve( {
				'Operation' : true
		} ) );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/users/reset-password',
			'payload' : {
				'Token'    : 'somerandomstring',
				'Password' : 'secrecy'
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );
			expect( res.result.data ).to.be.an.object();
			expect( res.result.data.Operation ).to.be.boolean();

			userService.resetPassword.restore();
			server.stop( done );
		} );
	} );
} );
