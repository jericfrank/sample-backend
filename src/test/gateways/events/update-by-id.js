'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );

const config       = use( 'config' );
const log          = use( 'logging' );
const eventService = use( 'services/events' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, config.app.jwtKey );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'events'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );
	sinon.stub( log, 'info' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	log.info.restore();
	done();
} );

describe( 'PUT: /api/events/{Id}', () => {
	it( 'should be able to catch 500 bad request', done => {
		sinon.stub( eventService, 'updateById', function () {
			return Promise.reject( {
				'message' : 'Validation error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'PUT',
			'url'     : '/api/events/1',
			'headers' : {
				authorization
			},
			'payload' : {
				'Id'           : 1,
				'StationId'    : 1,
				'EventTypeId'  : 1,
				'Title'        : 'Hello World',
				'Start'        : moment().add( 1, 'h' ).format(),
				'End'          : moment().add( 2, 'h' ).format(),
				'TeamOne'      : 'Bulls',
				'TeamOneScore' : 0,
				'TeamTwo'      : 'Heat',
				'TeamTwoScore' : 0,
				'created_at'   : '2017-02-06T06:04:00.000Z',
				'updated_at'   : '0000-00-00 00:00:00',
				'CreatedBy'    : 1,
				'UpdatedBy'    : 1
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			eventService.updateById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 404 not found', done => {
		sinon.stub( eventService, 'updateById', () => Promise.resolve( null ) );

		const request = {
			'method'  : 'PUT',
			'url'     : '/api/events/1',
			'headers' : {
				authorization
			},
			'payload' : {
				'Id'           : 1,
				'StationId'    : 1,
				'EventTypeId'  : 1,
				'Title'        : 'Hello World',
				'Start'        : moment().add( 1, 'h' ).format(),
				'End'          : moment().add( 2, 'h' ).format(),
				'TeamOne'      : 'Bulls',
				'TeamOneScore' : 0,
				'TeamTwo'      : 'Heat',
				'TeamTwoScore' : 0,
				'created_at'   : '2017-02-06T06:04:00.000Z',
				'updated_at'   : '0000-00-00 00:00:00',
				'CreatedBy'    : 1,
				'UpdatedBy'    : 1
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 404 );
			expect( res.result.error ).to.equal( 'Not Found' );

			eventService.updateById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to response 400 bad request when response contains an error', done => {
		sinon.stub( eventService, 'updateById' ).returns( Promise.resolve( {
			'message' : 'Sparky Error',
			'errors'  : [ {
				'message' : 'Error was emitted'
			} ]
		} ) );

		const request = {
			'method'  : 'PUT',
			'url'     : '/api/events/1',
			'headers' : {
				authorization
			},
			'payload' : {
				'Id'           : 1,
				'StationId'    : 1,
				'EventTypeId'  : 1,
				'Title'        : 'Hello World',
				'Start'        : moment().add( 1, 'h' ).format(),
				'End'          : moment().add( 2, 'h' ).format(),
				'TeamOne'      : 'Bulls',
				'TeamOneScore' : 0,
				'TeamTwo'      : 'Heat',
				'TeamTwoScore' : 0,
				'created_at'   : '2017-02-06T06:04:00.000Z',
				'updated_at'   : '0000-00-00 00:00:00',
				'CreatedBy'    : 1,
				'UpdatedBy'    : 1
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 400 );
			expect( res.result.error ).to.equal( 'Bad Request' );
			expect( res.result.message ).to.equal( 'Error was emitted' );

			eventService.updateById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to update resource', done => {
		sinon.stub( eventService, 'updateById', function () {
			return Promise.resolve( {
				'CreatedBy'    : 0,
				'UpdatedBy'    : 0,
				'Id'           : 2,
				'StationId'    : 1,
				'EventTypeId'  : 1,
				'Title'        : 'Hello World',
				'Start'        : moment().add( 1, 'h' ).format(),
				'End'          : moment().add( 2, 'h' ).format(),
				'TeamOne'      : 'Bulls',
				'TeamOneScore' : 0,
				'TeamTwo'      : 'Heat',
				'TeamTwoScore' : 0,
				'updated_at'   : '2017-03-15T07:25:16.000Z',
				'created_at'   : '2017-03-15T07:25:16.000Z'
			} );
		} );

		const request = {
			'method'  : 'PUT',
			'url'     : '/api/events/1',
			'headers' : {
				authorization
			},
			'payload' : {
				'Id'           : 1,
				'StationId'    : 1,
				'EventTypeId'  : 1,
				'Title'        : 'Hello World',
				'Start'        : moment().add( 1, 'h' ).format(),
				'End'          : moment().add( 2, 'h' ).format(),
				'TeamOne'      : 'Bulls',
				'TeamOneScore' : 0,
				'TeamTwo'      : 'Heat',
				'TeamTwoScore' : 0,
				'created_at'   : '2017-02-06T06:04:00.000Z',
				'updated_at'   : '0000-00-00 00:00:00',
				'CreatedBy'    : 1,
				'UpdatedBy'    : 1
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = res.result.data;

			expect( response.CreatedBy ).to.be.a.number();
			expect( response.UpdatedBy ).to.be.a.number();
			expect( response.Id ).to.be.a.number();
			expect( response.StationId ).to.be.a.number();
			expect( response.EventTypeId ).to.be.a.number();
			expect( response.Title ).to.be.a.string();
			expect( response.Start ).to.be.a.string();
			expect( response.End ).to.be.a.string();
			expect( response.TeamOne ).to.be.a.string();
			expect( response.TeamOneScore ).to.be.a.number();
			expect( response.TeamTwo ).to.be.a.string();
			expect( response.TeamTwoScore ).to.be.a.number();

			eventService.updateById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to update reccuring resource', done => {
		sinon.stub( eventService, 'updateWithRecurrence', function () {
			return Promise.resolve( {
				'CreatedBy'    : 0,
				'UpdatedBy'    : 0,
				'Id'           : 1,
				'StationId'    : 1,
				'EventTypeId'  : 1,
				'Title'        : 'Hello World',
				'Start'        : moment().add( 1, 'h' ).format(),
				'End'          : moment().add( 2, 'h' ).format(),
				'TeamOne'      : 'Bulls',
				'TeamOneScore' : 0,
				'TeamTwo'      : 'Heat',
				'TeamTwoScore' : 0,
				'updated_at'   : '2017-03-15T07:25:16.000Z',
				'created_at'   : '2017-03-15T07:25:16.000Z'
			} );
		} );

		const request = {
			'method'  : 'PUT',
			'url'     : '/api/events/1',
			'headers' : {
				authorization
			},
			'payload' : {
				'Id'           : 1,
				'StationId'    : 69,
				'EventTypeId'  : 1,
				'Title'        : 'NBA Recap: GSW vs Spurs',
				'Start'        : moment().add( 1, 'h' ).format(),
				'End'          : moment().add( 2, 'h' ).format(),
				'EventSetting' : {
					'Id'                 : 1,
					'EventId'            : 1,
					'FrequencyTypeId'    : 1,
					'FrequencyTypeValue' : 1,
					'Color'              : 'red',
					'NoLineTextToExport' : 1
				},

				'EventDetails' : [
					{ 'Description' : 'GSW win in Game 1' }
				],

				'EventRecurrence' : {
					'Id'           : 1,
					'EventId'      : 1,
					'RepeatOption' : 1,
					'DayOption'    : 1,
					'RepeatEvery'  : 1,
					'Start'        : '2017-05-15',
					'End'          : '2017-05-19'
				}
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = res.result.data;

			expect( response.CreatedBy ).to.be.a.number();
			expect( response.UpdatedBy ).to.be.a.number();
			expect( response.Id ).to.be.a.number();
			expect( response.StationId ).to.be.a.number();
			expect( response.EventTypeId ).to.be.a.number();
			expect( response.Title ).to.be.a.string();
			expect( response.Start ).to.be.a.string();
			expect( response.End ).to.be.a.string();
			expect( response.TeamOne ).to.be.a.string();
			expect( response.TeamOneScore ).to.be.a.number();
			expect( response.TeamTwo ).to.be.a.string();
			expect( response.TeamTwoScore ).to.be.a.number();

			eventService.updateWithRecurrence.restore();
			server.stop( done );
		} );
	} );
} );
