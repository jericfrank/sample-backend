'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const config       = use( 'config' );
const log          = use( 'logging' );
const eventService = use( 'services/events' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, config.app.jwtKey );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'events'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );
	sinon.stub( log, 'info' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	log.info.restore();
	done();
} );

describe( 'DELETE: /api/events/{id}', () => {
	it( 'should be able to catch 500 server error', done => {
		sinon.stub( eventService, 'deleteBySeries', () => {
			return Promise.reject( {
				'message' : 'Deletion Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'DELETE',
			'url'     : '/api/events/series/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			eventService.deleteBySeries.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 404 not found', done => {
		sinon.stub( eventService, 'deleteBySeries', () => Promise.resolve( null ) );

		const request = {
			'method'  : 'DELETE',
			'url'     : '/api/events/series/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 404 );
			expect( res.result.error ).to.equal( 'Not Found' );

			eventService.deleteBySeries.restore();
			server.stop( done );
		} );
	} );


	it( 'should be able to delete', done => {
		sinon.stub( eventService, 'deleteBySeries', () => Promise.resolve( {
			'Operation' : true
		} ) );

		const request = {
			'method'  : 'DELETE',
			'url'     : '/api/events/series/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			eventService.deleteBySeries.restore();
			server.stop( done );
		} );
	} );
} );
