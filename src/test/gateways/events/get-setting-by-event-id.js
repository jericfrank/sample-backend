'use strict';

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const log          = use( 'logging' );
const eventService = use( 'services/events' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, process.env.JWT_KEY );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'events'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	done();
} );

describe( 'GET: /api/events/{EventId}/settings', () => {
	it( 'should be able to catch 500 server error', done => {
		sinon.stub( eventService, 'getSettingByEventId', () => {
			return Promise.reject( {
				'message' : 'Finding Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/events/1/settings',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			eventService.getSettingByEventId.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 404 not found', done => {
		sinon.stub( eventService, 'getSettingByEventId', () => Promise.resolve( null ) );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/events/1/settings',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 404 );
			expect( res.result.error ).to.equal( 'Not Found' );

			eventService.getSettingByEventId.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to fetch resource', done => {
		sinon.stub( eventService, 'getSettingByEventId', () => {
			return {
				'CreatedBy'          : 0,
				'UpdatedBy'          : 0,
				'Id'                 : 0,
				'EventId'            : 0,
				'FrequencyTypeId'    : 0,
				'FrequencyTypeValue' : 0,
				'Color'              : 'string',
				'NoLineTextToExport' : 0,
				'created_at'         : '2017-02-06T06:04:00.000Z',
				'updated_at'         : '0000-00-00 00:00:00'
			};
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/events/1/settings',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = res.result.data;

			expect( response.Id ).to.be.a.number();
			expect( response.EventId ).to.be.a.number();
			expect( response.FrequencyTypeId ).to.be.a.number();
			expect( response.FrequencyTypeValue ).to.be.a.number();
			expect( response.Color ).to.be.a.string();
			expect( response.NoLineTextToExport ).to.be.number();
			expect( response.CreatedBy ).to.be.a.number();
			expect( response.UpdatedBy ).to.be.a.number();

			eventService.getSettingByEventId.restore();
			server.stop( done );
		} );
	} );
} );
