'use strict';

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const log          = use( 'logging' );
const eventService = use( 'services/events' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, process.env.JWT_KEY );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'events'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	done();
} );

describe( 'GET: /api/events/{Id}', () => {
	it( 'should be able to catch 500 server error', done => {
		sinon.stub( eventService, 'getById', () => {
			return Promise.reject( {
				'message' : 'Finding Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/events/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			eventService.getById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 404 not found', done => {
		sinon.stub( eventService, 'getById', () => Promise.resolve( null ) );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/events/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 404 );
			expect( res.result.error ).to.equal( 'Not Found' );

			eventService.getById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to fetch resource', done => {
		sinon.stub( eventService, 'getById', () => {
			return {
				'CreatedBy'    : 0,
				'UpdatedBy'    : 0,
				'Id'           : 2,
				'StationId'    : 1,
				'EventTypeId'  : 1,
				'Title'        : 'Hello World',
				'Start'        : '2017-03-15T00:00:00.000Z',
				'End'          : '2017-03-15T00:00:00.000Z',
				'TeamOne'      : 'Bulls',
				'TeamOneScore' : 0,
				'TeamTwo'      : 'Heat',
				'TeamTwoScore' : 0,
				'updated_at'   : '2017-03-15T07:25:16.000Z',
				'created_at'   : '2017-03-15T07:25:16.000Z',
				'EventSetting' : {}
			};
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/events/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = res.result.data;

			expect( response.CreatedBy ).to.be.a.number();
			expect( response.UpdatedBy ).to.be.a.number();
			expect( response.Id ).to.be.a.number();
			expect( response.StationId ).to.be.a.number();
			expect( response.EventTypeId ).to.be.a.number();
			expect( response.Title ).to.be.a.string();
			expect( response.Start ).to.be.a.string();
			expect( response.End ).to.be.a.string();
			expect( response.TeamOne ).to.be.a.string();
			expect( response.TeamOneScore ).to.be.a.number();
			expect( response.TeamTwo ).to.be.a.string();
			expect( response.TeamTwoScore ).to.be.a.number();
			expect( response.EventSetting ).to.be.an.object();

			eventService.getById.restore();
			server.stop( done );
		} );
	} );
} );
