'use strict';

require( 'dotenv' ).config();

const Code        = require( 'code' );
const Lab         = require( 'lab' );
const sinon       = require( 'sinon' );
const jwt         = require( 'jsonwebtoken' );
const moment      = require( 'moment' );

const config       = use( 'config' );
const log          = use( 'logging' );
const eventService = use( 'services/events' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, config.app.jwtKey );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'events'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );
	sinon.stub( log, 'info' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	log.info.restore();
	done();
} );

describe( 'POST: /api/events/{EventId}/settings', () => {
	it( 'should be able to catch 500 bad request', done => {
		sinon.stub( eventService, 'createSetting', function () {
			return Promise.reject( {
				'message' : 'Validation error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/events/1/settings',
			'headers' : {
				authorization
			},
			'payload' : {
				'EventId'            : 1,
				'FrequencyTypeId'    : 1,
				'FrequencyTypeValue' : 2,
				'Color'              : 'pink',
				'NoLineTextToExport' : 2
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			eventService.createSetting.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to response 400 bad request when response contains an error', done => {
		sinon.stub( eventService, 'createSetting' ).returns( Promise.resolve( {
			'message' : 'Sparky Error',
			'errors'  : [ {
				'message' : 'Error was emitted'
			} ]
		} ) );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/events/1/settings',
			'headers' : {
				authorization
			},
			'payload' : {
				'EventId'            : 1,
				'FrequencyTypeId'    : 1,
				'FrequencyTypeValue' : 2,
				'Color'              : 'pink',
				'NoLineTextToExport' : 2
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 400 );
			expect( res.result.error ).to.equal( 'Bad Request' );
			expect( res.result.message ).to.equal( 'Error was emitted' );

			eventService.createSetting.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to create new resource', done => {
		sinon.stub( eventService, 'createSetting', function () {
			return Promise.resolve( {
				'CreatedBy'          : 0,
				'UpdatedBy'          : 0,
				'Id'                 : 46,
				'EventId'            : 1,
				'FrequencyTypeId'    : 1,
				'FrequencyTypeValue' : 2,
				'Color'              : 'pink',
				'NoLineTextToExport' : 2,
				'updated_at'         : '2017-02-23T06:48:35.000Z',
				'created_at'         : '2017-02-23T06:48:35.000Z'
			} );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/events/1/settings',
			'headers' : {
				authorization
			},
			'payload' : {
				'EventId'            : 1,
				'FrequencyTypeId'    : 1,
				'FrequencyTypeValue' : 2,
				'Color'              : 'pink',
				'NoLineTextToExport' : 2
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 201 );

			const response = res.result.data;

			expect( response.CreatedBy ).to.be.a.number();
			expect( response.UpdatedBy ).to.be.a.number();
			expect( response.Id ).to.be.a.number();
			expect( response.EventId ).to.be.a.number();
			expect( response.FrequencyTypeId ).to.be.a.number();
			expect( response.FrequencyTypeValue ).to.be.a.number();
			expect( response.Color ).to.be.a.string();
			expect( response.NoLineTextToExport ).to.be.a.number();
			expect( response.updated_at ).to.be.a.string();
			expect( response.created_at ).to.be.a.string();

			eventService.createSetting.restore();
			server.stop( done );
		} );
	} );
} );
