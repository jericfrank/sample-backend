'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const sinon  = require( 'sinon' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );

const config         = use( 'config' );
const log            = use( 'logging' );
const companyService = use( 'services/company' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, config.app.jwtKey );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'company'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );
	sinon.stub( log, 'info' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	log.info.restore();
	done();
} );

describe( 'PUT: /api/companies', () => {
	it( 'should be able to catch 500 bad request', done => {
		sinon.stub( companyService, 'createOrUpdate', function () {
			return Promise.reject( {
				'message' : 'Validation error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'PUT',
			'url'     : '/api/companies',
			'headers' : {
				authorization
			},
			'payload' : {
				'Name'          : 'John wick bro and co',
				'Address'       : 'Cebu',
				'ContactPerson' : 'xxx',
				'ContactNumber' : 'yyy',
				'PricingType'   : 1,
				'MaxLimit'      : 10
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			companyService.createOrUpdate.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to update resource', done => {
		sinon.stub( companyService, 'createOrUpdate', function () {
			return Promise.resolve( {
				'Name'          : 'John wick bro and co',
				'Address'       : 'Cebu',
				'ContactPerson' : 'xxx',
				'ContactNumber' : 'yyy',
				'PricingType'   : 1,
				'MaxLimit'      : 10,
				'CompanyLogo'   : null
			} );
		} );

		const request = {
			'method'  : 'PUT',
			'url'     : '/api/companies',
			'headers' : {
				authorization
			},
			'payload' : {
				'Name'          : 'John wick bro and co',
				'Address'       : 'Cebu',
				'ContactPerson' : 'xxx',
				'ContactNumber' : 'yyy',
				'PricingType'   : 1,
				'MaxLimit'      : 10
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = res.result.data;

			expect( response.Name ).to.be.a.string();
			expect( response.Address ).to.be.a.string();
			expect( response.ContactPerson ).to.be.string();
			expect( response.ContactNumber ).to.be.string();
			expect( response.PricingType ).to.be.number();
			expect( response.MaxLimit ).to.be.number();
			expect( response.CompanyLogo ).to.be.null();

			companyService.createOrUpdate.restore();
			server.stop( done );
		} );
	} );
} );
