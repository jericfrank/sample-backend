'use strict';

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const log            = use( 'logging' );
const companyService = use( 'services/company' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, process.env.JWT_KEY );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'company'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	done();
} );

describe( 'GET: /api/companies/logo', () => {
	it( 'should be able to catch 500 server error', done => {
		sinon.stub( companyService, 'getAll', () => {
			return Promise.reject( {
				'message' : 'Finding Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/companies/logo',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			companyService.getAll.restore();
			server.stop( done );
		} );
	} );

	it ( 'should be able to fetch all resource', done => {
		sinon.stub( companyService, 'getAll', function () {
			return {
				'Name'          : 'Jon wick bro and co.',
				'Address'       : 'zz',
				'ContactPerson' : 'yy',
				'ContactNumber' : 'xxx',
				'PricingType'   : 2,
				'MaxLimit'      : 10,
				'CompanyLogo'   : null
			};
		} );
		const request = {
			'method'  : 'GET',
			'url'     : '/api/companies/logo',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = res.result.data;

			expect( response.CompanyLogo ).to.be.null();

			companyService.getAll.restore();
			server.stop( done );
		} );
	} );
} );
