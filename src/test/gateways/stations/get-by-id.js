'use strict';

const Code        = require( 'code' );
const Lab         = require( 'lab' );
const jwt         = require( 'jsonwebtoken' );
const moment      = require( 'moment' );
const sinon       = require( 'sinon' );

const config         = use( 'config' );
const log            = use( 'logging' );
const stationService = use( 'services/stations' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, config.app.jwtKey );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'stations'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	done();
} );

describe( 'GET: /api/stations/{Id}', () => {
	it( 'should be able to catch 500 server error', done => {
		sinon.stub( stationService, 'getById', () => {
			return Promise.reject( {
				'message' : 'Finding Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/stations/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			stationService.getById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 404 not found', done => {
		sinon.stub( stationService, 'getById', () => Promise.resolve( null ) );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/stations/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 404 );
			expect( res.result.error ).to.equal( 'Not Found' );

			stationService.getById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to fetch resource', done => {
		sinon.stub( stationService, 'getById', () => {
			return {
				'Id'         : 1,
				'Name'       : 'MWFB',
				'Location'   : 'Wisconsin',
				'Logo'       : null,
				'IsActive'   : true,
				'CreatedBy'  : 1,
				'UpdatedBy'  : 1,
				'created_at' : '2017-02-06T06:04:00.000Z',
				'updated_at' : '0000-00-00 00:00:00'
			};
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/stations/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = res.result.data;

			expect( response.Id ).to.be.a.number();
			expect( response.Name ).to.be.a.string();
			expect( response.Location ).to.be.a.string();
			expect( response.Logo ).to.be.null();
			expect( response.IsActive ).to.be.a.boolean();
			expect( response.CreatedBy ).to.be.a.number();
			expect( response.UpdatedBy ).to.be.a.number();

			stationService.getById.restore();
			server.stop( done );
		} );
	} );
} );
