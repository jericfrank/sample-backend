'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const config         = use( 'config' );
const log            = use( 'logging' );
const stationService = use( 'services/stations' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, config.app.jwtKey );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'stations'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );
	sinon.stub( log, 'info' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	log.info.restore();
	done();
} );

describe( 'DELETE: /api/users/{id}', () => {
	it( 'should be able to catch 500 server error', done => {
		sinon.stub( stationService, 'deleteById', () => {
			return Promise.reject( {
				'message' : 'Deletion Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'DELETE',
			'url'     : '/api/stations/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			stationService.deleteById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 404 not found', done => {
		sinon.stub( stationService, 'deleteById', () => Promise.resolve( null ) );

		const request = {
			'method'  : 'DELETE',
			'url'     : '/api/stations/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 404 );
			expect( res.result.error ).to.equal( 'Not Found' );

			stationService.deleteById.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 422 bad data', done => {
		sinon.stub( stationService, 'deleteById', function () {
			return Promise.resolve( {
				'errorType' : 'badData',
				'message'   : 'station has a reference to user'
			} );
		} );

		const request = {
			'method'  : 'DELETE',
			'url'     : '/api/stations/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 422 );
			expect( res.result.error ).to.equal( 'Unprocessable Entity' );
			expect( res.result.message ).to.equal( 'station has a reference to user' );

			stationService.deleteById.restore();
			server.stop( done );
		} );
	} );


	it( 'should be able to delete', done => {
		sinon.stub( stationService, 'deleteById', () => Promise.resolve( {
			'Operation' : true
		} ) );

		const request = {
			'method'  : 'DELETE',
			'url'     : '/api/stations/1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			stationService.deleteById.restore();
			server.stop( done );
		} );
	} );
} );
