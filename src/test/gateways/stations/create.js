'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const config         = use( 'config' );
const log            = use( 'logging' );
const stationService = use( 'services/stations' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, config.app.jwtKey );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'stations'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );
	sinon.stub( log, 'info' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	log.info.restore();
	done();
} );

describe( 'POST: /api/stations', () => {
	it( 'should be able to response 400 bad request when response contains an error', done => {
		sinon.stub( stationService, 'create' ).returns( Promise.resolve( {
			'message' : 'S3 Error',
			'errors'  : [ {
				'message' : 'Error was emitted'
			} ]
		} ) );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/stations',
			'headers' : {
				authorization
			},
			'payload' : {
				'Name'            : 'MWFB',
				'Location'        : 'Wisconsin',
				'Logo'            : '',
				'CalendarSetting' : {
					'StartOfWeek' : 1
				}
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 400 );
			expect( res.result.error ).to.equal( 'Bad Request' );
			expect( res.result.message ).to.equal( 'Error was emitted' );

			stationService.create.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 500 bad request', done => {
		sinon.stub( stationService, 'create', () => {
			return Promise.reject( {
				'message' : 'Validation error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/stations',
			'headers' : {
				authorization
			},
			'payload' : {
				'Name'            : 'MWFB',
				'Location'        : 'Wisconsin',
				'Logo'            : '',
				'CalendarSetting' : {
					'StartOfWeek' : 1
				}
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			stationService.create.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to create new resource', done => {
		sinon.stub( stationService, 'create', function () {
			return Promise.resolve( {
				'Id'              : 1,
				'Name'            : 'MWFB',
				'Location'        : 'Wisconsin',
				'Logo'            : '<base64-image>',
				'CreatedBy'       : 1,
				'UpdatedBy'       : 1,
				'created_at'      : '2017-02-06T06:04:00.000Z',
				'updated_at'      : '0000-00-00 00:00:00',
				'CalendarSetting' : {
					'CreatedBy'       : 1,
					'UpdatedBy'       : 1,
					'StationId'       : 53,
					'StorageLocation' : '<path-to-storage>',
					'StartOfWeek'     : 5,
					'created_at'      : '2017-02-06T06:04:00.000Z',
					'updated_at'      : '0000-00-00 00:00:00'
				}
			} );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/stations',
			'headers' : {
				authorization
			},
			'payload' : {
				'Name'            : 'MWFB',
				'Location'        : 'Wisconsin',
				'Logo'            : '<base64-image>',
				'CalendarSetting' : {
					'StartOfWeek' : 1
				}
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 201 );

			const response = res.result.data;

			expect( response.Id ).to.be.a.number();
			expect( response.Name ).to.be.a.string();
			expect( response.Location ).to.be.a.string();
			expect( response.Logo ).to.be.a.string();
			expect( response.CreatedBy ).to.be.a.number();
			expect( response.UpdatedBy ).to.be.a.number();
			expect( response.updated_at ).to.be.a.string();
			expect( response.created_at ).to.be.a.string();
			expect( response.CalendarSetting ).to.be.an.object();

			stationService.create.restore();
			server.stop( done );
		} );
	} );
} );
