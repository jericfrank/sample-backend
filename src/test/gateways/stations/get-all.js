'use strict';

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const config         = use( 'config' );
const log            = use( 'logging' );
const stationService = use( 'services/stations' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, config.app.jwtKey );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'stations'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );
	sinon.stub( log, 'info' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	log.info.restore();
	done();
} );

describe( 'GET: /api/stations', () => {
	it( 'should be able to catch 500 server error', done => {
		sinon.stub( stationService, 'getAll', () => {
			return Promise.reject( {
				'message' : 'Finding Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/stations',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			stationService.getAll.restore();
			server.stop( done );
		} );
	} );

	it ( 'should be able to fetch all resource', done => {
		sinon.stub( stationService, 'getAll', function () {
			return Promise.resolve( [ {
				'Id'              : 1,
				'Name'            : 'MWFB',
				'Location'        : 'Wisconsin',
				'Logo'            : null,
				'CreatedBy'       : 1,
				'UpdatedBy'       : 1,
				'created_at'      : '2017-02-06T06:04:00.000Z',
				'updated_at'      : '0000-00-00 00:00:00',
				'CalendarSetting' : null
			} ] );
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/stations',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = res.result.data;

			expect( response[ 0 ].Id ).to.be.a.number();
			expect( response[ 0 ].Name ).to.be.a.string();
			expect( response[ 0 ].Location ).to.be.a.string();
			expect( response[ 0 ].Logo ).to.be.null();
			expect( response[ 0 ].CreatedBy ).to.be.a.number();
			expect( response[ 0 ].UpdatedBy ).to.be.a.number();

			stationService.getAll.restore();
			server.stop( done );
		} );
	} );
} );

