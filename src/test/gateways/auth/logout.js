
'use strict';

require( 'dotenv' ).config();

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const config      = use( 'config' );
const log         = use( 'logging' );
const authService = use( 'services/auth' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, config.app.jwtKey );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'auth'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );
	sinon.stub( log, 'info' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	log.info.restore();
	done();
} );

describe( 'POST: /api/logout', () => {
	it( 'should be able to response 400 bad request when response contains an error', done => {
		sinon.stub( authService, 'logout' ).returns( Promise.resolve( {
			'message' : 'S3 Error',
			'errors'  : [ {
				'message' : 'Error was emitted'
			} ]
		} ) );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/logout',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 400 );
			expect( res.result.error ).to.equal( 'Bad Request' );
			expect( res.result.message ).to.equal( 'Error was emitted' );

			authService.logout.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 500 bad request', done => {
		sinon.stub( authService, 'logout', () => {
			return Promise.reject( {
				'message' : 'Validation error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/logout',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			authService.logout.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to invalidate token', done => {
		sinon.stub( authService, 'logout', function () {
			return Promise.resolve( {
				'Operation' : true
			} );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/logout',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );
			expect( res.result.data ).to.be.an.object();
			expect( res.result.data.Operation ).to.be.boolean();

			authService.logout.restore();
			server.stop( done );
		} );
	} );
} );
