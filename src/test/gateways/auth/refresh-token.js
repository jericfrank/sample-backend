'use strict';

require( 'dotenv' ).config();

const Code        = require( 'code' );
const Lab         = require( 'lab' );
const jwt         = require( 'jsonwebtoken' );
const moment      = require( 'moment' );
const sinon       = require( 'sinon' );

const config      = use( 'config' );
const log         = use( 'logging' );
const authService = use( 'services/auth' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, config.app.jwtKey );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'auth'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );
	sinon.stub( log, 'info' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	log.info.restore();
	done();
} );

describe( 'POST: /api/refresh-token', () => {
	it( 'it should be able to response 400 bad request when response contains an error', done => {
		sinon.stub( authService, 'refreshToken', () => {
			return Promise.resolve( {
				'message' : 'JWT Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/refresh-token',
			'headers' : {
				'authorization' : 'Bearer sadjaksdjaskldasdasd'
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 400 );
			expect( res.result.error ).to.equal( 'Bad Request' );
			expect( res.result.message ).to.equal( 'JWT Error' );

			authService.refreshToken.restore();
			server.stop( done );
		} );
	} );

	it ( 'should be able to catch 500 server error', done => {
		sinon.stub( authService, 'refreshToken', function () {
			return Promise.reject( 'Something happened' );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/refresh-token',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			authService.refreshToken.restore();
			expect( res.statusCode ).to.equal( 500 );
			server.stop( done );
		} );
	} );

	it( 'it should be able to refresh new token', done => {
		const newToken = `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.
			eyJzdWIiOjIsImlhdCI6MTQ4NzU1MzUwNSwiZXhwIjoxNDg4NzYzMTA1fQ.
			niL8gaxuFpTSN71jgBoKniWHBQ6D8aCaIfWD3aNitws`;

		sinon.stub( authService, 'refreshToken', function () {
			return Promise.resolve( {
				'Token' : newToken
			} );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/refresh-token',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );
			expect( res.result.data ).to.be.an.object();
			expect( res.result.data.Token ).to.be.string();
			authService.refreshToken.restore();
			server.stop( done );
		} );
	} );
} );
