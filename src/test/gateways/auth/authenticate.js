'use strict';

require( 'dotenv' ).config();

const Code        = require( 'code' );
const Lab         = require( 'lab' );
const sinon       = require( 'sinon' );

const log         = use( 'logging' );
const authService = use( 'services/auth' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;

before( done => {
	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'auth'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );
	sinon.stub( log, 'info' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	log.info.restore();
	done();
} );

describe( 'POST: /api/auth', () => {
	it( 'should be able to catch 401 bad request when user does not exists', done => {
		sinon.stub( authService, 'authenticate', () => {
			return Promise.resolve( null );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/authenticate',
			'headers' : {
				'authorization' : 'saltkey'
			},
			'payload' : {
				'UserName' : 'danswater',
				'Password' : 'secret'
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 401 );
			expect( res.result.error ).to.equal( 'Unauthorized' );
			expect( res.result.message ).to.equal( 'Invalid username or password' );

			authService.authenticate.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 401 bad request when user account is disabled', done => {
		sinon.stub( authService, 'authenticate', () => {
			return Promise.resolve( { 'accountDisabled' : true } );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/authenticate',
			'headers' : {
				'authorization' : 'saltkey'
			},
			'payload' : {
				'UserName' : 'danswater',
				'Password' : 'secret'
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 401 );
			expect( res.result.error ).to.equal( 'Unauthorized' );
			expect( res.result.message ).to.equal( 'true' );

			authService.authenticate.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 400 bad request', done => {
		sinon.stub( authService, 'authenticate', function () {
			return Promise.resolve( {
				'message' : 'Validation Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/authenticate',
			'payload' : {
				'UserName' : 'danswater',
				'Password' : 'secrets'
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 400 );
			expect( res.result.error ).to.equal( 'Bad Request' );
			expect( res.result.message ).to.equal( 'Validation Error' );

			authService.authenticate.restore();
			server.stop( done );
		} );
	} );

	it( 'should be able to catch 500 server error', done => {
		sinon.stub( authService, 'authenticate', () => {
			return Promise.reject( 'Service Error' );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/authenticate',
			'payload' : {
				'UserName' : 'danswater',
				'Password' : 'secrets'
			}
		};

		server.inject( request, res => {
			authService.authenticate.restore();
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );
			server.stop( done );
		} );
	} );

	it( 'should be able to authenticate user', done => {
		sinon.stub( authService, 'authenticate', () => {
			return Promise.resolve( {
				'Token' : '<some-fancy-token>',
				'User'  : {
					'Id'              : 1,
					'UserName'        : 'danswater',
					'FirstName'       : 'Francis',
					'MiddleName'      : 'Viscayno',
					'LastName'        : 'Lamayo',
					'EmailAddress'    : 'dansisabot@gmail.com',
					'ProfileId'       : 1,
					'StationId'       : 1,
					'AccountDisabled' : false,
					'CreatedBy'       : 0,
					'UpdatedBy'       : 0,
					'created_at'      : '2017-02-06T06:04:16.000Z',
					'updated_at'      : '2017-02-22T07:13:04.000Z',
					'Profile'         : {
						'Id'         : 1,
						'Name'       : 'admin',
						'CreatedBy'  : 1,
						'UpdatedBy'  : 1,
						'created_at' : '2017-02-06T05:59:56.000Z',
						'updated_at' : '0000-00-00 00:00:00',
						'deleted_at' : null
					}
				}
			} );
		} );

		const request = {
			'method'  : 'POST',
			'url'     : '/api/authenticate',
			'payload' : {
				'UserName' : 'danswater',
				'Password' : 'secret'
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = res.result.data;

			expect( response.Token ).to.be.a.string();
			expect( response.User.Id ).to.be.a.number();
			expect( response.User.UserName ).to.be.a.string();
			expect( response.User.FirstName ).to.be.a.string();
			expect( response.User.MiddleName ).to.be.a.string();
			expect( response.User.LastName ).to.be.a.string();
			expect( response.User.EmailAddress ).to.be.a.string();
			expect( response.User.ProfileId ).to.be.a.number();
			expect( response.User.StationId ).to.be.a.number();
			expect( response.User.AccountDisabled ).to.be.boolean();
			expect( response.User.CreatedBy ).to.be.a.number();
			expect( response.User.UpdatedBy ).to.be.a.number();
			expect( response.User.Profile ).to.be.an.object();

			authService.authenticate.restore();
			server.stop( done );
		} );
	} );
} );
