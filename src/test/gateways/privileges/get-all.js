'use strict';

const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const log               = use( 'logging' );
const privilegesService = use( 'services/privileges' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, process.env.JWT_KEY );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'privileges'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	done();
} );

describe( 'GET: /api/privileges', () => {
	it( 'should be able to catch 500 server error', done => {
		sinon.stub( privilegesService, 'getAll', () => {
			return Promise.reject( {
				'message' : 'Finding Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/privileges',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			privilegesService.getAll.restore();
			server.stop( done );
		} );
	} );

	it ( 'should be able to fetch filtered resource by profile', done => {
		sinon.stub( privilegesService, 'getAll', function () {
			return [ {
				'ModuleId'   : 5,
				'ProfileId'  : 1,
				'CanRead'    : true,
				'CanCreate'  : true,
				'CanUpdate'  : true,
				'CanDelete'  : true,
				'created_at' : '2017-02-06T06:01:57.000Z',
				'updated_at' : '0000-00-00 00:00:00'
			} ];
		} );
		const request = {
			'method'  : 'GET',
			'url'     : '/api/privileges?profile=1',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = res.result.data;

			expect( response[ 0 ].ModuleId ).to.be.a.number();
			expect( response[ 0 ].ProfileId ).to.be.a.number();
			expect( response[ 0 ].CanRead ).to.be.boolean();
			expect( response[ 0 ].CanCreate ).to.be.boolean();
			expect( response[ 0 ].CanUpdate ).to.be.boolean();
			expect( response[ 0 ].CanDelete ).to.be.boolean();

			privilegesService.getAll.restore();
			server.stop( done );
		} );
	} );

	it ( 'should be able to fetch all resource', done => {
		sinon.stub( privilegesService, 'getAll', function () {
			return [ {
				'ModuleId'   : 5,
				'ProfileId'  : 1,
				'CanRead'    : true,
				'CanCreate'  : true,
				'CanUpdate'  : true,
				'CanDelete'  : true,
				'created_at' : '2017-02-06T06:01:57.000Z',
				'updated_at' : '0000-00-00 00:00:00'
			}, {
				'ModuleId'   : 5,
				'ProfileId'  : 2,
				'CanRead'    : true,
				'CanCreate'  : true,
				'CanUpdate'  : true,
				'CanDelete'  : true,
				'created_at' : '2017-02-06T06:01:57.000Z',
				'updated_at' : '0000-00-00 00:00:00'
			} ];
		} );
		const request = {
			'method'  : 'GET',
			'url'     : '/api/privileges',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = res.result.data;

			expect( response.length ).to.be.equal( 2 );

			expect( response[ 0 ].ModuleId ).to.be.a.number();
			expect( response[ 0 ].ProfileId ).to.be.a.number();
			expect( response[ 0 ].CanRead ).to.be.boolean();
			expect( response[ 0 ].CanCreate ).to.be.boolean();
			expect( response[ 0 ].CanUpdate ).to.be.boolean();
			expect( response[ 0 ].CanDelete ).to.be.boolean();

			privilegesService.getAll.restore();
			server.stop( done );
		} );
	} );
} );
