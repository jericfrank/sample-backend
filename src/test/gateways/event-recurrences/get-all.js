'use strict';

const _      = require( 'lodash' );
const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const config                 = use( 'config' );
const log                    = use( 'logging' );
const eventRecurrenceService = use( 'services/event-recurrences' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, config.app.jwtKey );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'event-recurrences'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	done();
} );

describe( 'GET: /api/event-recurrences', () => {
	it( 'should be able to catch 500 server error', done => {
		sinon.stub( eventRecurrenceService, 'getAll', () => {
			return Promise.reject( {
				'message' : 'Finding Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/event-recurrences',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			eventRecurrenceService.getAll.restore();
			server.stop( done );
		} );
	} );

	it ( 'should be able to fetch all resource', done => {
		sinon.stub( eventRecurrenceService, 'getAll', function () {
			return [ {
				'Id'                  : 8,
				'EventId'             : 25,
				'RepeatOption'        : 1,
				'DayOption'           : 1,
				'RepeatEvery'         : 2,
				'Weekdays'            : [],
				'Day'                 : null,
				'MonthOption'         : null,
				'MonthWeekOption'     : null,
				'MonthWeekdaysOption' : null,
				'Start'               : '2017-06-05T00:00:00.000Z',
				'End'                 : null,
				'EndAfter'            : 4
			} ];
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/event-recurrences',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = _.first( res.result.data );

			expect( response.Id ).to.be.a.number();
			expect( response.EventId ).to.be.a.number();
			expect( response.RepeatOption ).to.be.number();
			expect( response.DayOption ).to.be.number();
			expect( response.RepeatEvery ).to.be.number();
			expect( response.Weekdays ).to.be.an.array();

			eventRecurrenceService.getAll.restore();
			server.stop( done );
		} );
	} );
} );
