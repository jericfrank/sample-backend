'use strict';

const _      = require( 'lodash' );
const Code   = require( 'code' );
const Lab    = require( 'lab' );
const jwt    = require( 'jsonwebtoken' );
const moment = require( 'moment' );
const sinon  = require( 'sinon' );

const config                = use( 'config' );
const log                   = use( 'logging' );
const eventFrequencyService = use( 'services/event-frequencies' );

const lab      = exports.lab = Lab.script();
const describe = lab.describe;
const it       = lab.test;
const before   = lab.before;
const after    = lab.after;
const expect   = Code.expect;

let server;
let authorization;

before( done => {
	const payload = {
		'sub' : 1,
		'iat' : moment().unix(),
		'exp' : moment().add( 14, 'days' ).unix()
	};

	const token   = jwt.sign( payload, config.app.jwtKey );
	authorization = `Bearer ${token}`;

	server = use( 'utils/test-server' )( {
		'port'     : 9002,
		'gateways' : 'event-frequencies'
	} );

	sinon.stub( log, 'fatal' ).returns( '' );

	done();
} );

after( done => {
	log.fatal.restore();
	done();
} );

describe( 'GET: /api/event-frequencies', () => {
	it( 'should be able to catch 500 server error', done => {
		sinon.stub( eventFrequencyService, 'getAll', () => {
			return Promise.reject( {
				'message' : 'Finding Error',
				'errors'  : [ {
					'message' : 'Error was emitted'
				} ]
			} );
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/event-frequencies',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 500 );
			expect( res.result.error ).to.equal( 'Internal Server Error' );
			expect( res.result.message ).to.equal( 'An internal server error occurred' );

			eventFrequencyService.getAll.restore();
			server.stop( done );
		} );
	} );

	it ( 'should be able to fetch all resource', done => {
		sinon.stub( eventFrequencyService, 'getAll', function () {
			return [ {
				'Id'                : 1,
				'Name'              : 'Static',
				'UnitOfMeasurement' : 'hrs',
				'CreatedBy'         : 1,
				'UpdatedBy'         : 1,
				'created_at'        : '2017-02-06T05:59:56.000Z',
				'updated_at'        : '0000-00-00 00:00:00'
			} ];
		} );

		const request = {
			'method'  : 'GET',
			'url'     : '/api/event-frequencies',
			'headers' : {
				authorization
			}
		};

		server.inject( request, res => {
			expect( res.statusCode ).to.equal( 200 );

			const response = _.first( res.result.data );

			expect( response.Id ).to.be.a.number();
			expect( response.Name ).to.be.a.string();
			expect( response.UnitOfMeasurement ).to.be.a.string();
			expect( response.CreatedBy ).to.be.a.number();
			expect( response.UpdatedBy ).to.be.a.number();

			eventFrequencyService.getAll.restore();
			server.stop( done );
		} );
	} );
} );
