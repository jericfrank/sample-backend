# Project greasy API ![Build Status](https://circleci.com/gh/tritontekph/project-greasy-be.png?circle-token=3b17fe692f15cbce6a57de2b447661624b3b2bbb)

## Prerequisites

1. [Node.js]( http://nodejs.org ) (v6.x LTS)
- on OSX use [homebrew](http://brew.sh) `brew install node`
- on Windows use [chocolatey](https://chocolatey.org/) `choco install nodejs`

2. [MySQL](www.mysql.com) (5.*)
3. [redis](https://redis.io/)
2. nodemon (v1.9.1)
3. [eslint](https://www.npmjs.com/package/eslint-config-airbnb)

# Third party tools
- [sparkpost](http://www.sparkpost.com)
- [s3](https://aws.amazon.com/s3/)

## Environment Variables
 ```env
    NODE_ENV=[development|production]
    JWT_KEY=[some-random-string]
    SPARKPOST_KEY=[some-random-string]
    DB_HOST=[db host]
    DB_USERNAME=[db username]
    DB_PASSWORD=[db password]
    AWS_ACCESS_KEY_ID = [aws key id]
    AWS_SECRET_ACCESS_KEY = [aws secret access key]
    AWS_BUCKET_NAME= [s3 bucket name]
    S3_URL=[ s3 url path ]
    APP_PORT=[application port]
    IMAGE_PROXY=[image proxy url]
 ```

## Installation
In order to setup, run and start hacking the app locally you just have to:

1. Clone this repository
2. `npm install`
3. run `npm run dev`

At this point, you should be have your local deploy running on `http://localhost:9001`.


## Docs
Point your browser to: `http://localhost:9001/api/docs`
## Kue redis
Point your browser to `http://localhost:9007/kue`
