'use strict';

global.use = modulePath => require( `${__dirname}/src/${modulePath}` );

require( 'dotenv' ).config();

module.exports = {
	'verbose'          : true,
	'lint'             : true,
	'colors'           : true,
	'debug'            : true,
	'threshold'        : 0,
	'environment'      : 'development',
	'coverage-exclude' : 'src/test'
};
