CREATE TABLE `MWFB`.`event_game_parts` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `part_name` VARCHAR(20) NOT NULL,
  `part_description` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`));
