CREATE DATABASE  IF NOT EXISTS `MWFB` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `MWFB`;
-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: MWFB
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `event_details`
--

DROP TABLE IF EXISTS `event_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ed_e_id` int(10) unsigned NOT NULL,
  `ed_e_desc` varchar(30) NOT NULL,
  `created_by` int(10) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(10) unsigned NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_details_1_idx` (`ed_e_id`),
  CONSTRAINT `fk_event_details_1` FOREIGN KEY (`ed_e_id`) REFERENCES `events` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_details`
--

LOCK TABLES `event_details` WRITE;
/*!40000 ALTER TABLE `event_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_frequency_types`
--

DROP TABLE IF EXISTS `event_frequency_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_frequency_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `eft_name` varchar(30) DEFAULT NULL COMMENT 'frequency type name',
  `eft_uom_id` int(10) unsigned DEFAULT NULL COMMENT 'uom id',
  ` created_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'created by whom',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created when',
  `updated_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'updated by whom',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'updated when',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'deleted when',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_frequency_types`
--

LOCK TABLES `event_frequency_types` WRITE;
/*!40000 ALTER TABLE `event_frequency_types` DISABLE KEYS */;
INSERT INTO `event_frequency_types` VALUES (1,'immediate',NULL,1,'2017-02-06 05:53:08',1,'0000-00-00 00:00:00',NULL),(2,'every nth seconds',1,1,'2017-02-06 05:53:08',1,'0000-00-00 00:00:00',NULL),(3,'every nth minutes',2,1,'2017-02-06 05:53:08',1,'0000-00-00 00:00:00',NULL),(4,'every nth hours',3,1,'2017-02-06 05:53:08',1,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `event_frequency_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_settings`
--

DROP TABLE IF EXISTS `event_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'event setting id',
  `es_e_id` int(10) unsigned NOT NULL COMMENT 'event id',
  `es_eft_id` int(10) unsigned NOT NULL COMMENT 'event type id',
  `es_eft_value` double(10,2) NOT NULL DEFAULT '0.00' COMMENT 'frequency value',
  `es_color` varchar(10) NOT NULL DEFAULT '#000000' COMMENT 'color of the event in calendar',
  `es_nolineoftxttoexport` int(10) NOT NULL DEFAULT '0' COMMENT 'no of lines of text to export',
  `created_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'created by whom',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created when',
  `updated_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'updated by whom',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'updated when',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'deleted when',
  PRIMARY KEY (`id`),
  KEY `fk_event_settings_1_idx` (`es_e_id`),
  KEY `fk_event_settings_2_idx` (`es_eft_id`),
  CONSTRAINT `fk_event_settings_1` FOREIGN KEY (`es_e_id`) REFERENCES `events` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_settings_2` FOREIGN KEY (`es_eft_id`) REFERENCES `event_frequency_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_settings`
--

LOCK TABLES `event_settings` WRITE;
/*!40000 ALTER TABLE `event_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_types`
--

DROP TABLE IF EXISTS `event_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'event type id',
  `et_name` varchar(15) NOT NULL COMMENT 'event type name',
  `created_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'created by whom',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created when',
  `updated_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'updated by whom',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'updated when',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'deleted when',
  PRIMARY KEY (`id`),
  UNIQUE KEY `et_name_UNIQUE` (`et_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_types`
--

LOCK TABLES `event_types` WRITE;
/*!40000 ALTER TABLE `event_types` DISABLE KEYS */;
INSERT INTO `event_types` VALUES (1,'Static/Fixed',1,'2017-02-06 05:49:57',1,'0000-00-00 00:00:00',NULL),(2,'Live',1,'2017-02-06 05:49:57',1,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `event_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'event id',
  `e_s_id` int(10) unsigned NOT NULL COMMENT 'station id',
  `e_et_id` int(10) unsigned NOT NULL COMMENT 'type of event',
  `e_title` varchar(50) NOT NULL COMMENT 'Title of Event',
  `e_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'start of event',
  `e_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'End of event',
  `e_team1` varchar(25) DEFAULT NULL COMMENT 'Name of the opponent',
  `e_score1` double(10,2) DEFAULT NULL COMMENT 'score of the opponent',
  `e_team2` varchar(25) DEFAULT NULL COMMENT 'name of the opponent',
  `e_score2` double(10,2) DEFAULT NULL COMMENT 'Score of the challenger',
  `created_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'created by whom',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created when',
  `updated_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'updated by whom',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'updated when',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Deleted when',
  PRIMARY KEY (`id`),
  KEY `fk_events_1_idx` (`e_et_id`),
  KEY `fk_events_2_idx` (`e_s_id`),
  CONSTRAINT `fk_events_1` FOREIGN KEY (`e_et_id`) REFERENCES `event_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_events_2` FOREIGN KEY (`e_s_id`) REFERENCES `stations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_categories`
--

DROP TABLE IF EXISTS `module_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'module category id',
  `mc_name` varchar(50) NOT NULL COMMENT 'module category name',
  `created_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'created by whom',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created when',
  `updated_by` int(10) unsigned DEFAULT '1' COMMENT 'updated by whom',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'updated when',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'deleted when',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mc_name_UNIQUE` (`mc_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_categories`
--

LOCK TABLES `module_categories` WRITE;
/*!40000 ALTER TABLE `module_categories` DISABLE KEYS */;
INSERT INTO `module_categories` VALUES (1,'User Management',1,'2017-02-06 05:56:09',1,'0000-00-00 00:00:00',NULL),(2,'Stations',1,'2017-02-06 05:56:09',1,'0000-00-00 00:00:00',NULL),(3,'Station Calendar',1,'2017-02-06 05:56:09',1,'0000-00-00 00:00:00',NULL),(4,'Live Events',1,'2017-02-06 05:56:09',1,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `module_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'module id',
  `m_name` varchar(50) NOT NULL COMMENT 'module name',
  `m_mc_id` int(10) unsigned NOT NULL COMMENT 'module category id',
  `m_route_name` varchar(100) NOT NULL COMMENT 'route name',
  `created_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'created by whom',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created when',
  `updated_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'updated by whom',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'updated when',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'deleted when',
  PRIMARY KEY (`id`),
  UNIQUE KEY `m_name_UNIQUE` (`m_name`),
  KEY `fk_modules_1_idx` (`m_mc_id`),
  CONSTRAINT `fk_modules_1` FOREIGN KEY (`m_mc_id`) REFERENCES `module_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,'User Management',1,'users',1,'2017-02-06 05:58:20',1,'0000-00-00 00:00:00',NULL),(2,'Change password',1,'changepassword',1,'2017-02-06 05:58:20',1,'0000-00-00 00:00:00',NULL),(3,'Station Management',2,'stations',1,'2017-02-06 05:58:20',1,'0000-00-00 00:00:00',NULL),(4,'Station Calendar',3,'calendar',1,'2017-02-06 05:58:20',1,'0000-00-00 00:00:00',NULL),(5,'Live Events',4,'liveevents',1,'2017-02-06 05:58:20',1,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_modules`
--

DROP TABLE IF EXISTS `profile_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Profile module Id\n',
  `pm_p_id` int(10) unsigned NOT NULL COMMENT 'profile Id',
  `pm_m_id` int(10) unsigned NOT NULL COMMENT 'module id\n',
  `pm_can_read` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'has read access',
  `pm_can_create` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'has create access',
  `pm_can_update` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'has update access',
  `pm_can_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'has delete access',
  `created_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'created by whom',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created when',
  `updated_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'updated by whom',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'updated by whom',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'deleted when',
  PRIMARY KEY (`id`),
  KEY `fk_profile_modules_1_idx` (`pm_p_id`),
  KEY `fk_profile_modules_2_idx` (`pm_m_id`),
  CONSTRAINT `fk_profile_modules_1` FOREIGN KEY (`pm_p_id`) REFERENCES `profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profile_modules_2` FOREIGN KEY (`pm_m_id`) REFERENCES `modules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_modules`
--

LOCK TABLES `profile_modules` WRITE;
/*!40000 ALTER TABLE `profile_modules` DISABLE KEYS */;
INSERT INTO `profile_modules` VALUES (1,1,1,1,1,1,1,1,'2017-02-06 06:01:57',1,'0000-00-00 00:00:00',NULL),(2,1,2,1,1,1,1,1,'2017-02-06 06:01:57',1,'0000-00-00 00:00:00',NULL),(3,1,3,1,1,1,1,1,'2017-02-06 06:01:57',1,'0000-00-00 00:00:00',NULL),(4,1,4,1,1,1,1,1,'2017-02-06 06:01:57',1,'0000-00-00 00:00:00',NULL),(5,1,5,1,1,1,1,1,'2017-02-06 06:01:57',1,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `profile_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_name` varchar(30) NOT NULL COMMENT 'profile name',
  `created_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT ' created by whom',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created when',
  `updated_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'updated by whom',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'updated when',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'deleted when',
  PRIMARY KEY (`id`),
  UNIQUE KEY `p_name_UNIQUE` (`p_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (1,'admin',1,'2017-02-06 05:59:56',1,'0000-00-00 00:00:00',NULL),(2,'power users',1,'2017-02-06 05:59:56',1,'0000-00-00 00:00:00',NULL),(3,'managers',1,'2017-02-06 05:59:56',1,'0000-00-00 00:00:00',NULL),(4,'standard',1,'2017-02-06 05:59:56',1,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `station_calendar_settings`
--

DROP TABLE IF EXISTS `station_calendar_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `station_calendar_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'station calendar settings id',
  `scs_s_id` int(10) unsigned NOT NULL COMMENT 'station id',
  `scs_storage_location` varchar(150) NOT NULL COMMENT 'storage location of files',
  `scs_startofweek` int(11) NOT NULL DEFAULT '1' COMMENT 'startofweek',
  `created_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'created by whom',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created when',
  `updated_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'updated by whom',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'updated when',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'deleted when',
  PRIMARY KEY (`id`),
  KEY `fk_station_calendar_settings_1_idx` (`scs_s_id`),
  CONSTRAINT `fk_station_calendar_settings_1` FOREIGN KEY (`scs_s_id`) REFERENCES `stations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `station_calendar_settings`
--

LOCK TABLES `station_calendar_settings` WRITE;
/*!40000 ALTER TABLE `station_calendar_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `station_calendar_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stations`
--

DROP TABLE IF EXISTS `stations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'station id',
  `s_name` varchar(45) NOT NULL COMMENT 'station name',
  `s_location` varchar(50) NOT NULL COMMENT 'station location',
  `s_logo` varchar(150) DEFAULT NULL COMMENT 'station logo',
  `created_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'created by whom',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created when',
  `updated_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'updated by whom',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'updated when',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'deleted when',
  PRIMARY KEY (`id`),
  UNIQUE KEY `s_name_UNIQUE` (`s_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stations`
--

LOCK TABLES `stations` WRITE;
/*!40000 ALTER TABLE `stations` DISABLE KEYS */;
INSERT INTO `stations` VALUES (1,'MWFB','Wisconsin',NULL,1,'2017-02-06 06:04:00',1,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `stations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit_of_measurements`
--

DROP TABLE IF EXISTS `unit_of_measurements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit_of_measurements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'uom id',
  `uom_name` varchar(30) NOT NULL COMMENT 'uom name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uom_name_UNIQUE` (`uom_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit_of_measurements`
--

LOCK TABLES `unit_of_measurements` WRITE;
/*!40000 ALTER TABLE `unit_of_measurements` DISABLE KEYS */;
INSERT INTO `unit_of_measurements` VALUES (3,'hours'),(2,'minutes'),(1,'seconds');
/*!40000 ALTER TABLE `unit_of_measurements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `u_name` varchar(20) NOT NULL COMMENT 'User id or name',
  `u_first_name` varchar(30) NOT NULL COMMENT 'First Name',
  `u_middle_name` varchar(30) NOT NULL COMMENT 'Middle Name',
  `u_last_name` varchar(30) NOT NULL COMMENT 'Last Name',
  `u_email_address` varchar(60) NOT NULL COMMENT 'email address',
  `u_password` varchar(150) NOT NULL COMMENT 'password',
  `u_p_id` int(10) unsigned NOT NULL COMMENT 'profile id',
  `u_user_picture` varchar(150) DEFAULT NULL COMMENT 'user picture URL',
  `u_default_s_id` int(10) unsigned NOT NULL COMMENT 'default station id of user',
  `u_account_disabled` tinyint(1) DEFAULT '0' COMMENT 'is account disabled',
  `created_by` int(10) unsigned NOT NULL COMMENT 'created by whom',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created when',
  `updated_by` int(10) unsigned NOT NULL COMMENT 'updated by whom',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'updated when',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'deleted when',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `u_name_UNIQUE` (`u_name`),
  KEY `fk_users_1_idx` (`u_p_id`),
  KEY `fk_users_2_idx` (`u_default_s_id`),
  CONSTRAINT `fk_users_1` FOREIGN KEY (`u_p_id`) REFERENCES `profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_2` FOREIGN KEY (`u_default_s_id`) REFERENCES `stations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','james','f','brennan','j.brennan@tritontek.ph','$2y$10$Lizwh/uUNb63gRjd3zQWqOS8.XjqGfF8MWeuKJOlHRJzUOfVLmf/m',1,NULL,1,0,0,'2017-02-06 06:04:16',0,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-09 15:13:46
