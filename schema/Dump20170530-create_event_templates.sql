CREATE TABLE `event_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'event id',
  `e_s_id` int(10) unsigned NOT NULL COMMENT 'station id',
  `e_et_id` int(10) unsigned NOT NULL COMMENT 'type of event',
  `e_title` varchar(50) NOT NULL COMMENT 'Title of Event',
  `e_team1` varchar(25) DEFAULT NULL,
  `e_team1_abbrv` varchar(20) DEFAULT NULL,
  `e_score1` double(10,2) DEFAULT NULL COMMENT 'score of the opponent',
  `e_team2` varchar(25) DEFAULT NULL,
  `e_team2_abbrv` varchar(20) DEFAULT NULL,
  `e_score2` double(10,2) DEFAULT NULL COMMENT 'Score of the challenger',
  `e_part_no` int(3) DEFAULT NULL,
  `e_gamepart_id` int(10) unsigned DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'created by whom',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created when',
  `updated_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'updated by whom',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'updated when',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Deleted when',
  PRIMARY KEY (`id`),
  KEY `fk_et_1_idx` (`e_et_id`),
  KEY `fk_et_2_idx` (`e_s_id`),
  CONSTRAINT `fk_et_1` FOREIGN KEY (`e_et_id`) REFERENCES `event_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_et_2` FOREIGN KEY (`e_s_id`) REFERENCES `stations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

CREATE TABLE `event_detail_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ed_e_id` int(10) unsigned NOT NULL,
  `ed_e_desc` varchar(30) NOT NULL,
  `created_by` int(10) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(10) unsigned NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_edt_1_idx` (`ed_e_id`),
  CONSTRAINT `fk_edt_1` FOREIGN KEY (`ed_e_id`) REFERENCES `event_templates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `event_setting_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'event setting id',
  `es_e_id` int(10) unsigned NOT NULL COMMENT 'event id',
  `es_eft_id` int(10) unsigned NOT NULL COMMENT 'event type id',
  `es_eft_value` double(10,2) NOT NULL DEFAULT '0.00' COMMENT 'frequency value',
  `es_color` varchar(10) NOT NULL DEFAULT '#000000' COMMENT 'color of the event in calendar',
  `es_nolineoftxttoexport` int(10) NOT NULL DEFAULT '0' COMMENT 'no of lines of text to export',
  `created_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'created by whom',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created when',
  `updated_by` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'updated by whom',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'updated when',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'deleted when',
  PRIMARY KEY (`id`),
  KEY `fk_est_1_idx` (`es_e_id`),
  KEY `fk_est_2_idx` (`es_eft_id`),
  CONSTRAINT `fk_est_1` FOREIGN KEY (`es_e_id`) REFERENCES `event_templates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_est_2` FOREIGN KEY (`es_eft_id`) REFERENCES `event_frequency_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
