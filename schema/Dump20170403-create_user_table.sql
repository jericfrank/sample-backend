CREATE TABLE `user_stations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `us_s_id` int(10) unsigned NOT NULL,
  `us_u_id` int(10) unsigned NOT NULL,
  `us_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_user_stations_1_idx` (`us_s_id`),
  KEY `fk_user_stations_2_idx` (`us_u_id`),
  CONSTRAINT `fk_user_stations_1` FOREIGN KEY (`us_s_id`) REFERENCES `stations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_stations_2` FOREIGN KEY (`us_u_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
