ALTER TABLE `MWFB`.`event_templates`
DROP FOREIGN KEY `fk_et_2`;
ALTER TABLE `MWFB`.`event_templates`
CHANGE COLUMN `e_s_id` `e_s_id` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'station id' ,
DROP INDEX `fk_et_2_idx` ;
