- Table for Recurrence
CREATE TABLE `event_recurrences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `er_e_id` int(10) unsigned NOT NULL,
  `er_repeatoption_id` int(10) unsigned NOT NULL,
  `er_day_option` tinyint(1) DEFAULT NULL,
  `er_repeat_every` int(3) DEFAULT NULL,
  `er_repeat_dayofweek` varchar(20) NULL DEFAULT NULL,
  `er_day` int(3) DEFAULT NULL,
  `er_month_option` tinyint(1) DEFAULT NULL,
  `er_monthweek_option` tinyint(1) DEFAULT NULL,
  `er_monthweekdays_option` tinyint(1) DEFAULT NULL,
  `er_start_date` datetime NOT NULL,
  `er_end_date` datetime DEFAULT NULL,
  `er_end_after` int(3) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_recurrences_1_idx` (`er_repeatoption_id`),
  KEY `fk_event_recurrences_2_idx` (`er_e_id`),
  CONSTRAINT `fk_event_recurrences_1` FOREIGN KEY (`er_repeatoption_id`) REFERENCES `event_repeat_options` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_recurrences_2` FOREIGN KEY (`er_e_id`) REFERENCES `events` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
