'use strict';

global.use = modulePath => require( `${__dirname}/src/${modulePath}` );

require( 'dotenv' ).config();

require( './src/services/async-files' );

require( './src/kue-server' );
